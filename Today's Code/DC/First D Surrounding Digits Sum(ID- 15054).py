a, b = map(int, input().split())
z = list(list(map(int, input().split())) for _ in range(a))


def fun(x, y):
    if x >= 0 and x < a and y >= 0 and y < b:
        return [z[x][y], 1]
    return [0, 0]


for i in range(a):
    for j in range(b):
        t = 0
        o = z[i][j]
        for k, l in [[-1, -1], [-1, 0], [-1, 1], [0, 1], [1, 1], [1, 0], [1, -1], [0, -1]]:
            p = fun(i+k, j+l)
            t += p[0]
            o -= p[1]
            if o == 0:
                break
        print(t, end=' ')
    print()
