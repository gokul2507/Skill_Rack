#include <stdio.h>
#include <stdlib.h>

int main()
{
     char ch[1000][1000];
     int l = 0;
     while (scanf("%s ", ch[l]) > 0)
     {
          l++;
     }
     for (int i = 0; i < l / 2; i++)
     {
          char c = ch[i][strlen(ch[i]) - 1];
          for (int j = l / 2; j < l; j++)
          {
               if (ch[j][0] == c)
               {
                    printf("%s", ch[i]);
                    for (int k = 1; k < strlen(ch[j]); k++)
                    {
                         printf("%c", ch[j][k]);
                    }
                    printf(" ");
                    ch[j][0] = '*';
                    break;
               }
          }
     }
}