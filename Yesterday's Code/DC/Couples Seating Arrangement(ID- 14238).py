n = int(input())
l = list(input().strip())
count = 0
for i in range(0, len(l)-1):
    if l[i] == "#" and l[i+1] == "#":
        if i != 0:
            if i < len(l)-2 and l[i-1] != l[i+2]:
                if l[i-1] == "M":
                    l[i] = "M"
                    l[i+1] = "F"
                    count += 1
                else:
                    l[i] = "F"
                    l[i+1] = "M"
                    count += 1
            elif i == len(l)-2:
                if l[i-1] == "M":
                    l[i] = "M"
                    l[i+1] = "F"
                else:
                    l[i] = "F"
                    l[i+1] = "M"
                count += 1
        else:
            j = i
            while(j < len(l) and l[j] == "#"):
                j += 1
            k = j-i
            if k == len(l):
                count = n//2
                break
            if k != 0 and k % 4 == 0 and l[j] == "M":
                l[i] = "M"
                l[i+1] = "F"
                count += 1
            elif k != 0 and k % 4 == 0 and l[j] == "F":
                l[i] = "F"
                l[i+1] = "M"
                count += 1
            elif k != 0 and k % 2 == 0 and k % 4 != 0 and l[j] == "F":
                l[i] = "M"
                l[i+1] = "F"
                count += 1
            elif k != 0 and k % 2 == 0 and k % 4 != 0 and l[j] == "M":
                l[i] = "F"
                l[i+1] = "M"
                count += 1
            elif k >= 1:
                l[i] = "M"
                l[i+1] = "F"
                count += 1
print(count)
