#include <stdio.h>
#include <stdlib.h>

int main()
{
     int n, count = 0, i = 0;
     scanf("%d", &n);
     char seats[n];
     scanf("%s", seats);
     if (n == 2 && seats[0] == seats[1] && seats[0] == '#')
     {
          printf("1");
          return 0;
     }
     while (i < n - 1)
     {
          int temp = i;
          while (temp < n - 1 && seats[temp] == '#' && seats[temp + 1] == '#')
               temp++;
          if (temp - i >= 2)
          {
               int ind = temp + 1;
               if (ind >= n)
               {
                    count += (ind - i) / 2;
                    break;
               }
               while (ind - 2 >= i && seats[ind] != seats[ind - 3])
               {
                    if (seats[ind] == 'M')
                    {
                         seats[ind - 1] = 'M';
                         seats[ind - 2] = 'F';
                    }
                    else
                    {
                         seats[ind - 1] = 'F';
                         seats[ind - 2] = 'M';
                    }
                    ind -= 2;
                    count++;
               }
               i = temp + 1;
          }
          else if (temp - i == 1)
          {
               if (seats[temp + 1] != seats[i - 1])
               {
                    seats[temp] = seats[temp + 1];
                    seats[i] = seats[i - 1];
                    count++;
                    i = temp + 1;
               }
               else
                    i++;
          }
          else
               i++;
     }
     printf("%d", count);
}