import java.util.*;
public class Hello {

    public static void main(String[] args) {
		//Your Code Here
		Scanner sc = new Scanner(System.in);
		int n  = sc.nextInt();
		char[] arr = sc.next().toCharArray();
		int sum=0;
		for(int index=0;index<n;index++){
		    if(arr[index]=='#'){
		        char prev= index>=1?arr[index-1]:'\0',next='\0';
		        int hashCount=1,j=-1;
		        for(j=index+1;j<n;j++,hashCount++){
		            if(Character.isAlphabetic(arr[j])){
		                next = arr[j];
		                break;
		            }
		        }
		        index =j;
		        int val =count(prev,next,hashCount);
		        //System.out.printf("at index %d ,%c %c %d\n",index,prev,next,hashCount);
		        sum+=val;
		    }
		}
		System.out.println(sum);
	}
	static int count(char prev,char next,int hashCount){
	    if(hashCount%2==0){
	        int quotient = hashCount/2; 
	        if(prev=='\0'||next=='\0') return quotient;
	        if((quotient%2==1 && prev!=next)||(quotient%2==0 && prev==next)){
	           return quotient; 
	        }
	        return quotient-1;
	    }else{
	        return hashCount/2;
	    }
	}
}