#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int N;
     cin >> N;
     string s;
     cin >> s;
     int ind = 0, ans = 0;
     while (ind < N && s[ind] == '#')
     {
          ind++;
     }
     ans += ind / 2;
     if (ind == N)
     {
          cout << ans;
          return 0;
     }

     int ctr = 0;
     while (ind < N)
     {
          ctr = 0;
          while (ind < N && s[ind] != '#')
               ind++;
          if (ind == N)
               break;
          char pre = s[ind - 1];

          while (ind < N && s[ind] == '#')
               ind++, ctr++;
          if (ind == N)
               break;
          char curr = s[ind];

          // cout << curr << " " << pre << " " << ctr << endl;
          if (ctr % 2)
               ans += ctr / 2;
          else if (curr == pre)
          {
               if ((ctr / 2) & 1)
                    ans += (ctr - 2) / 2;
               else
                    ans += ctr / 2;
          }
          else
          {
               if ((ctr / 2) & 1)
                    ans += ctr / 2;
               else
                    ans += (ctr - 2) / 2;
          }
     }
     // cout << ctr << endl;
     ans += ctr / 2;
     cout << ans;
}