import java.util.*;
public class Hello {

    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
        int a[]=new int[n];
        for(int i=0;i<n;i++) a[i]=sc.nextInt();
        int t=sc.nextInt();
        int r=n%t;
        int remc=0;
        int rem[]=new int[r];
        for(int i=n-r;i<n;i++){
            rem[remc++]=a[i];
        }
        int not=n/t;
        ArrayList<Integer> temp=new ArrayList<>();
        if(t>=n){
            int m=0;
            for(int i=0;i<n;i++){
                if(a[i]>m) m=a[i];
            }
            System.out.println(m);
            System.exit(0);
        }
        int c=1;
        for(int i=0;i<n-r;i+=t){
            for(int j=i;j<i+t;j++){
                temp.add(a[j]);
            }
            for(int j=c-1;j<remc;j+=not){
                temp.add(rem[j]);
            }
            int max=0;
            for(int j=0;j<temp.size();j++){
                if(temp.get(j)>max) max=temp.get(j);
            }
            System.out.printf("%d ",max);
            temp.clear();
            c++;
        }
	}
}
