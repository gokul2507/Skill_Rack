#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int n,s;
    cin>>n;
    vector<int> z,l;
    for(int i=0;i<n;i++){
        int t;
        cin>>t;
        z.push_back(t);
    }
    cin>>s;
    if(s>=n){
        int m=z[0];
        for(int i=1;i<n;i++)
        m=max(z[i],m);
        cout<<m;
        return 0;
    }
    for(int i=0;i+s<=n;i+=s){
        int m=z[i];
        for(int j=i;j<i+s;j++)
            m=max(z[j],m);
        l.push_back(m);
    }
    int p=n-(n%s);
    for(int i=0;i<n%s;i++){
         l[i%l.size()]=max(l[i%l.size()],z[p+i]);
    }
    for(int i:l)
    cout<<i<<" ";
}