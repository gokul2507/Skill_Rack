#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int n;
    cin>>n;
    vector<int> z;
    for(int i=0;i<n;i++){
        int temp;
        cin>>temp;
        z.push_back(temp);
    }
    sort(z.begin(),z.end());
    int ans=0,t_len=1;
    for(int i=1;i<n;i++){
        if(z[i-1]+1!=z[i]){
            if(t_len>1){
                ans++;
            }
            t_len=1;
        }
        else{
            t_len++;
        }
    }
    if(t_len>1)
    ans++;
    cout<<ans;
}
