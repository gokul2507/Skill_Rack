#include <bits/stdc++.h>
 
using namespace std;
vector<vector<int>> d;
 int fun(vector<int>& x, int left,int right){
     int ma=0;  
     if(d[left][right]!=-1) 
     return d[left][right];
     for(int i=left+1;i<right;i++){
         int temp=fun(x,left,i)+fun(x,i,right)+(x[left]*x[i]*x[right]); 
         ma=max(ma,temp); 
    }   
    d[left][right]=ma; 
    return ma; 
 }    
 int maxCoins(vector<int>& nums) {     
     nums.insert(nums.begin(),1); 
     nums.push_back(1);      
     int len=nums.size();
     d.resize(len+1,vector<int>(len+1,-1)); 
     int temp=fun(nums,0,len-1); 
     return temp;  
}

int main(int argc, char** argv)
{
    int n;
    cin>>n;
    vector<int> z;
    for(int i=0;i<n;i++){
        int temp;
        cin>>temp;
        z.push_back(temp);
    }
    cout<<maxCoins(z);
}