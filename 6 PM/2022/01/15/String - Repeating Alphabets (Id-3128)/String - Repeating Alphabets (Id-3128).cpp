#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    string s;
    cin>>s;
    map<char,int> d;
    for(int i=0;i<s.size();i++){
        if(d.find(s[i])==d.end()){
            d[s[i]]=1;
        }
        else{
            d[s[i]]=2;
        }
    }
    for(int i=0;i<s.size();i++){
        if(d[s[i]]==2){
            cout<<s[i];
            d[s[i]]=0;
        }
    }
}