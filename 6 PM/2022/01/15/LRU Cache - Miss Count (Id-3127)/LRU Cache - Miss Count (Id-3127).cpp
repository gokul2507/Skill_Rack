#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int n,c,ans=0;
    cin>>c>>n;
    vector<int> z;
    while(n--){
        int t;
        cin>>t;
        auto p=find(z.begin(),z.end(),t);
        if(p!=z.end())
            z.erase(p);
        else{
            if(z.size()>=c)
                z.erase(z.begin());
            ans++;
        }
        z.push_back(t);
    }
    cout<<ans;
}