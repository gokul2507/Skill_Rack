#include <bits/stdc++.h>
using namespace std;
int ans;
unordered_map<int,int> d;
unordered_map<int,unordered_set<int>> z;
void fun(int x){
    if(d.find(x)!=d.end())
    return;
    d[x]=1;
    ans++;
    if(z.find(x)!=z.end()){
        unordered_set<int> temp=z.find(x)->second;
        for(int i:temp)
            fun(i);
    }
}
int main(int argc, char** argv)
{
    int n;
    cin>>n;
    for(int i=0;i<n;i++){
        int x,y;
        cin>>x>>y;
        if(z.find(x)==z.end()){
            unordered_set<int> temp;
            temp.insert(y);
            z[x]=temp;
        }
        else{
            unordered_set<int> t=z.find(x)->second;
            t.insert(y);
            z[x]=t;
        }
        if(z.find(y)==z.end()){
            unordered_set<int> temp;
            temp.insert(x);
            z[y]=temp;
        }
        else{
            unordered_set<int> t=z.find(y)->second;
            t.insert(x);
            z[y]=t;
        }
    }
    int k;
    cin>>k;
    fun(k);
    cout<<ans;
}
