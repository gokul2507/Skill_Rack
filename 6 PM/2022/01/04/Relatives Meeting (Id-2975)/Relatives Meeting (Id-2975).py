a=int(input())
d={}
for i in range(a):
    x,y=map(int,input().split())
    t=d.get(x,set())
    t.add(y)
    d[x]=t
    t=d.get(y,set())
    t.add(x)
    d[y]=t
k=int(input())
ans={}
def fun(x):
    global ans
    if ans.get(x,0):
        return
    ans[x]=1
    for i in d.get(x,[]):
        fun(i)
fun(k)
print(len(ans))
