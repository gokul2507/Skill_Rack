a=int(input())
z=list(map(int,input().split()))
def fun(l,r):
    if l+1==r:
        return 1
    m=min(z[l:r])
    if m>=r-l:
        return r-l
    ans=m
    ii=-1
    for i in range(l,r):
        z[i]-=m
        if z[i]>0:
            if ii==-1:
                ii=i
        else:
            if ii!=-1:
                tep=min(i-ii,fun(ii,i))
                ans+=tep
            ii=-1
    if ii!=-1:
        tep=min(r-ii,fun(ii,r))
        ans+=tep
    return min(r-l,ans)
print(min(a,fun(0,a)))
