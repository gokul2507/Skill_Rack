n,m=map(int,input().split())
z=[input().strip() for _ in range(m)]
t=int(input())
for _ in range(t):
    z.remove(input().strip())
print(*z[:n],sep='\n')