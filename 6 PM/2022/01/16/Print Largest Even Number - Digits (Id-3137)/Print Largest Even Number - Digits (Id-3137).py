s=sorted(map(str,input().strip()),reverse=1)
if int(''.join(s))%2==0:
    print(''.join(s))
else:
    for i in range(len(s)-1,-1,-1):
        if int(s[i])%2==0:
            t=s.pop(i)
            break 
    s.append(t)
    print(''.join(s))
