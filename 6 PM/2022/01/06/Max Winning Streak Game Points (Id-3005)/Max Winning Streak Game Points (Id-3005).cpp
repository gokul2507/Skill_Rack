#include <bits/stdc++.h>
using namespace std;
int main(int argc, char** argv)
{
    int n;
    cin>>n;
    int ans=0,t=0;
    for(int i=0;i<n;i++){
        int temp;
        cin>>temp;
        if(temp>0){
            t+=temp;
        }
        else{
            ans=max(ans,t);
            t=0;
        }
    }
    cout<<max(ans,t);
}