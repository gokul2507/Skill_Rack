n = int(input())
a = [int(x) for x in input().split()]
c,s = 0,0
for i in a:
    if i > 0:
        s += i
    else:
        s = 0
    c = max(c,s)

print(c)