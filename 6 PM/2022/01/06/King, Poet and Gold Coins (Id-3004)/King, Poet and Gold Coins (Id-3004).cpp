#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int n;
    cin>>n;
    int x,y;
    cin>>x>>y;
    y=max(y,x);
    n-=2;
    while(n--){
        int t;
        cin>>t;
        t=max(x+t,y);
        x=y;
        y=t;
    }
    cout<<y;
}
