#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    string s;
    int n;
    cin>>s>>n;
    if(s.size()%n)
    s.insert(s.size(),n-s.size()%n,'*');
    for(int i=0;i<s.size()/n;i++){
        string t=s.substr(i*n,n);
        if(i%2)
        reverse(t.begin(),t.end());
        cout<<t<<endl;
    }
}