#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int n,m;
    cin>>n>>m;
    vector<vector<long long int>> z;
    z.resize(n,vector<long long int>(m,1));
    for(int i=1;i<n;i++){
        for(int j=1;j<m;j++){
            z[i][j]=z[i-1][j]+z[i][j-1];
        }
    }
    cout<<z[n-1][m-1];
}