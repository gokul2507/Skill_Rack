#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int n;
    cin>>n;
    vector<vector<int>> z;
    z.resize(n,vector<int>(n,1));
    for(int i=0;i<n;i++)
    for(int j=0;j<n;j++){
        int t;
        cin>>t;
        z[i][j]=t;
    }
    int sr=0,sc=0,ec=n-1,er=n-1;
    while(sr<=er){
        for(int i=sc;i<=ec;i++)
        cout<<z[sr][i]<<" ";
        sr++;
        for(int i=sr;i<=er;i++)
        cout<<z[i][ec]<<" ";
        ec--;
        for(int i=ec;i>=sc;i--)
        cout<<z[er][i]<<" ";
        er--;
        for(int i=er;i>=sr;i--)
        cout<<z[i][sc]<<" ";
        sc++;
    }
}
