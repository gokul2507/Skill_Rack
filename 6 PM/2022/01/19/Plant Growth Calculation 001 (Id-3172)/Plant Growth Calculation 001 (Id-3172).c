#include <stdio.h>
long long int prime(long long int n){ 
    if(n<2) return 0;
    long long int i; 
    for(i=2;i<=n/2;i++) 
    {   
        if(n%i==0)
        return 0;
    }
    return 1; 
}
long long int preprime(long long int x){
    x--;
    while(x){
        if(prime(x))
        break;
        x--;
    }
    return x;
}
int main(){
    long long int a,b,month;   
    scanf("%lld %lld",&a,&b);  
    long long int m[a][b];  
    for(long long int i=0;i<a;i++)
    {  
        for(long long int j=0;j<b;j++)
        {  
            scanf("%lld",&m[i][j]);
        }  
    }  
    scanf("%lld",&month);
    while(month>0){  
        for(long long int i=0;i<a;i++)
        { 
            long long int left=0;   
            for(long long int j=0;j<b;j++)
            {
                long long int num=m[i][j];
                if(prime(num))
                {
                    num+=num-preprime(num); 
                }    
                else
                {  
                    long long int right=0;   
                    if(j<b-1)   
                    right=m[i][j+1]%10;  
                    num=num+(left%10)+right;   
                }       
                left=m[i][j]; 
                m[i][j]=num; 
            }   
        }  
        month--; 
    }  
    for(long long int k=0;k<a;k++){   
        for(long long int j=0;j<b;j++){
            printf("%lld ",m[k][j]);  
        }printf("\n"); 
    }
}