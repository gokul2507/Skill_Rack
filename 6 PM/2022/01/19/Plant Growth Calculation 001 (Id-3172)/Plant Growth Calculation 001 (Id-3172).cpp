#include <bits/stdc++.h>
#define lld long long int
using namespace std;
long long int prime(long long int x){
    if(x==0 || x==1) return 0;
    if(x==2) return 1;
    for(long long int i=2;i<=sqrt(x);i++){
        if(x%i==0) return 0;
    }
    return 1;
}
long long int lprime(long long int x){
    x--;
    if(x<2) return 0;
    while(!prime(x)) x--;
    return x;
}
int main(int argc, char** argv)
{
    long long int r,c,x,n;
    cin>>r>>c;
    vector<vector<long long int>> v;
    for(long long int i=0;i<r;i++){
        vector<long long int> t;
        for(long long int j=0;j<c;j++){
            cin>>x;
            t.push_back(x);
        }
        v.push_back(t);
    }
    cin>>n;
    if(c==1){
        for(int i=0;i<n;i++){
            for(int j=0;j<r;j++){
                if(prime(v[j][0])) v[j][0]+=v[j][0]-lprime(v[j][0]);
            }
        }
        for(int i=0;i<r;i++){
            cout<<v[i][0]<<endl;
        }
        return 0;
    }
    for(long long int tp=0;tp<n;tp++){
        for(long long int i=0;i<r;i++){
            long long int prev=v[i][0],temp=v[i][0];
            if(prime(v[i][0])) v[i][0]+=v[i][0]-lprime(v[i][0]);
            else v[i][0]+=v[i][1]%10;
            for(long long int j=1;j<c-1;j++){
                prev=v[i][j];
                if(prime(v[i][j])){
                    v[i][j]+=v[i][j]-lprime(v[i][j]);
                }
                else{
                    v[i][j]+=(temp%10+v[i][j+1]%10);
                }
                temp=prev;
            }
            if(prime(v[i][c-1])) v[i][c-1]+=v[i][c-1]-lprime(v[i][c-1]);
            else v[i][c-1]+=prev%10;
        }
    }
    for(long long int i=0;i<r;i++){
        for(long long int j=0;j<c;j++){
            cout<<v[i][j]<<" ";
        }
        cout<<endl;
    }
}
