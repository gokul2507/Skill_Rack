n,x,c=int(input()),'',0
for i in range(n):  x+=''.join(list(set(input().strip())))
for i in set(x): 
    if x.count(i)==n: c+=1 
print(c)


d,n={},int(input())
for i in range(n):
    for j in input().strip():
        if d.get(j,0)==i:
            d[j]=i+1
print(sum([1 if d[i]==n else 0 for i in d]))


n=int(input())
l=list(input().strip() for _ in range(n))
d,c={},0
for i in range(len(l)):
    for j in set(l[i]):
        d[j]=d.get(j,0)+1
for i in d.values():
    if i==n:
        c+=1
print(c)