from math import gcd
n=int(input())
l=list(map(int,input().split()))
g=l[0]
for i in l[1:]:
    g=gcd(g,i)
print(g)