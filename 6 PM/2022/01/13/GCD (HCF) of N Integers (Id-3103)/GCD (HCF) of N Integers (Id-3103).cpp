#include <bits/stdc++.h>
using namespace std;
int gcd(int a,int b){
    (b==0)?a:gcd(b,a%b);
}
int main(int argc, char** argv)
{
    int n,a;
    cin>>n>>a;
    n--;
    while(n--){
        int t;
        cin>>t;
        a=gcd(t,a);
    }
    cout<<a;
}
