s=input().strip()
d={}
for i in s:
    d[i]=d.get(i,0)+1 
d=sorted(d.items(),key=lambda i:(-i[1],i[0]))
for i in d:
    print(i[0],i[1],sep='')
