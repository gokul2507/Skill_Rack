n=int(input())
s1,s2,s3="","",""
for i in range(1,n+1):
    s=input().strip()
    s1+=s[:i]
    s3+=s[-i:]
    s2+=s[i:-i]
print(s1,s2,s3,sep='\n')