#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int r,c;
    cin>>r>>c;
    while(r--){
        int s=0,t;
        for(int i=0;i<c;i++){
            cin>>t;
            s+=t;
        }
        cout<<s<<endl;
    }
}
