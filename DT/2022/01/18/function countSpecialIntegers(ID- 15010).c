#include <stdio.h>
#include <stdlib.h>
int fun(int a,int x, int y){
    int p=0,q=0;
    while(a){
        if(a%10==x)
        p=1;
        if(a%10==y)
        q=1;
        a/=10;
    }
    return p*q;
}
int countSpecialIntegers(int L, int R, int A, int B)
{
    int ans=0;
    while(L<=R){
        if(L%A==0 && L%B==0 && fun(L,A,B)){
            ans++;
        }
        L++;
    }
    return ans;
}
int main()
{
    int L, R, A, B;
    scanf("%d %d %d %d", &L, &R, &A, &B);
    printf("%d", countSpecialIntegers(L, R, A, B));
    return 0;
}
