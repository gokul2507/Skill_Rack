#include <stdio.h>
#include <stdlib.h>

struct Duration
{
     int days;
     int hours;
     int minutes;
     int seconds;
};
long long int d = 0, h = 0, m = 0, se = 0;
void fun(char *s)
{
     int t = 0;
     for (int i = 0; s[i] != '\0'; i++)
     {
          if (s[i] == ':')
          {
               if (i == 2)
                    h += t;
               else
                    m += t;
               t = 0;
          }
          else
          {
               t *= 10;
               t += s[i] - '0';
          }
     }
     se += t;
}
struct Duration *getTotalDuration(char *str)
{
     struct Duration *ans = malloc(sizeof(struct Duration));
     int t = 0;
     for (int i = 0; str[i] != '\0'; i++)
          if (str[i] == ' ')
          {
               str[i] = '\0';
               fun(str + t);
               t = i + 1;
          }
     fun(str + t);
     m += se / 60;
     se %= 60;
     h += m / 60;
     m %= 60;
     d += h / 24;
     h %= 24;
     ans->days = d;
     ans->hours = h;
     ans->minutes = m;
     ans->seconds = se;
     return ans;
}

int main()
{
     char str[1001];
     scanf("%[^\n]", str);
     struct Duration *duration = getTotalDuration(str);
     if (duration == NULL)
     {
          printf("Duration is not formed\n");
     }
     printf("%d %d %d %d", duration->days, duration->hours, duration->minutes, duration->seconds);
     return 0;
}
