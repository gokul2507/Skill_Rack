#include <stdio.h>
#include <stdlib.h>
 
int bin(int x){
    if(x==0)
    return 0;
    return (bin(x/2)*10)+(x%2);
}
int getBinaryAsDecimalSum(int SIZE, int *arr)
{
    long long int y=0;
    for(int i=0;i<SIZE;i++)
    y+=bin(arr[i]);
 return y;   
}
int main()
{
    int N;
    scanf("%d", &N);
    int arr[N];
    for(int index = 0; index < N; index++)
    {
        scanf("%d", &arr[index]);
    }
    printf("%d", getBinaryAsDecimalSum(N, arr));
    return 0;
}
