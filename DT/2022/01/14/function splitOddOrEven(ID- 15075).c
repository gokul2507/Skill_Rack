#include <stdio.h>
#include <stdlib.h>

typedef struct BoundedArray
{
    int SIZE;
    int *arr;
} boundedArray;
int l=0,h=0;
int len(int x){
    int p=0;
    while(x){
        p++;
        x/=10;
    }
    return p;
}
void half(int x){
    h=0;
    l=0;
    int le=pow(10,len(x)/2);
    h=x%le;
    l=x/le;
}
boundedArray* splitOddOrEven(int SIZE, int *arr)
{
    boundedArray* ans=malloc(sizeof(struct BoundedArray));
    ans->arr=malloc(sizeof(int [SIZE+SIZE]));
    ans->SIZE=0;
    for(int i=0;i<SIZE;i++){
        half(arr[i]);
        if(l%2==h%2){
            ans->arr[ans->SIZE++]=l;
            ans->arr[ans->SIZE++]=h;
        }
        else{
            ans->arr[ans->SIZE++]=arr[i];
        }
    }
    for(int i=0;i<ans->SIZE;i++){
        for(int j=i+1;j<ans->SIZE;j++){
            if(ans->arr[i]>ans->arr[j]){
                int t=ans->arr[i];
                ans->arr[i]=ans->arr[j];
                ans->arr[j]=t;
            }
        }
    }
    return ans;
}

int main()
{
    int N;
    scanf("%d", &N);
    int arr[N];
    for(int index = 0; index < N; index++)
    {
        scanf("%d", &arr[index]);
    }
    boundedArray *bArr = splitOddOrEven(N, arr);
    if(bArr == NULL)
    {
        printf("Array is not formed\n");
    }
    if(bArr->SIZE <= 0)
    {
        printf("Invalid size for the array\n");
    }
    for(int index = 0; index < bArr->SIZE; index++)
    {
        printf("%d ", bArr->arr[index]);
    }
    return 0;
}
