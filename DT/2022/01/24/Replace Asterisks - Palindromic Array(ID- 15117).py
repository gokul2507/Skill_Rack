n=int(input())
s=input().split()
i,j=0,n-1
while i<=j:
    if s[i]=='*':
        if s[j]=='*':
            s[i],s[j]='1','1'
        else:
            s[i]=s[j]
    elif s[j]=='*':
        s[j]=s[i]
    elif s[i]!=s[j]:
        print(-1)
        exit()
    i+=1
    j-=1
print(*s)
