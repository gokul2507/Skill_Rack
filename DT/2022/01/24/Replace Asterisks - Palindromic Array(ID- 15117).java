import java.util.*;
public class Hello {

    public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		int n=s.nextInt();
		String[] arr=new String[n];
		for(int i=0;i<n;i++)
		    arr[i]=s.next();
	    int i=0,j=n-1;
	    while(i<=j) {
	        if((!arr[i].equals("*")) && (!arr[j].equals("*"))) {
	            if(!arr[i].equals(arr[j])) {
	                System.out.println(-1);
	                return;
	            }
	        }
	        else if(arr[i].equals("*") && arr[j].equals("*")) {
	            arr[i]="1";
	            arr[j]="1";
	        }
	        else if(arr[i].equals("*") && (!arr[j].equals("*"))) 
	            arr[i]=arr[j];
            else if((!arr[i].equals("*")) && arr[j].equals("*"))
                arr[j]=arr[i];
            i++;
            j--;
	    } 
	    for(i=0;i<n;i++)
	        System.out.print(arr[i]+" ");

	}
}