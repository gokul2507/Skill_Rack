#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int N;
    cin >> N;
    vector<string> a(N);
    for(string &i : a) cin >> i;
    int l=0, h=N-1;
    while(l<=h){
        if(a[l]=="*" && a[h]=="*"){
            a[l]="1", a[h]="1";
        }else if(a[l]=="*" ){
            a[l]=a[h];
        }else if(a[h]=="*"){
            a[h]=a[l];
        }else if(a[h]!=a[l]){
            cout << -1;
            return 0;
        }
        l++, h--;
    }
    for(string i : a) cout << i << " ";
}