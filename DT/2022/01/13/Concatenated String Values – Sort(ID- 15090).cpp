#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    string a,b;
    cin>>a>>b;
    vector<string> ans;
    for(char ch = 'a';ch<='z';ch++){
        if(a.find(ch)!=-1){
            int ind1 = a.find(ch),ind2 = b.find(ch);
            ans.push_back(a.substr(0,ind1+1)+b.substr(ind2+1));
        }
    }
    sort(ans.begin(),ans.end());
    for(auto &i: ans)cout<<i<<endl;
}