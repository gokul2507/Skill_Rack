#include<stdio.h>
#include<stdlib.h>
char matrix[1001][1001];
int startRow=1001,endRow=-1001,startCol=1001,endCol=-1001;
void fillMatrix(char* str,int row,int col,int X,int Y){
    for(int index=0;str[index];index++){
        matrix[row][col]=str[index];
        if(row<startRow){
            startRow=row;
        }
        if(row>endRow){
            endRow=row;
        }
        if(col<startCol){
            startCol=col;
        }
        if(col>endCol){
            endCol=col;
        }
        row+=X;
        col+=Y;
    }
}
int main()
{
    char S1[101],S2[101],S3[101],S4[101];
    scanf("%s\n%s\n%s\n%s",S1,S2,S3,S4);
    for(int row=0;row<1001;row++){
        for(int col=0;col<1001;col++){
            matrix[row][col]='*';
        }
    }
    fillMatrix(S1,101,101,-1,-1);
    fillMatrix(S2,101,102,-1,1);
    fillMatrix(S3,102,101,1,-1);
    fillMatrix(S4,102,102,1,1);
    for(int row=startRow;row<=endRow;row++){
        for(int col=startCol;col<=endCol;col++){
            printf("%c",matrix[row][col]);
        }
        printf("\n");
    }
}