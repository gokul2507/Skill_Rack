#include <stdio.h>
#include <stdlib.h>

int isCaseBalancedString(char *str)
{
    int f=-1,s=-1;
    for(int i=0;str[i]!='\0';i++){
        int t=tolower(str[i])-'a';
        if(t<('n'-'a')){
            if(f==-1){
                f=isupper(str[i]);
            }
            if(f!=isupper(str[i]))
            return 0;
        }
        else{
            if(s==-1)
            s=isupper(str[i]);
            if(s!=isupper(str[i]))
            return 0;
        }
    }
    if(s==f)
    return 0;
    return 1;
}

int main()
{
    char str[101];
    scanf("%s", str);
    printf("%d", isCaseBalancedString(str));
    return 0;
}
