def fun(x,y):
    if y==3:
         return oct(x)[2:]
    return hex(x)[2:].upper()
a=bin(int(input()))[2:]
l=[3,4]
s=""
while a:
    t=fun(int(a[-l[0]:],2),l[0])
    a=a[:-l[0]]
    l=l[::-1]
    s=t+s
print(s)