#include <stdio.h>
#include <stdlib.h>

int main()
{
     int N, mul = 3, c = 0, i;
     scanf("%d", &N);
     char arr[10];
     while (arr[c++] = (i = N & ((1 << mul) - 1)) < 10 ? '0' + i : 'A' + (i - 10),
            N >>= mul,
            mul ^= 7,
            N)
          ;
     while (printf("%c", arr[--c]), c)
          ;
}

#include <stdio.h>
#include <stdlib.h>

int main()
{
     int n, a[1001], i = 0, k = 0, f = 1;
     char r[1001];
     scanf("%d", &n);
     while (n > 0)
     {
          a[i++] = n % 2;
          n /= 2;
     }
     for (int j = 0; j < i;)
     {
          if (f)
          {
               if (a[j + 2] < i)
                    r[k++] = (a[j] * 1 + a[j + 1] * 2 + a[j + 2] * 4) + '0';
               else
               {
                    if (a[j + 1] < i)
                         r[k++] = (a[j] * 1 + a[j + 1] * 2) + '0';
                    else
                         r[k++] = (a[j] * 1) + '0';
               }
               j += 3;
               f = !f;
          }
          else
          {
               if (a[j + 1] >= i)
                    a[j + 1] = 0;
               if (a[j + 2] >= i)
                    a[j + 2] = 0;
               if (a[j + 3] >= i)
                    a[j + 3] = 0;
               int x = (a[j] * 1 + a[j + 1] * 2 + a[j + 2] * 4 + a[j + 3] * 8);
               if (x > 9)
                    r[k++] = x + 55;
               else
                    r[k++] = x + '0';
               j += 4;
               f = !f;
          }
     }
     for (int j = k - 1; j >= 0; j--)
          printf("%c", r[j]);
}