import java.util.*;
public class Hello {

    public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		int n=s.nextInt();
		String str=Integer.toBinaryString(n);
		while(str.length()%7!=0) {
		    str="0"+str;
		}
		String s1="";
		for(int i=0;i<str.length();i+=7) {
		    s1+=Integer.toHexString(Integer.parseInt(str.substring(i,i+4),2));
		    s1+=Integer.toOctalString(Integer.parseInt(str.substring(i+4,i+7),2));
		}
		int flag=0;
		for(int i=0;i<s1.length();i++) {
		    if(flag==0 && s1.charAt(i)=='0') {}
		    else {
		        if(Character.isLetter(s1.charAt(i)))
		            System.out.print(Character.toUpperCase(s1.charAt(i)));
	            else 
	                System.out.print(s1.charAt(i));
                flag=1;
		    }
		}

	}
}