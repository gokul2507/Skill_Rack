#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int N;
     cin >> N;
     string ans = "";
     while (N)
     {
          int a = N & ((1 << 3) - 1);
          ans += a + '0';
          N >>= 3;
          if (N)
          {
               int b = N & ((1 << 4) - 1);
               char num[100];
               sprintf(num, "%x", b);
               string t = num;
               ans += num;
               N >>= 4;
          }
     }
     for (int i = ans.size() - 1; i >= 0; i--)
     {
          if (isalpha(ans[i]))
               cout << (char)toupper(ans[i]);
          else
               cout << ans[i];
     }
}

