#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int N;
    cin  >> N;
    vector<vector<int >> arr(N, vector<int>(2));
    for(int i=0; i<N; i++){
        cin >> arr[i][0] >> arr[i][1];
    }
    sort(arr.begin(), arr.end(), [](auto &a, auto &b){
        if(a[1]!=b[1]) return a[1]<b[1];
        return a[0]<b[0];
    });
    for(auto i : arr){
        cout << i[0] << " " << i[1] << endl;
    }
}