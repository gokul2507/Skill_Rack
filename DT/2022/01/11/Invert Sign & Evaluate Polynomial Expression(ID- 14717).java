import java.util.*;
public class Hello {

    public static void main(String[] args) {
		Scanner io = new Scanner(System.in);
		
		char[] str = io.nextLine().toCharArray();
		
		int X = io.nextInt();
		
		int strSize = str.length;
		for(int index = 0; index < strSize; index ++) {
		    if(str[index] == '-') {
		        str[index] = '+';
		    }
		    else if(str[index] == '+') {
		        str[index] = '-';
		    }
		}
		
		int res = 0, num = 0, powerOfX = 0, sign = 1;
		
		for(int index = 0; index < strSize; index ++) {
		    if(str[index] == 'x') {
		        powerOfX = 0;
		        index += 2;
		        while(index < strSize && str[index] != '+' && str[index] != '-') {
		            powerOfX = (powerOfX * 10) + (str[index ++] - '0');
		        }
		        res += (sign * num * (int)Math.pow(X, powerOfX));
		        num = 0;
		        -- index;
		    }
		    else if(str[index] == '-') {
		        sign = -1;
		    }
		    else if(str[index] == '+') {
		        sign = 1;
		    }
		    else if(Character.isDigit(str[index])) {
		        num = (num * 10) + (str[index] - '0');
		    }
		}
		
		System.out.println(String.valueOf(str));
		System.out.println(res);
		
	}
}