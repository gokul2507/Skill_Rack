#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    string str;
    cin>>str;str+='+';
    for(char &ch: str){
        if(ch=='+')ch='-';
        else if(ch=='-')ch='+';
    }
    int x ;cin>>x;
    int ans = 0 ,p=0;
    int pre = 0 ;
    bool sign = 0,flag = 0;
    for(char ch: str){
        if(ch=='+' || ch=='-'){
            if(sign)pre = -pre;
            ans+=pre*pow(x,p);
            pre = 0;
            sign = ch=='-';
            flag = 0;
        }
        else if(isdigit(ch)){
            if(flag){
                p = ch-'0';
            }
            else
                pre = pre*10+ch-'0';
        }
        else
            flag = 1;
    }
    str.pop_back();
    cout<<str<<endl<<ans;
}