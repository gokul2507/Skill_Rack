#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    #define int long long
    int n;cin>>n;
    map<int,int> mp;
    for(int i = 0 ;i<n;i++){
        string str;cin>>str;
        int bs = *max_element(str.begin(),str.end())-'0'+1;
        mp[bs]+=stoi(str,0,bs);
    }
    vector<int> ans;
    for(auto i: mp)ans.push_back(i.second);
    sort(ans.begin(),ans.end());
    for(int i: ans)cout<<i<<" ";
}