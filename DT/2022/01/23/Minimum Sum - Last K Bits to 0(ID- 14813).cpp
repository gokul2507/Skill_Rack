#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    #define int long long
    int n;cin>>n;
    int arr[n];
    for(int i = 0 ;i<n;i++)cin>>arr[i];
    int k ;cin>>k;
    int lm = (1<<k)-1;
    int mx = 0;
    for(int i: arr){
        mx = max(mx,i&lm);
    }
    cout<<accumulate(arr,arr+n,0ll)-mx;
}