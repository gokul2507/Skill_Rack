#include <stdio.h>
#include <stdlib.h>

int main()
{
     size_t N, sum = 0, ans = 1 << 31;
     scanf("%llu", &N);
     size_t arr[N], i = 0;
     while (scanf("%llu", arr + i), sum += arr[i], ++i < N)
          ;
     size_t K;
     scanf("%llu", &K);
     K = ((1 << (31 - K)) - 1) << K;
     for (i = 0; i < N; i++)
          ans = fmin(ans, sum - arr[i] + (arr[i] & K));
     return printf("%llu", ans);
}