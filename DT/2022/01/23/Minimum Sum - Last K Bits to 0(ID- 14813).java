import java.util.*;
public class Hello {

    public static void main(String[] args) {
		//Your Code Here
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		int[] a = new int[n];
		
		long sum = 0;
		
		for(int i=0;i<n;i++)
		{
		    a[i] = scan.nextInt();
		    sum+=a[i];
		}
		
		int k = scan.nextInt();
		long min = Long.MAX_VALUE, temp = sum;
		
		for(int i=0;i<n;i++)
		{
		    String t = Integer.toBinaryString(a[i]);
		    t = t.substring(0,Math.max(0,t.length()-k));
		    t = t + "0".repeat(k);
		    
		    temp = sum - a[i] + Integer.parseInt(t,2);
		  //  System.out.println(t+" "+temp+" "+(sum-a[i])+" "+sum);
		    min = Math.min(min,temp);
		    
		}
		
		System.out.print(min);

	}
}