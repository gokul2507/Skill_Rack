a=input()
z=[["*" for _ in range(len(a))] for _ in range(9)]
p=0
for i,j in zip(a,input().strip()):
    i,j=int(i),int(j)
    for k in range(i,i+j):
        z[k][p]='-'
    p+=1
for i in z:
    print(*i[:-1],sep='')
    
l=[list(map(int,input().strip())) for i in range(3)]
m,x=[],''
for i in list(zip(*l)):
    x="*"*i[-1]+"-"*i[-2]+"*"*i[0] 
    m.append(list(x))
for i in list(zip(*m))[::-1]:
    print(*i,sep='')