#include <stdio.h>
#include <stdlib.h>

typedef struct BoundedArray
{
     int SIZE;
     int *arr;
} boundedArray;

boundedArray *getPositionDigitsSum(int SIZE, int *arr)
{
     boundedArray *ans = malloc(sizeof(struct BoundedArray));
     ans->SIZE = 0;
     ans->arr = malloc(sizeof(int[100]));
     while (1)
     {
          int p = 0, t = 0;
          for (int i = 0; i < SIZE; i++)
          {
               if (arr[i])
               {
                    t = 1;
                    p += arr[i] % 10;
                    arr[i] /= 10;
               }
          }
          if (t)
               ans->arr[ans->SIZE++] = p;
          else
               break;
     }
     for (int i = 0, j = ans->SIZE - 1; i < j; i++, j--)
     {
          int t = ans->arr[i];
          ans->arr[i] = ans->arr[j];
          ans->arr[j] = t;
     }
     return ans;
}
int main()
{
     int N;
     scanf("%d", &N);
     int arr[N];
     for (int index = 0; index < N; index++)
     {
          scanf("%d", &arr[index]);
     }
     boundedArray *bArr = getPositionDigitsSum(N, arr);
     if (bArr == NULL || bArr->arr == NULL || bArr->arr == arr)
     {
          printf("Array is not formed\n");
     }
     if (bArr->SIZE <= 0)
     {
          printf("Invalid size for the array\n");
     }
     for (int index = 0; index < bArr->SIZE; index++)
     {
          printf("%d ", bArr->arr[index]);
     }
     return 0;
}
