import java.util.*;
public class Hello {

    public static void main(String[] args) {
		//Your Code Here
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		int a[][]=new int[n][n];
		for(int i=0;i<n;i++){
		    for(int j=0;j<n;j++){
		        a[i][j]=sc.nextInt();
		    }
		}
		int m=sc.nextInt();
		int b[][]=new int[m][m];
		for(int i=0;i<m;i++){
		    for(int j=0;j<m;j++){
		        b[i][j]=sc.nextInt();
		    }
		}
		int k=m/n;
		for(int i=0,p=0;i<m;i+=k,p++){
		    for(int j=0,q=0;j<m;j+=k,q++){
		        for(int x=i;x<k+i;x++){
		            for(int y=j;y<k+j;y++){
		                if(b[x][y]!=a[p][q]){
		                    System.out.println("No");
		                    return;
		                }
		            }
		        }
		    }
		}
		System.out.println("Yes");

	}
}