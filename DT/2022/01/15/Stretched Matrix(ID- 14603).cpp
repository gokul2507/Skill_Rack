#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int N, X;cin >>N;
    vector<vector<int>> m1(N, vector<int>(N));
    for(auto &i : m1) for(auto &j : i)cin>> j;
    cin >> X;
    vector<vector<int>> m2(X, vector<int>(X));
    for(auto &i : m2) for(auto &j : i) cin >> j;
    int R= X/N;
    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            int val = m1[i][j];
            for(int k=i*R; k<(i+1)*R; k++){
                for(int l=j*R; l<(j+1)*R; l++){
                    if(val!=m2[k][l]){
                        cout <<"No";
                        return 0;
                    }
                }
            }
        }
    }
    cout << "Yes";
}