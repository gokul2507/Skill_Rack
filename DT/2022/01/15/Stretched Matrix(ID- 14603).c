#include<stdio.h>
#include<stdlib.h>

int main()
{
    int N;
    scanf("%d",&N);
    int matrix[N][N];
    for(int row=0;row<N;row++){
        for(int col=0;col<N;col++){
            scanf("%d",&matrix[row][col]);
        }
    }
    int M;
    scanf("%d",&M);
    int stretchedMat[M][M];
    for(int row=0;row<M;row++){
        for(int col=0;col<M;col++){
            scanf("%d",&stretchedMat[row][col]);
        }
    }
    int X=0,Y=0;
    for(int row=0;row<M;row+=M/N){
        for(int col=0;col<M;col+=M/N){
            for(int sRow=row;sRow<row+M/N;sRow++){
                for(int sCol=col;sCol<col+M/N;sCol++){
                    if(stretchedMat[sRow][sCol]!=matrix[X][Y]){
                        printf("No");
                        return 0;
                    }
                }
            }
            Y++;
        }
        X++;
        Y=0;
    }
    printf("Yes");
    return 0;
}