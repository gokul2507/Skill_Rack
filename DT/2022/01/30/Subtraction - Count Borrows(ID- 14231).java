import java.util.*;
public class Hello {

    public static void main(String[] args) {
	
	Scanner sk = new Scanner(System.in);
	int x = sk.nextInt(), y = sk.nextInt();
	int count = 0, borrow = 0;
	while(x > 0 || y > 0)
	{
	    int top = x % 10;
	    int down = y % 10;
	    if(top - borrow < down)
	    {
	        borrow = 1;
	        count ++;
	    }else
	    {
	        borrow = 0;
	    }
	    x /= 10;
	    y /= 10;
	}
	System.out.print(count);

	}
}