#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int a,b;
    cin>>a>>b;
    int ans = 0;
    bool borrow = 0;
    while(b || borrow){
        if(borrow && a%10){
            a--;borrow =0;
        }
        if(a%10<b%10 || borrow){
            ans++;
            borrow = 1;
        }
        a/=10;
        b/=10;
    }
    cout<<ans;
}