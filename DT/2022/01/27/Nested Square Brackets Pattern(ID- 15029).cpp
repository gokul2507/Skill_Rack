#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int N;
    cin >> N;
    while(N--){
        int r, c;
        cin >> r >> c;
        cout << "[";
        int ctr=1;
        for(int i=1; i<=r; i++){
            cout << "[";
            for(int j=1; j<=c-1; j++){
                cout << ctr << " ";
                ctr++;
            }
            
            cout << ctr<< "]";
            if(i!=r) cout << " ";
            ctr++;
        }
      
        cout << "]" << endl;
    }
}