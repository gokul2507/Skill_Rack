import java.util.*;
public class Hello {

    public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		int n=s.nextInt();
		for(int i=0;i<n;i++) {
		    int a=s.nextInt();
		    int b=s.nextInt();
		    System.out.print("[");
		    for(int j=0;j<a;j++) {
		        System.out.print("[");
		        for(int k=(j*b)+1;k<(j*b)+1+b;k++) {
		            System.out.print(k);
		            if(k!=(j*b)+b)
		                System.out.print(" ");
		        }
	            System.out.print("]");
	            if(j!=a-1)
	                System.out.print(" ");
		    }
		    System.out.println("]");
		}

	}
}