def fun(x):
    x,y,f=int(x[0]),int(x[-1]),0
    if x>y:
        x,y,f=y,x,1
    x=list(range(x,y+1))
    if f:
        x=x[::-1]
    return int("".join(map(str,x)))
input()
print(*sorted(map(fun,input().split())))