#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    #define int long long 
    int n;cin >> n;
    vector<int> ans(n);
    for(int i=0; i<n; i++){
        string val ;
        cin >> val;
        string tmp="";
        if(val.back()>val[0]){
            for(char c=val[0]; c<=val.back(); c++) tmp+=c;
        }else{
            for(char c=val[0]; c>=val.back(); c--) tmp+=c;
        }
        ans[i]=stoll(tmp);
    }
    sort(ans.begin(), ans.end());
    for(int i : ans) cout << i<< " ";
}