#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int N;
     cin >> N;
     vector<int> arr(N);
     for (int &i : arr)
          cin >> i;
     int i;
     for (i = N - 1; i >= 0; i -= 2)
          cout << arr[i] << " ";
     i = N & 1 ? 1 : 0;
     for (i; i < N; i += 2)
          cout << arr[i] << " ";
}