import java.util.*;
public class Hello {

    public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		int n=s.nextInt();
		int[] arr=new int[n];
		for(int i=0;i<n;i++)
		    arr[i]=s.nextInt();
	    for(int i=n-1;i>=0;i-=2)
	        System.out.print(arr[i]+" ");
        if(n%2==0) {
            for(int i=0;i<n;i+=2)
                System.out.print(arr[i]+" ");
        }
        else {
            for(int i=1;i<n;i+=2)
                System.out.print(arr[i]+" ");
        }

	}
}