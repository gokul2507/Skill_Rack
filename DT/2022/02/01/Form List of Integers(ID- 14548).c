#include <stdio.h>
#include <stdlib.h>

int main()
{
     int N;
     scanf("%d\n", &N);
     int arr[N], ind;
     for (ind = 0; ind < N; ind++)
     {
          scanf("%d ", &arr[ind]);
     }
     for (ind = N - 1; ind >= 0; ind -= 2)
     {
          printf("%d ", arr[ind]);
     }
     for (ind = N % 2; ind < N; ind += 2)
     {
          printf("%d ", arr[ind]);
     }
}