#include <stdio.h>
#include <stdlib.h>

int main()
{
     long long int n, c = 1, a[100001], ai = 1;
     scanf("%lld", &n);
     a[0] = 1;
     while (ai < n)
     {
          long long int t = ai;
          a[ai++] = pow(7, c);
          long long int temp = pow(7, c);
          c++;
          for (int i = 0; i < t; i++)
               a[ai++] = a[i] + temp;
     }
     for (int i = 0; i < n; i++)
          printf("%lld ", a[i]);
}