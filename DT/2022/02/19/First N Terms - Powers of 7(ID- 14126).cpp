#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
#define ll unsigned long long
     ll N;
     cin >> N;
     vector<ll> ans;
     ll len = 10;
     for (ll i = 1; i < (1 << len); i++)
     {
          ll sum = 0;
          for (ll j = 0; j < len; j++)
          {
               if (i & (1 << j))
               {
                    sum += pow(7, j);
               }
          }
          // if(!sum) sum=1;
          ans.push_back(sum);
     }
     sort(ans.begin(), ans.end());
     for (int i = 0; i < N; i++)
          cout << ans[i] << " ";
}