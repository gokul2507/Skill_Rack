import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner sc = new Scanner(System.in);
          int N = sc.nextInt(), i, j, max = 0, L, r, c;
          char m[][] = new char[N][N];
          for (i = 0; i < N; i++)
               for (j = 0; j < N; m[i][j++] = sc.next().charAt(0))
                    ;
          L = sc.nextInt();
          for (i = 0; i < N - L + 1; i++) {
               int count = 0;
               for (r = i; r < i + L; r++)
                    for (c = 0; c < N; c++)
                         if (m[r][c] == '*')
                              count++;
               max = Math.max(max, count);
               count = 0;
               for (c = i; c < i + L; c++)
                    for (r = 0; r < N; r++)
                         if (m[r][c] == '*')
                              count++;
               max = Math.max(max, count);
          }
          System.out.println(max);
     }
}