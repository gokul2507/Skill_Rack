#include <stdio.h>
#include <stdlib.h>

int main()
{
     int N, L, max = -1;
     scanf("%d\n", &N);
     char mat[N][N];
     for (int i = 0; i < N; i++)
     {
          for (int j = 0; j < N; j++)
          {
               scanf("%c ", &mat[i][j]);
          }
          scanf("\n");
     }
     scanf("%d", &L);
     for (int i = 0; i <= N - L; i++)
     {
          int sumR = 0, sumC = 0;
          for (int j = i; j < i + L; j++)
          {
               for (int k = 0; k < N; k++)
               {
                    if (mat[j][k] == '*')
                         sumR++;
                    if (mat[k][j] == '*')
                         sumC++;
               }
          }
          if (max < sumR)
               max = sumR;
          if (max < sumC)
               max = sumC;
     }
     printf("%d", max);
}