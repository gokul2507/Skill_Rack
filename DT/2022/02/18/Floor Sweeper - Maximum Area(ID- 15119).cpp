#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int N;
     cin >> N;
     vector<vector<char>> mat(N, vector<char>(N));
     for (auto &i : mat)
     {
          for (auto &j : i)
          {
               cin >> j;
          }
     }
     int L;
     cin >> L;
     int ans = -1;
     for (int i = 0; i <= N - L; i++)
     {
          int tot = 0, sum = 0;
          for (int j = i; j < i + L; j++)
          {
               for (int k = 0; k < N; k++)
               {
                    tot += (mat[j][k] == '*');
                    sum += (mat[k][j] == '*');
               }
          }
          ans = max({ans, tot, sum});
     }
     cout << ans;
}