#include <stdio.h>
#include <stdlib.h>
int dec(char t[], char f[][25], char l[][25], int x)
{
     int c = 0, i;
     for (i = 0; i < x; i++)
     {
          if (strcmp(l[i], t) == 0)
          {
               c += dec(f[i], f, l, x) + 1;
               // printf("%s ",f[i]);
          }
     }
     return c;
}
int main()
{
     int x, i, j;
     char t[25], f[55][25], l[55][25];
     scanf("%d\n", &x);
     for (i = 0; i < x; i++)
          scanf("%s %s\n", f[i], l[i]);
     scanf("%s", t);
     printf("%d", dec(t, f, l, x) + 1);
}