#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n;
     string x, y;
     cin >> n;
     map<string, vector<string>> d;
     vector<string> z;
     for (int i = 0; i < n; i++)
     {
          cin >> x >> y;
          vector<string> t;
          if (d.find(y) != d.end())
               t = d[y];
          t.push_back(x);
          d[y] = t;
     }
     int res = 0;
     cin >> x;
     z.push_back(x);
     while (z.size())
     {
          string t = z.back();
          z.pop_back();
          res++;
          if (d.find(t) == d.end())
               continue;
          for (string i : d[t])
               z.push_back(i);
     }
     cout << res;
}