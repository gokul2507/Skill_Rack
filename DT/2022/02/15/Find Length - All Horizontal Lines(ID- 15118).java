import java.util.*;
public class Hello {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt(), i, j, p[][] = new int[N][2], l[] = new int[5000], n=0;
        boolean f = true;
        for(i=0; i<N; p[i][0]=sc.nextInt(), p[i++][1]=sc.nextInt());
        for(i=0; i<N; i++) {
            for(j=i+1; j<N; j++) {
                if(p[i][1]==p[j][1]) {
                    l[n++] = Math.abs(p[i][0]-p[j][0]);
                    f = false;
                }
            }
        }
        if(f) System.out.println(-1);
        Arrays.sort(l,0,n);
        for(i=0; i<n; System.out.print(l[i++]+" "));
	}
}