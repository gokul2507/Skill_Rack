#include <stdio.h>
#include <stdlib.h>

int main()
{
     int n, z = 0;
     scanf("%d", &n);
     int x[n], y[n], d[10001];
     for (int i = 0; i < n; i++)
          scanf("%d%d", &x[i], &y[i]);
     for (int i = 0; i < n; i++)
     {
          for (int j = i + 1; j < n; j++)
          {
               if (y[i] == y[j])
                    d[z++] = abs(x[i] - x[j]);
          }
     }
     for (int i = 0; i < z; i++)
     {
          for (int j = 0; j < z - i - 1; j++)
          {
               if (d[j] > d[j + 1])
               {
                    int t = d[j];
                    d[j] = d[j + 1];
                    d[j + 1] = t;
               }
          }
     }
     if (z == 0)
          return printf("-1");
     for (int i = 0; i < z; i++)
          printf("%d ", d[i]);
}