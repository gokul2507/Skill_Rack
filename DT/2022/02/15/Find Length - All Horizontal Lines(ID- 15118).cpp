#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n;
     map<int, vector<int>> m;
     cin >> n;
     for (int i = 0; i < n; i++)
     {
          int x, y;
          cin >> x >> y;
          vector<int> t;
          if (m.find(y) != m.end())
               t = m[y];
          t.push_back(x);
          m[y] = t;
     }
     vector<int> ans;
     for (auto i : m)
     {
          vector<int> t = i.second;
          for (int i = 0; i < t.size(); i++)
               for (int j = i + 1; j < t.size(); j++)
                    ans.push_back(abs(t[i] - t[j]));
     }
     sort(ans.begin(), ans.end());
     if (ans.size() == 0)
          ans.push_back(-1);
     for (int i : ans)
          cout << i << " ";
}