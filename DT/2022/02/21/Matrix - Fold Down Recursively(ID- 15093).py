x, y = map(int, input().split())
z = list(list(map(int, input().split())) for _ in range(x))
while x % 2 == 0:
    x = x//2
    q, w = z[:x][::-1], z[x:]
    z = []
    for i, j in list(zip(q, w)):
        z.append([e+r for e, r in list(zip(i, j))])
    for i in z:
        print(*i)
