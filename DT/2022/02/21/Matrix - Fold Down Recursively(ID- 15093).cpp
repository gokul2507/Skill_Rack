#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int R, C;
     cin >> R >> C;
     vector<vector<int>> mat(R, vector<int>(C));
     for (auto &i : mat)
     {
          for (auto &j : i)
               cin >> j;
     }
     int start = 0, end = R - 1;
     while ((start - end) % 2 != 0)
     {
          for (int ctr = 0; ctr <= (end - start) / 2; ctr++)
          {
               for (int j = 0; j < C; j++)
                    mat[end - ctr][j] += mat[start + ctr][j];
          }
          start += (end - start) / 2 + 1;
          for (int i = start; i <= end; i++)
          {
               for (int j = 0; j < C; j++)
               {
                    cout << mat[i][j] << " ";
               }
               cout << endl;
          }
     }
}