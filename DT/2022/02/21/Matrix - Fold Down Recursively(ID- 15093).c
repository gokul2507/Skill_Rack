#include <stdio.h>
#include <stdlib.h>

int main()
{
     int r, c, t = 1;
     scanf("%d %d", &r, &c);
     int mat[r][c];
     for (int i = 0; i < r; i++)
     {
          for (int j = 0; j < c; j++)
          {
               scanf("%d", &mat[i][j]);
          }
     }
     while (r % 2 == 0)
     {
          for (int i = 0; i < r / 2; i++)
          {
               for (int j = 0; j < c; j++)
               {
                    mat[i][j] += mat[r - i - 1][j];
               }
          }
          for (int i = r / 2 - 1; i > -1; i--)
          {
               for (int j = 0; j < c; j++)
               {
                    printf("%d ", mat[i][j]);
               }
               printf("\n");
          }
          r /= 2;
     }
}