import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner s = new Scanner(System.in);
          int r = s.nextInt();
          int c = s.nextInt();
          int[][] mat = new int[r][c];
          for (int i = 0; i < r; i++) {
               for (int j = 0; j < c; j++)
                    mat[i][j] = s.nextInt();
          }
          while (mat.length % 2 == 0) {
               int[][] m1 = new int[mat.length / 2][c];
               for (int i = mat.length / 2, j = (mat.length / 2) - 1, a = 0; i < mat.length; i++, j--, a++) {
                    for (int k = 0; k < c; k++) {
                         m1[a][k] = mat[i][k] + mat[j][k];
                    }
               }
               mat = m1;
               for (int i = 0; i < mat.length; i++) {
                    for (int j = 0; j < c; j++)
                         System.out.print(mat[i][j] + " ");
                    System.out.println();
               }
          }
     }
}