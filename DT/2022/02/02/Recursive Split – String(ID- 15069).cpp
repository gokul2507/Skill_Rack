#include <bits/stdc++.h>

using namespace std;
int main(int argc, char **argv)
{
     string s;
     cin >> s;
     vector<string> ans;
     ans.push_back(s);
     while (1)
     {
          vector<string> tmp;
          for (string i : ans)
          {
               int N = i.size();
               tmp.push_back(i.substr(0, N / 2));
               tmp.push_back(i.substr(N / 2));
          }
          int f = 1;
          for (string i : tmp)
          {
               if (i.size())
                    cout << i << " ";
               if (i.size() > 1)
                    f = 0;
          }
          cout << endl;
          if (f)
               return 0;
          ans = tmp;
     }
}