import java.util.*;

public class Hello {

    public static void main(String[] args) {
        // Your Code Here
        Scanner sc = new Scanner(System.in);
        boolean found = true;
        String s = sc.next();
        Queue<String> q = new LinkedList<>();
        q.add(s);
        int size = 1;
        while (!q.isEmpty() && found) {
            int number = 0, subfound = 0;
            while (size-- > 0) {
                String curr = q.poll();
                int len = curr.length(), mid = len / 2;
                if (len != 1) {
                    String val1 = curr.substring(0, mid);
                    String val2 = curr.substring(mid);
                    q.add(val1);
                    q.add(val2);
                    number += 2;
                    if (val1.length() != 1 || val2.length() != 1)
                        subfound++;
                    System.out.print(val1 + " " + val2 + " ");
                } else {
                    number++;
                    q.add(curr);
                    System.out.print(curr + " ");
                }
            }
            System.out.println();
            size = number;
            if (subfound != 0)
                found = true;
            else
                found = false;
        }
    }

}