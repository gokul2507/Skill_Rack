#include <stdio.h>
#include <stdlib.h>

int main()
{
     int N;
     scanf("%d", &N);
     int mat[N][N], i = 0, j;
     while (~scanf("%d", *mat + i++))
          ;
     while (N)
     {
          for (i = 0; i < N; i += 2)
               for (j = 0; j < N; j += 2)
                    mat[i / 2][j / 2] = mat[i][j] + mat[i][j + 1] + mat[i + 1][j] + mat[i + 1][j + 1];
          for (N /= 2, i = 0; i < N; i += printf("\n"))
               for (j = 0; j < N; printf("%d ", mat[i][j++]))
                    ;
     }
}