n = int(input())
z = [list(map(int, input().split())) for _ in range(n)]
while n > 1:
    n = n//2
    l = []
    for i in range(n):
        tp = []
        for j in range(n):
            r, c = i*2, j*2
            t = z[r][c]+z[r][c+1]+z[r+1][c]+z[r+1][c+1]
            print(t, end=' ')
            tp.append(t)
        print()
        l.append(tp)
    z = l[:]