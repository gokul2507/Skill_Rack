#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int N;
     cin >> N;
     vector<vector<int>> mat(N, vector<int>(N));
     for (auto &i : mat)
     {
          for (auto &j : i)
          {
               cin >> j;
          }
     }
     for (int i = 2; i <= N; i <<= 1)
     {
          for (int j = 0; j < N; j += i)
          {
               for (int k = 0; k < N; k += i)
               {
                    int sum = 0;
                    for (int l = j; l < j + i; l++)
                    {
                         for (int m = k; m < k + i; m++)
                         {
                              sum += mat[l][m];
                         }
                    }
                    cout << sum << " ";
               }
               cout << endl;
          }
     }
}