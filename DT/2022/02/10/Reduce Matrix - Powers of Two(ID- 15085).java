import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner s = new Scanner(System.in);
          int n = s.nextInt();
          int[][] mat = new int[n][n];
          for (int i = 0; i < n; i++) {
               for (int j = 0; j < n; j++)
                    mat[i][j] = s.nextInt();
          }
          while (n != 1) {
               int num = n / 2;
               int[][] m1 = new int[num][num];
               for (int a = 0; a < num; a++) {
                    for (int b = 0; b < num; b++) {
                         int sum = 0;
                         for (int i = a * 2; i < (a * 2) + 2; i++) {
                              for (int j = b * 2; j < (b * 2) + 2; j++)
                                   sum += mat[i][j];
                         }
                         m1[a][b] = sum;
                         System.out.print(sum + " ");
                    }
                    System.out.println();
               }
               // System.out.println();
               n /= 2;
               mat = m1;
          }
     }
}