#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n;
     cin >> n;
     while (n--)
     {
          int t;
          cin >> t;
          int s = 0, m = 1;
          while (t)
          {
               s += t % 10;
               m *= t % 10;
               t /= 10;
          }
          cout << max(s, m) << " ";
     }
}