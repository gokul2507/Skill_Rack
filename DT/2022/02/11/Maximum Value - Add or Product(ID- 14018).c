#include <stdio.h>
#include <stdlib.h>
void printResult(int num)
{
     int product = 1, sum = 0;
     while (num > 0)
     {
          product *= num % 10;
          sum += num % 10;
          num /= 10;
     }
     if (product > sum)
     {
          printf("%d ", product);
     }
     else
     {
          printf("%d ", sum);
     }
}
int main()
{
     int N;
     scanf("%d", &N);
     int arr[N];
     for (int index = 0; index < N; index++)
     {
          scanf("%d", arr + index);
          printResult(arr[index]);
     }
}