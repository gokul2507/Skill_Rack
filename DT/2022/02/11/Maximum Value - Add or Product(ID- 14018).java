import java.util.*;
public class Hello {

    public static void main(String[] args) {
		//Your Code Here
Scanner in=new Scanner(System.in);
int n=in.nextInt();
while(n-->0)
{
    int no=in.nextInt();
    int sum=0;
    int pro=1;
    while(no!=0)
    {
        sum+=no%10;
        pro*=no%10;
        no/=10;
    }
    System.out.print(Math.max(pro,sum)+" ");
}
	}
}