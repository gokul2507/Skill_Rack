#include <bits/stdc++.h>

using namespace std;
string s;
long long int fun(string t, int p)
{
     long long int l = s.size(), ans = 0, o = t.size();
     for (int i = 0; i < l - o + 1; i++)
     {
          int flag = 1;
          for (int j = 0; j < o; j++)
          {
               if (s[i + j] != t[j])
               {
                    flag = 0;
                    break;
               }
          }
          if (flag)
          {
               for (int j = i; j < i + o; j++)
                    s[j] = '-';
               string u = "";
               for (int j = i + o; j < l && isdigit(s[j]); j++)
                    u += s[j];
               if (u.size() == 0)
                    u = "1";
               // cout<<t/<<" "<<u<<endl;
               stringstream q(u);
               long long int w;
               q >> w;
               ans += p * w;
          }
     }
     return ans;
}
int main(int argc, char **argv)
{
     cin >> s;
     cout << fun("Cu", 63) + fun("Ca", 40) + fun("S", 32) + fun("Mg", 24) + fun("He", 4) + fun("O", 16) + fun("N", 14) + fun("C", 12) + fun("H", 1);
}