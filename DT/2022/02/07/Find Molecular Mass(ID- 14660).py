m = {'H': 1, 'He': 4, 'C': 12, 'N': 14, 'O': 16,
     'Mg': 24, 'S': 32, 'Ca': 40, 'Cu': 63}
s = input().strip()
x, y = [s[0]], 0
for i in s[1:]:
    if i.isupper():
        x.append(i)
    else:
        x[-1] += i
for i in x:
    c = ''
    for j in i:
        if j.isalpha():
            c += j
            i = i.replace(j, '')
    y = y+m[c]*int(i) if i != '' else y+m[c]
print(y)
