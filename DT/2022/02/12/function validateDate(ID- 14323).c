#include <stdio.h>
#include <stdlib.h>

int fun(int x, int e)
{
     return 1 <= x && x <= e;
}
int validateDate(char *dateStr)
{
     int d = (dateStr[0] - '0') * 10 + dateStr[1] - '0', m = (dateStr[3] - '0') * 10 + dateStr[4] - '0', y = (dateStr[6] - '0') * 1000 + (dateStr[7] - '0') * 100 + (dateStr[8] - '0') * 10 + dateStr[9] - '0';
     if (m == 2)
     {
          if (((y % 4 == 0 && y % 100) || y % 400 == 0))
          {
               return fun(d, 29);
          }
          return fun(d, 28);
     }
     if (m > 12 || m < 0)
          return 0;
     if (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12)
          return fun(d, 31);
     return fun(d, 30);
}
int main()
{
     char dateStr[11];
     scanf("%s", dateStr);
     printf("%d", validateDate(dateStr));
     return 0;
}