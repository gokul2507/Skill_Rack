import java.util.*;

public class Hello {
     public static void main(String[] args) {
          Scanner q = new Scanner(System.in);
          int n = Integer.parseInt(q.nextLine());
          HashMap<Character, Integer> m = new HashMap<Character, Integer>();
          List<String> z = new ArrayList<String>();
          char ans = '-';
          for (int i = 0; i < n; i++) {
               String t = q.nextLine();
               z.add(t);
               for (int j = 0; j < t.length(); j++)
                    if (i == 0)
                         m.put(t.charAt(j), 1);
                    else if (m.containsKey(t.charAt(j)) && m.get(t.charAt(j)) == i) {
                         m.put(t.charAt(j), m.get(t.charAt(j)) + 1);
                         ans = t.charAt(j);
                    }
          }
          for (int i = 0; i < n; i++) {
               z.set(i, z.get(i).substring(z.get(i).indexOf(ans)));
          }
          Collections.sort(z);
          for (String i : z)
               System.out.println(i);
     }
}