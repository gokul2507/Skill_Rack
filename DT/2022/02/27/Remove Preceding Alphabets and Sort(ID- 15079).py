N = int(input())
string, S = [input().strip() for row in range(N)], []
for val in string[0]:
    for ch in string[1:]:
        if val not in ch:
            break
    else:
        for ind in range(N):
            S.append(string[ind][string[ind].index(val):])
print(*sorted(S), sep='\n')
