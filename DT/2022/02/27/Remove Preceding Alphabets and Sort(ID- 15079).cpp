#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int N;
     cin >> N;
     vector<string> arr(N), ans;
     for (string &i : arr)
          cin >> i;
     for (char i : arr[0])
     {
          for (string j : arr)
          {
               int ok = 0;
               for (char c : j)
               {
                    if (c == i)
                    {
                         ok = 1;
                    }
               }
               if (!ok)
                    goto end;
          }
          for (string j : arr)
          {
               int ok = 0;
               string t = "";
               for (char c : j)
               {
                    if (c == i)
                         ok = 1;
                    if (!ok)
                         continue;
                    t += c;
               }
               ans.push_back(t);
          }
          sort(ans.begin(), ans.end());
          for (string j : ans)
               cout << j << endl;
     end:;
     }
}