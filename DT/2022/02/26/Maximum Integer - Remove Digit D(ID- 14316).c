#include <stdio.h>
#include <stdlib.h>

int main()
{
     char str[10001], c;
     scanf("%s\n%c", str, &c);
     int max = -100000;
     for (int i = 0; i < strlen(str); i++)
     {
          if (str[i] == c)
          {
               char temp[100];
               int ind = 0;
               for (int j = 0; j < strlen(str); j++)
               {
                    if (i != j)
                    {
                         temp[ind++] = str[j];
                    }
               }
               temp[ind] = '\0';
               if (max < atoi(temp))
               {
                    max = atoi(temp);
               }
          }
     }
     printf("%d", max);
}