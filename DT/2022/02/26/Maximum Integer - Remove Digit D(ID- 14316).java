import java.util.*;

public class Hello {

     public static void main(String[] args) {

          Scanner sc = new Scanner(System.in);
          String n = sc.next();
          char ch = sc.next().charAt(0);
          sc.close();

          int max = Integer.MIN_VALUE;

          for (int i = 0; i < n.length(); i++) {
               if (n.charAt(i) == ch) {
                    int num = Integer.parseInt(n.substring(0, i) + n.substring(i + 1));
                    if (num > max) {
                         max = num;
                    }
               }
          }

          System.out.println(max);

     }
}