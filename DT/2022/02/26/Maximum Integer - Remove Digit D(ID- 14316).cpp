#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     string str;
     cin >> str;
     char ch;
     cin >> ch;
     int ans = INT_MIN;
     for (int i = 0; str[i]; i++)
     {
          if (str[i] == ch)
          {
               ans = max(ans, stoi(str.substr(0, i) + str.substr(i + 1)));
          }
     }
     cout << ans;
}