#include <bits/stdc++.h>
using namespace std;
vector<vector<int>> z;
int a, b, c, d, n;
void display()
{
     for (int i = 0; i < n; i++)
     {
          for (int j : z[i])
               cout << j << " ";
          cout << endl;
     }
}
void fit(int x)
{
     int y = a;
     for (int i = x + 1; i < n - x - 1; i++)
          z[x][i] = ++y;
     y = b;
     for (int i = x + 1; i < n - x - 1; i++)
          z[i][n - 1 - x] = ++y;
     y = c;
     for (int i = n - 2 - x; i > x; i--)
          z[n - 1 - x][i] = ++y;
     y = d;
     for (int i = n - 2 - x; i > x; i--)
          z[i][x] = ++y;
}
int main(int argc, char **argv)
{
     cin >> a >> b >> c >> d >> n;
     z.resize(n, vector<int>(n));
     for (int i = 0; i < n / 2; i++)
     {
          z[i][i] = a;
          z[i][n - 1 - i] = b;
          z[n - 1 - i][i] = d;
          z[n - 1 - i][n - i - 1] = c;
          fit(i);
     }
     display();
}