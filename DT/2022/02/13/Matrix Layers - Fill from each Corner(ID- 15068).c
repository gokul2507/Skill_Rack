int main()
{
     int N, A, B, C, D;
     scanf("%d %d %d %d\n%d", &A, &B, &C, &D, &N);
     int matrix[N][N];
     for (int layer = 1; layer <= N / 2; layer++)
     {
          int num = A;
          for (int col = layer - 1; col < N - layer + 1; col++)
          {
               matrix[layer - 1][col] = num++;
          }
          num = B;
          for (int row = layer - 1; row < N - layer + 1; row++)
          {
               matrix[row][N - layer] = num++;
          }
          num = C;
          for (int col = N - layer; col >= layer - 1; col--)
          {
               matrix[N - layer][col] = num++;
          }
          num = D;
          for (int row = N - layer; row >= layer; row--)
          {
               matrix[row][layer - 1] = num++;
          }
     }
     for (int row = 0; row < N; row++)
     {
          for (int col = 0; col < N; col++)
          {
               printf("%d ", matrix[row][col]);
          }
          printf("\n");
     }
}