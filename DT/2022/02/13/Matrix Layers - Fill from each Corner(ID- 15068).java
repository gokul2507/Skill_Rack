import java.util.*;

public class Hello {

     public static void main(String[] args) {
          // Your Code Here
          Scanner sc = new Scanner(System.in);
          int[] arr = new int[4];
          for (int i = 0; i < 4; i++) {
               arr[i] = sc.nextInt();
          }
          int n = sc.nextInt();
          int[][] mat = new int[n][n];
          int x1 = 0, y1 = 0, x2 = 0, y2 = n - 1, x3 = n - 1, y3 = n - 1, x4 = n - 1, y4 = 0;
          while (y1 < y2) {
               for (int ind = y1, val = arr[0]; ind < y2; ind++) {
                    mat[x1][ind] = val++;
               }
               for (int ind = x2, val = arr[1]; ind < x3; ind++) {
                    mat[ind][y2] = val++;
               }
               for (int ind = y3, val = arr[2]; ind > y4; ind--) {
                    mat[x3][ind] = val++;
               }
               for (int ind = x4, val = arr[3]; ind > x1; ind--) {
                    mat[ind][y1] = val++;
               }
               x1++;
               y1++;
               x2++;
               y2--;
               x3--;
               y3--;
               x4--;
               y4++;
          }
          for (int i = 0; i < n; i++) {
               for (int j = 0; j < n; j++) {
                    System.out.print(mat[i][j] + " ");
               }
               System.out.println();
          }
     }
}