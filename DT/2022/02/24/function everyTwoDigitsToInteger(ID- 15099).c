#include <stdio.h>
#include <stdlib.h>

long long int fun(int x, int y)
{
    int inc = 1;
    long long int r = 0;
    if (x > y)
        inc = -1;
    while (x != y)
    {
        r *= 10;
        r += x;
        x += inc;
    }
    r *= 10;
    r += y;
    // printf("%lld\n",r);
    return r;
}
long long int everyTwoDigitsToInteger(int N)
{
    long long int ans = 0;
    int t = N % 10;
    N /= 10;
    while (N)
    {
        ans += fun(N % 10, t);
        t = N % 10;
        N /= 10;
    }
    return ans;
}
int main()
{
    int N;
    scanf("%d", &N);
    printf("%lld", everyTwoDigitsToInteger(N));
    return 0;
}
