import java.util.*;

public class Hello {

     public static void main(String[] args) {
          // Your Code Here
          Scanner sc = new Scanner(System.in);
          Map<String, String> map = new LinkedHashMap<>();
          int n = sc.nextInt();
          sc.nextLine();
          for (int i = 0; i < n; i++) {
               String[] inp = sc.nextLine().split(":");
               if (map.containsKey(inp[0])) {
                    map.remove(inp[0]);
               }
               map.put(inp[0], inp[1]);
          }
          String ans = "";
          for (String name : map.keySet()) {
               String message = name + ":" + map.get(name);
               ans = message + (ans.length() != 0 ? "\n" : "") + ans;
          }
          System.out.println(ans);
     }
}