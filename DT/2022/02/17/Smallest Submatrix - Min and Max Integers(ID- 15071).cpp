#include <stdio.h>
#include <stdlib.h>

int main()
{
     int x, i, j, f;
     char n[105][105], m[105][105];
     scanf("%d\n", &x);
     for (i = 0; i < x; i++)
     {
          scanf("%[^:]:%[^\n]\n", n[i], m[i]);
     }
     for (i = x - 1; i >= 0; i--)
     {
          f = 0;
          for (j = i + 1; j < x; j++)
               if (strcmp(n[i], n[j]) == 0)
               {
                    f = 1;
                    break;
               }
          if (f == 0)
               printf("%s:%s\n", n[i], m[i]);
     }
}