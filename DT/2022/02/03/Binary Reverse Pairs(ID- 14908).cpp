#include <bits/stdc++.h>

using namespace std;
int rev(int val)
{
     int ans = 0;
     while (val)
     {
          ans |= val & 1;
          ans <<= 1;
          val >>= 1;
     }

     return ans >> 1;
}
int main(int argc, char **argv)
{
     int N;
     cin >> N;
     vector<int> arr(N);
     for (int &i : arr)
          cin >> i;
     int f = 1;
     for (int i = 0; i < N; i++)
     {
          for (int j = i + 1; j < N; j++)
          {
               if (arr[i] == rev(arr[j]))
               {
                    cout << arr[i] << " " << arr[j] << endl;
                    f = 0;
               }
          }
     }
     if (f)
          cout << -1;
}
#include <bits/stdc++.h>
 
using namespace std;
int rev(int val)
{
     int ans = 0;
     while (val)
     {
          ans |= val & 1;
          ans <<= 1;
          val >>= 1;
     }

     return ans >> 1;
}
int main(int argc, char **argv)
{
     int N;
     cin >> N;
     vector<int> arr(N);
     for (int &i : arr)
          cin >> i;
     int f = 1;
     for (int i = 0; i < N; i++)
     {
          for (int j = i + 1; j < N; j++)
          {
               if (arr[i] == rev(arr[j]))
               {
                    cout << arr[i] << " " << arr[j] << endl;
                    f = 0;
               }
          }
     }
     if (f)
          cout << -1;
