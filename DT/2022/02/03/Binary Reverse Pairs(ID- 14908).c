#include <stdio.h>
#include <stdlib.h>
int isValid(int num1, int num2)
{
     int reversedNum = 0;
     while (num2 > 0)
     {
          reversedNum = reversedNum * 2 + (num2 % 2);
          num2 /= 2;
     }
     if (num1 == reversedNum)
     {
          return 1;
     }
     return 0;
}
int main()
{
     int N;
     scanf("%d", &N);
     int arr[N];
     for (int index = 0; index < N; index++)
     {
          scanf("%d", arr + index);
     }
     int hasPairs = 0;
     for (int ind1 = 0; ind1 < N; ind1++)
     {
          for (int ind2 = ind1 + 1; ind2 < N; ind2++)
          {
               if (isValid(arr[ind1], arr[ind2]))
               {
                    printf("%d %d\n", arr[ind1], arr[ind2]);
                    hasPairs = 1;
               }
          }
     }
     if (!hasPairs)
     {
          printf("-1");
     }
}