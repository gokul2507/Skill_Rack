import java.util.*;

public class Hello {
     static String rev(String str) {
          String ans = "";
          for (int i = str.length() - 1; i >= 0; i--) {
               ans += str.charAt(i);
          }
          return ans;
     }

     static String conv(int n) {
          return Integer.toBinaryString(n);
     }

     public static void main(String[] args) {
          // Your Code Here
          Scanner sc = new Scanner(System.in);
          int n = sc.nextInt();
          int arr[] = new int[n];
          for (int i = 0; i < n; i++) {
               arr[i] = sc.nextInt();
          }
          int c = 0;
          for (int i = 0; i < n; i++) {
               for (int j = i + 1; j < n; j++) {
                    String s1 = conv(arr[i]);
                    String s2 = conv(arr[j]);
                    s2 = rev(s2);
                    if (s1.equals(s2)) {
                         System.out.println(arr[i] + " " + arr[j]);
                         c = 1;
                    }
               }
          }
          if (c == 0)
               System.out.print(-1);

     }
}