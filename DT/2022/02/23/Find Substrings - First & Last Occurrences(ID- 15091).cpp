#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
    int n;
    cin >> n;
    string arr[n];
    for (int i = 0; i < n; i++)
        cin >> arr[i];
    int ans[n][2];
    for (int i = 0; i < n; i++)
    {
        int pre = i - 1, nxt = i + 1;
        if (pre == -1)
            pre = n - 1;
        if (nxt == n)
            nxt = 0;
        ans[i][0] = max(0, (int)arr[i].find(arr[pre].back()));
        ans[i][1] = arr[i].find_last_of(arr[nxt].front());
        if (ans[i][1] == -1)
            ans[i][1] = arr[i].size() - 1;
        ;
        if (ans[i][0] > ans[i][1])
            swap(ans[i][0], ans[i][1]);
        string temp = arr[i].substr(ans[i][0], ans[i][1] - ans[i][0] + 1);
        if (temp.empty())
            ; // cout<<-1;
        else
            cout << temp << endl;
        // cout<<endl;
    }
}