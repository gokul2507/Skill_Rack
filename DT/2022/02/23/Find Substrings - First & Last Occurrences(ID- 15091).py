n = int(input())
l = [input().strip() for _ in range(n)]
for i in range(n):
    a = l[i].find(l[i-1][-1])
    b = l[i].rfind(l[(i+1) % n][0])
    if a == -1:
        a = 0
    if b == -1:
        b = len(l[i])-1
    s, e = min(a, b), max(a, b)
    print(l[i][s:e+1])
