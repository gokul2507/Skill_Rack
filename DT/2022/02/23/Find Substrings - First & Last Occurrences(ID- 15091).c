#include <stdio.h>
#include <stdlib.h>

int main()
{
     int n;
     scanf("%d\n", &n);
     char s[n][101];
     for (int i = 0; i < n; i++)
     {
          scanf("%s\n", s[i]);
     }
     int b[n], e[n];
     for (int i = 0; i < n; i++)
     {
          b[i] = -1;
          e[i] = -1;
          for (int j = 0; j < strlen(s[i]); j++)
          {
               if (i == 0 && s[i][j] == s[n - 1][strlen(s[n - 1]) - 1])
                    b[i] = j;
               else if (s[i][j] == s[i - 1][strlen(s[i - 1]) - 1])
                    b[i] = j;
               if (b[i] != -1)
                    break;
          }
          if (b[i] == -1)
               b[i] = 0;
          for (int j = 0; j < strlen(s[i]); j++)
          {
               if (i == n - 1 && s[i][j] == s[0][0])
                    e[i] = j;
               else if (s[i][j] == s[i + 1][0])
                    e[i] = j;
          }
          if (e[i] == -1)
               e[i] = strlen(s[i]) - 1;
          if (e[i] > b[i])
          {
               for (int j = b[i]; j <= e[i]; j++)
                    printf("%c", s[i][j]);
          }
          else
          {
               for (int j = e[i]; j <= b[i]; j++)
                    printf("%c", s[i][j]);
          }
          printf("\n");
     }
}