#include <stdio.h>
#include <stdlib.h>

int size(int n)
{
     return (int)log2(n) + 1;
}

int main()
{
     int N;
     scanf("%d", &N);
     int arr[N * 2], i = 0, n, l;
     while (~scanf("%d", &n))
          l = size(n),
          arr[i++] = n >> (l / 2),
          arr[i++] = n & ((1 << (++l / 2)) - 1);
     int cmp(int *a, int *b) { return *a - *b; }
     qsort(arr, N * 2, 4, (int (*)(void *, void *))cmp);
     for (i = 0; i < N * 2; printf("%d ", arr[i++]))
          ;
}