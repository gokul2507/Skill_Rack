#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n;
     cin >> n;
     vector<int> arr;
     for (int i = 0; i < n; i++)
     {
          int val;
          cin >> val;
          int leno = (32 - __builtin_clz(val));
          int len = (leno + 1) / 2;
          arr.push_back(val & ((1 << len) - 1));
          len = leno / 2;
          arr.push_back(val >> len);
     }
     sort(arr.begin(), arr.end());
     for (int i : arr)
          cout << i << " ";
}