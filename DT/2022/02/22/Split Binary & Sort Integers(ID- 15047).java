import java.util.*;

public class Hello {
     public static String fun(int x) {
          if (x == 0)
               return "";
          return fun(x / 2) + x % 2;
     }

     public static void main(String[] args) {
          Scanner q = new Scanner(System.in);
          int n = q.nextInt();
          List<Integer> z = new ArrayList<Integer>();
          for (int i = 0; i < n; i++) {
               String x = fun(q.nextInt());
               int t = x.length();
               if (t % 2 == 0) {
                    z.add(Integer.parseInt(x.substring(0, t / 2), 2));
               } else {
                    z.add(Integer.parseInt(x.substring(0, t / 2 + 1), 2));
               }
               z.add(Integer.parseInt(x.substring(t / 2), 2));
          }
          Collections.sort(z);
          for (int i : z)
               System.out.print(i + " ");
     }
}