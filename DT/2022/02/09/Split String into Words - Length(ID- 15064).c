#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n;
     cin >> n;
     string str;
     cin >> str;
     vector<string> arr;
     for (int i = 0; i < n; i++)
     {
          int val;
          cin >> val;
          arr.push_back(str.substr(0, val));
          str = str.substr(val);
     }
     stable_sort(arr.begin(), arr.end(), [](auto a, auto b)
                 { return a.size() < b.size(); });
     for (auto &i : arr)
          cout << i;
}