import java.util.*;

public class Hello {

     public static void main(String[] args) {
          // Your Code Here
          Scanner sc = new Scanner(System.in);
          int n = Integer.parseInt(sc.next());
          String s = sc.next();
          String[] arr = new String[n];
          for (int i = 0, ind = 0; i < n; i++) {
               int length = sc.nextInt();
               arr[i] = s.substring(ind, ind + length);
               ind += length;
          }
          Arrays.sort(arr, (a, b) -> a.length() - b.length());
          for (String curr : arr)
               System.out.print(curr);
     }
}