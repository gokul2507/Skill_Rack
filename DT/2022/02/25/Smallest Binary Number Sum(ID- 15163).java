import java.util.*;

public class Hello {

     public static void main(String[] args) {
          // Your Code Here
          Scanner sc = new Scanner(System.in);
          int n = sc.nextInt(), s = 0;
          for (int i = 0; i < n; i++) {
               int k = sc.nextInt();
               long j = 1;
               while (true) {
                    String t = Long.toBinaryString(j);
                    long v = Long.parseLong(t);
                    if (v % k == 0) {
                         // System.out.print(j);
                         s += j;
                         break;
                    }
                    j++;
               }

          }
          System.out.print(s);
     }
}