#include <stdio.h>
#include <stdlib.h>

long int bine(long int a)
{
     if (a == 0)
          return 0;
     return (a % 2 + 10 * bine(a / 2));
}

int main()
{
     int n;
     scanf("%d ", &n);
     int ar[n];
     for (int i = 0; i < n; i++)
          scanf("%d ", &ar[i]);
     int s = 0;
     // printf("%d ",bine(2));
     for (int i = 0; i < n; i++)
     {
          int t = 1;
          while (1)
          {
               long int b = bine(t);
               if (b % ar[i] == 0)
               {
                    s += t;
                    break;
               }
               t++;
          }
     }
     printf("%d", s);
}