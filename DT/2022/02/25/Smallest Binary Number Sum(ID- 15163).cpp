#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
#define int long long
     int N;
     cin >> N;
     int sum = 0;
     while (N--)
     {
          int val;
          cin >> val;
          for (int i = 1;; i++)
          {
               string bin = bitset<32>(i).to_string();
               int x = stoll(bin);
               if (x % val == 0)
               {
                    sum += i;
                    break;
               }
          }
     }
     cout << sum;
}