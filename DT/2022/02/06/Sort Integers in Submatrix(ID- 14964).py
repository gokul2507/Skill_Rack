R, C = list(map(int, input().split()))
Arr = []
for i in range(R):
    Arr.append(list(map(int, input().split())))
X, Y = list(map(int, input().split()))

To_Sort = []

for i in range(X):
    To_Sort += Arr[i][Y - 1 :]
To_Sort = sorted(To_Sort)

index = 0

for i in range(X):
    for j in range(Y - 1, C):
        Arr[i][j] = To_Sort[index]
        index += 1

for row in Arr:
    print(" ".join(list(map(str, row))))
