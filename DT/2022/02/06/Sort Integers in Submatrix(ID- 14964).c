#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int R, C;
     cin >> R >> C;
     vector<vector<int>> mat(R, vector<int>(C));
     for (auto &i : mat)
     {
          for (auto &j : i)
               cin >> j;
     }
     int X, Y, ind = 0;
     cin >> X >> Y;
     vector<int> arr;
     for (int i = 0; i < X; i++)
     {
          for (int j = C - (C - Y + 1); j < C; j++)
          {
               arr.push_back(mat[i][j]);
          }
     }
     sort(arr.begin(), arr.end());
     for (int i = 0; i < X; i++)
     {
          for (int j = C - (C - Y + 1); j < C; j++)
          {
               mat[i][j] = arr[ind++];
          }
     }
     for (auto i : mat)
     {
          for (auto j : i)
          {
               cout << j << " ";
          }
          cout << endl;
     }
}