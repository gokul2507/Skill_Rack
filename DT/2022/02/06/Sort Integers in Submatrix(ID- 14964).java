import java.util.*;
public class Hello {

    public static void main(String[] args) {
	
	Scanner sk = new Scanner(System.in);
	int row = sk.nextInt(),col = Integer.parseInt(sk.nextLine().trim());
	int[][] matrix = new int[row][];
	for(int i = 0;i < row; i ++)
	{
	    matrix[i] = Arrays.stream(sk.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
	}
	int xrow = sk.nextInt(),ycol = sk.nextInt();
	ArrayList<Integer> ele = new ArrayList<Integer>();
	for(int i = 0;i < xrow; i ++)
	{
	    for(int j = ycol-1;j<col;j++)
	    {
	        ele.add(matrix[i][j]);
	    }
	}
	Collections.sort(ele);
	int index = 0;
	for(int i = 0;i < row;i ++)
	{
	    for(int j = 0;j < col ;j ++)
	    {
	        if(i < xrow && j>=ycol-1)
	        {
	            System.out.print(ele.get(index)+" ");
	            index ++;
	        }else
	        {
	            System.out.print(matrix[i][j] +" ");
	        }
	        
	    }System.out.println();
	}

	}
}