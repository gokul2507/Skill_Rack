#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int r, c;
     cin >> r >> c;
     int m[r][c];
     for (int i = 0; i < r; i++)
          for (int j = 0; j < c; j++)
               cin >> m[i][j];
     int x, y;
     cin >> x >> y;
     vector<int> z;
     for (int i = 0; i < x; i++)
          for (int j = y - 1; j < c; j++)
               z.push_back(m[i][j]);
     sort(z.begin(), z.end());
     for (int i = 0, u = 0; i < x; i++)
          for (int j = y - 1; j < c; j++)
               m[i][j] = z[u++];
     for (int i = 0; i < r; i++)
     {
          for (int j = 0; j < c; j++)
               cout << m[i][j] << " ";
          cout << endl;
     }
}