import java.util.*;
import java.io.*;

public class Hello {
     public static void main(String[] args) {
          Scanner q = new Scanner(System.in);
          int n = q.nextInt(), m = 0;
          q.nextLine();
          List<String> z = Arrays.asList(q.nextLine().split(" ", n));
          for (String i : z)
               m = Math.max(m, i.length());
          int ans = 0;
          for (String i : z)
               ans += Integer.parseInt(i + "0".repeat(m - i.length()));
          System.out.println(ans);
     }
}