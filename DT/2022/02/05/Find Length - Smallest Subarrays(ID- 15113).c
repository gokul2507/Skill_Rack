#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n;
     cin >> n;
     vector<int> st;
     int arr[n];
     for (int i = 0; i < n; i++)
     {
          cin >> arr[i];
     }
     for (int i = 0; i < n; i++)
     {
          int k;
          cin >> k;
          for (int j = i; j < n; j++)
          {
               if (arr[j] >= arr[i])
               {
                    k--;
                    if (k == 0)
                    {
                         cout << j - i + 1 << " ";
                         k = -1;
                    }
               }
          }
          if (k > 0)
          {
               cout << -1 << " ";
          }
     }
}