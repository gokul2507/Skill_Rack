#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int N;
     cin >> N;
     vector<int> a(N), b(N);
     for (int &i : a)
          cin >> i;
     for (int &i : b)
          cin >> i;
     for (int i = 0; i < N; i++)
     {
          int ctr = 0, ans = 0;
          for (int j = i; j < N; j++)
          {
               if (a[j] >= a[i])
               {
                    ctr++;
               }
               ans++;
               if (ctr == b[i])
                    break;
          }
          if (ctr >= b[i])
               cout << ans;
          else
               cout << -1;
          cout << " ";
     }
}