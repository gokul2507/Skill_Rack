import java.util.*;

public class Hello {

     public static void main(String[] args) {
          // Your Code Here
          Scanner sc = new Scanner(System.in);
          int n = sc.nextInt();
          int[] arr = new int[n];
          int[] val = new int[n];
          for (int i = 0; i < n; i++)
               arr[i] = sc.nextInt();
          for (int i = 0; i < n; i++)
               val[i] = sc.nextInt();
          for (int i = 0; i < n; i++) {
               int need = val[i], got = 1, ind = i + 1, length = 1;
               while (ind < n && got != need) {
                    if (arr[ind] >= arr[i])
                         got++;
                    ind++;
                    length++;
               }
               System.out.print((need == got ? length : -1) + " ");
          }
     }
}