#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int N;
     cin >> N;
     vector<vector<string>> a(N, vector<string>(2));
     for (auto &i : a)
          for (auto &j : i)
               cin >> j;
     string C;
     cin >> C;
     int f = 1;
     cout << C << " -> ";
     set<string> st1, st2;
     for (auto i : a)
     {
          if (i[0] == C && st1.find(i[1]) == st1.end())
          {
               cout << i[1] << " ";
               f = 0;
               st1.insert(i[1]);
          }
     }
     if (f)
          cout << -1;
     cout << endl;
     f = 1;
     cout << C << " <- ";
     for (auto i : a)
     {
          if (i[1] == C && st2.find(i[0]) == st2.end())
          {
               cout << i[0] << " ";
               f = 0;
               st2.insert(i[0]);
          }
     }
     if (f)
          cout << -1;
}s