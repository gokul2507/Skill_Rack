import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner q = new Scanner(System.in);
          int n = Integer.parseInt(q.nextLine());
          HashMap<String, ArrayList<String>> ft = new HashMap<String, ArrayList<String>>(),
                    tf = new HashMap<String, ArrayList<String>>();
          for (int i = 0; i < n; i++) {
               String x = q.next().trim(), y = q.nextLine().trim();
               ArrayList<String> t = new ArrayList<String>();
               if (ft.containsKey(x))
                    t = ft.get(x);
               t.add(y);
               ft.put(x, t);
               t = new ArrayList<String>();
               if (tf.containsKey(y))
                    t = tf.get(y);
               t.add(x);
               tf.put(y, t);
          }
          String s = q.nextLine().trim();
          System.out.print(s + " -> ");
          if (ft.containsKey(s)) {
               ArrayList<String> tl = new ArrayList<String>();
               for (String i : ft.get(s))
                    if (!tl.contains(i)) {
                         tl.add(i);
                         System.out.print(i + " ");
                    }
          } else
               System.out.print("-1");
          System.out.print("\n" + s + " <- ");
          if (tf.containsKey(s)) {
               ArrayList<String> tl = new ArrayList<String>();
               for (String i : tf.get(s))
                    if (!tl.contains(i)) {
                         tl.add(i);
                         System.out.print(i + " ");
                    }
          } else
               System.out.print("-1");
     }
}