
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main()
{
     int n;
     scanf("%d\n", &n);
     char sc[n][101], dc[n][101], c[101];
     for (int i = 0; i < n; i++)
          scanf("%s %s\n", sc[i], dc[i]);
     scanf("\n%s", c);
     int count = 0;
     for (int i = 0; i < n; i++)
     {
          for (int j = i + 1; j < n; j++)
          {
               if ((strcmp(sc[i], sc[j]) == 0) && (strcmp(dc[j], dc[i]) == 0))
               {
                    strcpy(sc[j], "*");
                    strcpy(dc[j], "*");
               }
          }
     }
     printf("%s -> ", c);
     for (int i = 0; i < n; i++)
     {
          if (strcmp(sc[i], c) == 0)
          {
               printf("%s ", dc[i]);
               count++;
          }
     }
     if (count == 0)
          printf("-1");
     printf("\n%s <- ", c);
     count = 0;
     for (int i = 0; i < n; i++)
     {
          if (strcmp(dc[i], c) == 0)
          {
               printf("%s ", sc[i]);
               count++;
          }
     }
     if (count == 0)
          printf("-1");
}