#include <stdio.h>
#include <stdlib.h>

int main()
{
     char S1[101], S2[101];
     scanf("%s\n%s", S1, S2);
     int subInd = 0, count = 0;
     for (int index = 0; S1[index]; index++)
     {
          if (S1[index] == S2[subInd])
          {
               subInd++;
          }
          else
          {
               count++;
               while (S1[index] != S2[subInd])
               {
                    subInd++;
               }
               subInd++;
          }
     }
     if (subInd < strlen(S2))
     {
          count++;
     }
     printf("%d", count);
}