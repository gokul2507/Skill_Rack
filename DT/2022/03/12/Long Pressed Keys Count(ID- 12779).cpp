#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     string s1, s2;
     cin >> s1 >> s2;
     int len1 = s1.size(), len2 = s2.size(), i = 0, j = 0, ans = 0;
     while (i < len1 && j < len2)
     {
          int key = s1[i];
          int ctr1 = 0, ctr2 = 0;
          while (i < len1 && key == s1[i])
          {
               i++;
               ctr1++;
          }
          while (j < len2 && key == s2[j])
          {
               j++;
               ctr2++;
          }
          if (ctr2 > ctr1)
               ans++;
     }
     cout << ans;
}