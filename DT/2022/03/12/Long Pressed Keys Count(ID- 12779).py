s1 = input().strip()
s2 = input().strip()
o = 0
while s1 and s2:
    ch = s1[0]
    c = 0
    while s1 and s1[0] == ch:
        s1 = s1[1:]
        c += 1
    while c:
        s2 = s2[1:]
        c -= 1
    if(s2 and s2[0] == ch):
        o += 1
        while s2 and s2[0] == ch:
            s2 = s2[1:]
print(o)
