import java.util.*;
public class Hello {

    public static void main(String[] args) {
        Scanner q=new Scanner(System.in);
        int n=q.nextInt(),m=0;
        q.nextLine();
        String[] z=new String[n];
        for(int i=0;i<n;i++){
            String t=q.nextLine();
            m=Math.max(m,t.length());
            z[i]=t;
        }
        char[][] ans=new char[m][n];
        for(int j=0;j<n;j++){
            int i=0;
          //  System.out.println(m-z[j].length());
            for(;i<(m-z[j].length())/2;i++)
            ans[i][j]='*';
            for(int k=0;k<z[j].length();k++)
            ans[i++][j]=z[j].charAt(k);
            for(int k=0;k<(m-z[j].length())/2 && i<m;k++)
            ans[i++][j]='*';
        }
        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++)
            System.out.print(ans[i][j]);
            System.out.println();
        }
    }
}