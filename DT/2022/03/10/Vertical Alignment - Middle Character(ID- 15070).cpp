#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n;
     cin >> n;
     vector<string> vec;
     int maxx = 0;
     while (n--)
     {
          cin >> vec.emplace_back();
          int vall = vec[vec.size() - 1].length();
          maxx = max(maxx, vall);
     }

     for (auto &i : vec)
     {
          int app = (maxx - i.length()) / 2;
          string ap = string(app, '*');
          i = ap + i + ap;
     }

     for (int i = 0; i < maxx; i++)
     {
          for (int j = 0; j < vec.size(); j++)
          {
               cout << vec[j][i];
          }
          cout << endl;
     }
}