N = int(input())
strVals = [input().strip() for _ in range(N)]
maxLength = len(max(strVals, key=len))
result = [list(strVal.center(maxLength, "*")) for strVal in strVals]
for row in zip(*result):
    print(*row, sep="")
