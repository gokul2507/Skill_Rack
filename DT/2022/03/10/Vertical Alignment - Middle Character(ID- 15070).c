#include <stdio.h>
#include <stdlib.h>

int main()
{
     int N;
     scanf("%d\n", &N);
     int matrix[200][N];
     for (int row = 0; row < 200; row++)
     {
          for (int col = 0; col < N; col++)
          {
               matrix[row][col] = '*';
          }
     }
     int minRow = 100, maxRow = 100;
     for (int index = 0; index < N; index++)
     {
          char S[101];
          scanf("%s\n", S);
          int len = strlen(S);
          int row = 100 - (len / 2);
          if (row < minRow)
          {
               minRow = row;
          }
          for (int subInd = 0; S[subInd]; subInd++)
          {
               matrix[row++][index] = S[subInd];
          }
          if (row > maxRow)
          {
               maxRow = row;
          }
     }
     for (int row = minRow; row < maxRow; row++)
     {
          for (int col = 0; col < N; col++)
          {
               printf("%c", matrix[row][col]);
          }
          printf("\n");
     }
}