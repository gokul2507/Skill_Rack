import java.util.*;

public class Hello {

     public static void main(String[] args) {
          // Your Code Here
          Scanner sc = new Scanner(System.in);
          int n = sc.nextInt();
          sc.nextLine();
          List<int[]> list = new ArrayList<>();
          String[] range = new String[n];
          for (int i = 0; i < n; i++) {
               range[i] = sc.next();
          }
          int x = sc.nextInt();
          for (int i = 0; i < n; i++) {
               String[] in = range[i].split("-");
               int start = Integer.parseInt(in[0]),
                         end = Integer.parseInt(in[1]);
               if (start < x && x < end) {
                    list.add(new int[] { start, x });
                    list.add(new int[] { x, end });
               } else
                    list.add(new int[] { start, end });
          }
          Collections.sort(list, (a, b) -> {
               if (a[0] == b[0])
                    return a[1] - b[1];
               return a[0] - b[0];
          });
          for (int[] r : list)
               System.out.println(r[0] + "-" + r[1]);
     }
}