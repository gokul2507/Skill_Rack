#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int N;
     cin >> N;
     vector<vector<int>> arr(N, vector<int>(2)), ans;
     for (int i = 0; i < N; i++)
     {
          char c;
          cin >> arr[i][0] >> c >> arr[i][1];
     }
     int X;
     cin >> X;
     for (auto i : arr)
     {
          if (X > i[0] && X < i[1])
          {
               ans.push_back({i[0], X});
               ans.push_back({X, i[1]});
          }
          else
          {
               ans.push_back(i);
          }
     }
     sort(ans.begin(), ans.end());
     for (auto i : ans)
          cout << i[0] << "-" << i[1] << endl;
}