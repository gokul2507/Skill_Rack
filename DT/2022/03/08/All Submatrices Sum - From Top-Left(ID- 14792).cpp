#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int r, c, temp;
     cin >> r >> c;
     int arr[1000] = {};
     for (int i = 0, val = 0; i < r; i++, val = 0)
     {
          for (int j = 0; j < c; j++)
          {
               cin >> temp;
               cout << arr[j] + val + temp << " ";
               val += temp;
               arr[j] += val;
          }
          cout << endl;
     }
}