R, C = map(int, input().split())
M = [list(map(int, input().split())) for i in range(R)]
for i in range(1, R):
    M[i][0] += M[i-1][0]
for j in range(1, C):
    M[0][j] += M[0][j-1]
for i in range(1, R):
    for j in range(1, C):
        M[i][j] += M[i-1][j] + M[i][j-1] - M[i-1][j-1]
[print(*L) for L in M]
