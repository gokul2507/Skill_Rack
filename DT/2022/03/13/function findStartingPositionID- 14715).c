#include <stdio.h>
#include <stdlib.h>

int *findStartingPosition(int R, int C, char *matrix, char *str)
{
     int len = strlen(str);
     int *arr = malloc(sizeof(int) * 2);
     for (int col = 0; col < C; col++)
     {
          int flag = 0;
          for (int row = 0; row < R; row++)
          {
               int index = 0;
               if (matrix[row * C + col] == str[index])
               {
                    int currRow = row;
                    while (matrix[row * C + col] == str[index] && str[index])
                    {
                         index++;
                         row++;
                         if (index == len)
                         {
                              arr[0] = currRow + 1;
                              arr[1] = col + 1;
                              return arr;
                         }
                    }
               }
          }
     }
     for (int col = 0; col < C; col++)
     {
          int flag = 0;
          for (int row = R - 1; row >= 0; row--)
          {
               int index = 0;
               if (matrix[row * C + col] == str[index])
               {
                    int currRow = row;
                    while (matrix[row * C + col] == str[index] && str[index])
                    {
                         index++;
                         row--;
                         if (index == len)
                         {
                              arr[0] = currRow + 1;
                              arr[1] = col + 1;
                              return arr;
                         }
                    }
               }
          }
     }
     return arr;
} // End of findStartingPosition function

int main()
{
     int R, C;
     scanf("%d%d", &R, &C);
     char matrix[R][C], str[C + 1];
     for (int row = 0; row < R; row++)
     {
          for (int col = 0; col < C; col++)
          {
               scanf(" %c", &matrix[row][col]);
          }
     }
     scanf("%s", str);
     int *pos = findStartingPosition(R, C, matrix, str);
     if (pos == NULL)
     {
          printf("Array is not formed\n");
     }
     printf("%d %d", pos[0], pos[1]);
     return 0;
}