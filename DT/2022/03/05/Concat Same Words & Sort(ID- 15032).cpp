#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     string s;
     vector<string> ans;
     map<string, int> mp;
     while (cin >> s)
     {
          mp[s]++;
     }
     for (auto &[key, val] : mp)
     {
          for (int i = 1; i <= val / 2; i++)
          {
               ans.push_back(key + key);
          }
          if (val & 1)
               ans.push_back(key);
     }
     sort(ans.begin(), ans.end());
     for (string i : ans)
          cout << i << " ";
}