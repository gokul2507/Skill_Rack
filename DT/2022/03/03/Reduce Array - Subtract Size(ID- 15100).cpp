#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int N;
     cin >> N;
     queue<int> q;
     while (N--)
     {
          int val;
          cin >> val;
          q.push(val);
     }
     while (q.empty() == false)
     {
          int sz = q.size();
          for (int i = 0; i < sz; i++)
          {
               int val = q.front() - sz;
               q.pop();
               if (val <= 0)
                    continue;
               cout << val << " ";
               q.push(val);
          }
          if (q.empty() == true)
               break;
          cout << endl;
     }
}