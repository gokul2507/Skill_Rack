import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner q = new Scanner(System.in);
          int n = q.nextInt();
          ArrayList<Integer> z = new ArrayList<Integer>();
          for (int i = 0; i < n; i++)
               z.add(q.nextInt());
          while (z.size() > 0) {
               int i = 0, t = z.size(), f = 1;
               while (i < z.size()) {
                    if (z.get(i) - t > 0) {
                         z.set(i, z.get(i) - t);
                         System.out.print(z.get(i) + " ");
                         i++;
                         f = 0;
                    } else {
                         z.remove(i);
                    }
               }
               if (f == 0)
                    System.out.println();
          }
     }
}