#include <stdio.h>
#include <stdlib.h>

int main()
{
     int N;
     scanf("%d", &N);
     int SIZE = N, arr[N];
     for (int index = 0; index < N; index++)
     {
          scanf("%d", arr + index);
     }
     while (SIZE > 0)
     {
          for (int index = 0; index < SIZE; index++)
          {
               arr[index] -= SIZE;
          }
          int tempArr[SIZE], currSize = 0;
          for (int index = 0; index < SIZE; index++)
          {
               if (arr[index] > 0)
               {
                    printf("%d ", arr[index]);
                    tempArr[currSize++] = arr[index];
               }
          }
          for (int index = 0; index < currSize; index++)
          {
               arr[index] = tempArr[index];
          }
          SIZE = currSize;
          if (SIZE > 0)
          {
               printf("\n");
          }
     }
}