n = int(input())
l = list(map(int, input().split()))
while len(l) >= 1:
    n = len(l)
    k = []
    for i in l:
        if i-n > 0:
            k.append(i-n)
    l = k[::]
    if l:
        print(*l)
