#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
#define int long long
     int N, ans = 0;
     cin >> N;
     while (N--)
     {
          string s;
          cin >> s;
          string t = "";
          for (char c : s)
          {
               if (isalpha(c))
               {
                    t += to_string(c - 'A' + 10);
               }
               else
                    t += c;
          }
          ans += stoll(t);
     }
     cout << ans;
}