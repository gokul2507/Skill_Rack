#include <stdio.h>
#include <stdlib.h>

int main()
{
     int N;
     long long int sum;
     scanf("%d\n", &N);
     for (int ctr = 1; ctr <= N; ctr++)
     {
          char S[101];
          scanf("%s ", S);
          long long int num = 0;
          for (int index = 0; S[index]; index++)
          {
               if (isdigit(S[index]))
               {
                    num = num * 10 + (S[index] - '0');
               }
               else
               {
                    S[index] = tolower(S[index]);
                    num = num * 100 + (S[index] - 97 + 10);
               }
          }
          sum += num;
     }
     printf("%lld", sum);
}