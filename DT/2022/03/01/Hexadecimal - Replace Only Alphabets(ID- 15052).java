import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner q = new Scanner(System.in);
          int n = Integer.parseInt(q.nextLine());
          long s = 0;
          for (int i = 0; i < n; i++) {
               String t = q.next(), w = "";
               for (int j = 0; j < t.length(); j++) {
                    if ('0' <= t.charAt(j) && t.charAt(j) <= '9') {
                         w += t.charAt(j);
                    } else {
                         w += (t.charAt(j) - 'A') + 10;
                    }
               }
               s += Long.parseLong(w);
          }
          System.out.print(s);
     }
}