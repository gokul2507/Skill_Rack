N = int(input())
totalMinutes = [0]*(24*60)
for ctr in range(N):
    start, end = input().split()
    startH, startM = map(int, start.split(":"))
    endH, endM = map(int, end.split(":"))
    for index in range(startH*60+startM, endH*60+endM):
        totalMinutes[index] = 1
print(totalMinutes.count(0))
