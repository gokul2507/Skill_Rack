#include <stdio.h>
#include <stdlib.h>

int main()
{
     int ans = 24 * 60, h0, m0, h1, m1;
     scanf("%d", &h0);
     while (~scanf("%d:%d %d:%d\n", &h0, &m0, &h1, &m1))
          ans -= (h1 - h0) * 60 + (m1 - m0);
     return printf("%d", ans);
}