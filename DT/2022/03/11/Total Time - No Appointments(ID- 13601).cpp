#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int N;
     cin >> N;
     int ans = 24 * 60;
     for (int i = 0; i < N; i++)
     {
          int h1, m1, h2, m2;
          char ch, ch2;
          cin >> h1 >> ch >> m1 >> h2 >> ch2 >> m2;
          int tot1 = h1 * 60 + m1, tot2 = h2 * 60 + m2;
          ans -= tot2 - tot1;
     }
     cout << ans;
}