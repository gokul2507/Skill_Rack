import java.util.*;

public class Hello {

     public static void main(String[] args) {
          // Your Code Here
          Scanner sc = new Scanner(System.in);
          int n = sc.nextInt();
          int total = 1440;
          for (int i = 0; i < n; i++) {
               String str1 = sc.next();
               String str2 = sc.next();
               String[] arr1 = str1.split("[:]");
               String[] arr2 = str2.split("[:]");
               int a = Integer.parseInt(arr2[0]) - Integer.parseInt(arr1[0]);
               a *= 60;
               a -= Integer.parseInt(arr1[1]);
               a += Integer.parseInt(arr2[1]);
               total -= a;
          }
          System.out.println(total);
     }
}