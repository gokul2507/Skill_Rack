#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int r, c;
     cin >> r >> c;
     vector<int> ans;
     string arr[r][c];
     for (int i = 0; i < r; i++)
     {
          for (int j = 0; j < c; j++)
          {
               cin >> arr[i][j];
          }
     }
     for (int i = 0; i < r; i += 3)
     {
          for (int j = 0; j < c; j += 3)
          {
               string k1, k2;
               for (int row = 0; row < 3; row++)
               {
                    string a, b;
                    for (int col = 0; col < 3; col++)
                    {
                         a += arr[i + row][j + col];
                         b += arr[i + col][j + row];
                    }
                    // cerr<<a<<" "<<b<<endl;
                    ans.push_back(stoi(a));
                    ans.push_back(stoi(b));
                    k1 += arr[i + row][j + row];
                    k2 += arr[row + i][j + 2 - row];
               }
               ans.push_back(stoi(k1));
               ans.push_back(stoi(k2));
          }
     }
     sort(ans.begin(), ans.end());
     for (int i : ans)
     {
          cout << i << " ";
     }
}