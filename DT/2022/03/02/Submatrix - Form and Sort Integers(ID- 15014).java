import java.util.*;

public class Hello {
     static int[][] a;
     static int r, c;
     static ArrayList<Integer> z = new ArrayList<Integer>();

     public static int line(int x, int y, int xi, int yi) {
          int t = 0, tx = x + 3, ty = y + 3;
          while (x < tx && y < ty) {
               t *= 10;
               t += a[x][y];
               x += xi;
               y += yi;
          }
          return t;
     }

     public static void pro(int x, int y) {
          z.add(line(x, y, 0, 1));
          z.add(line(x + 1, y, 0, 1));
          z.add(line(x + 2, y, 0, 1));
          z.add(line(x, y, 1, 0));
          z.add(line(x, y + 1, 1, 0));
          z.add(line(x, y + 2, 1, 0));
          z.add(line(x, y, 1, 1));
          z.add(line(x, y + 2, 1, -1));

     }

     public static void main(String[] args) {
          Scanner q = new Scanner(System.in);
          r = q.nextInt();
          c = q.nextInt();
          a = new int[r][c];
          for (int i = 0; i < r; i++) {
               for (int j = 0; j < c; j++) {
                    a[i][j] = q.nextInt();
                    if ((i + 1) % 3 == 0 && (j + 1) % 3 == 0) {
                         pro(i - 2, j - 2);
                    }
               }
          }
          Collections.sort(z);
          for (int i : z)
               System.out.print(i + " ");
     }
}