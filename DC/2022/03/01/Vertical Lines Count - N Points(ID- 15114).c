#include <stdio.h>
#include <stdlib.h>

int main()
{
     int n, count = 0;
     scanf("%d", &n);
     int x[n], y[n];
     for (int i = 0; i < n; i++)
     {
          scanf("%d %d", &x[i], &y[i]);
     }
     int l;
     scanf("%d", &l);
     for (int i = 0; i < n; i++)
     {
          for (int j = i + 1; j < n; j++)
          {
               if (x[i] == x[j])
               {
                    if (y[i] > y[j])
                    {
                         if (y[i] - y[j] == l)
                         {
                              count++;
                         }
                    }
                    else
                    {
                         if (y[j] - y[i] == l)
                         {
                              count++;
                         }
                    }
               }
          }
     }
     printf("%d", count);
}