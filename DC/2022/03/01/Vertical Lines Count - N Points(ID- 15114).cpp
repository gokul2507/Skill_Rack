#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n;
     cin >> n;
     int x[n], y[n];
     for (int i = 0; i < n; i++)
          cin >> x[i] >> y[i];
     int l;
     cin >> l;
     int sum = 0;
     for (int i = 0; i < n; i++)
     {
          // cout<<x[i]<<" "<<y[i]<<endl;
          for (int j = 0; j < n; j++)
          {
               if (x[i] == x[j] && i != j)
               {
                    if (y[j] == (y[i] + l) || y[j] == (y[i] - l))
                         sum += 1;
               }
          }
     }
     cout << sum / 2;
}