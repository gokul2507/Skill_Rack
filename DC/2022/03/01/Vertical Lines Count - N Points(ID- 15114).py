n = int(input())
pts = [list(map(int, input().split())) for i in range(n)]
l = int(input())
sor_pts = sorted(pts, key=lambda x: x[0])
# print(sor_pts)
count = 0
for i in range(len(sor_pts)):
    for j in range(i, n):
        if sor_pts[i][0] == sor_pts[j][0]:
            if abs(sor_pts[i][1] - sor_pts[j][1]) == l:
                count += 1
        else:
            break
print(count)
