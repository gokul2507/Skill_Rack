#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int N;
     cin >> N;
     vector<int> a(N);
     for (int &i : a)
     {
          cin >> i;
     }
     int C, curr = 0;
     cin >> C;
     for (int i = 0; i < a.size(); i++)
     {
          if (a[i] < C)
          {
               for (int j = i + 1; j < a.size(); j++)
               {
                    int need = C - a[j];
                    if (need < a[i])
                    {
                         a[j] = C;
                         a[i] -= need;
                    }
                    else
                    {
                         a[j] += a[i];
                         a[i] = 0;
                         break;
                    }
               }
          }
     }
     for (int i : a)
          if (i)
               cout << i << " ";
}