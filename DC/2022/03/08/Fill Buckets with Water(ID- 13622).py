# Your code below
n = int(input())
l = list(map(int, input().split()))
v = int(input())
i = 0
while(i < n):
    j = i+1
    while(j < n and v > l[i] > 0):
        if(l[j] < v):
            l[j] += 1
            l[i] -= 1
        else:
            j += 1
    i = j

print(*filter(lambda x: x > 0, l))
