#include <stdio.h>
#include <stdlib.h>

int main()
{
     int n;
     scanf("%d", &n);
     int arr[n], C;

     for (int i = 0; i < n; i++)
     {
          scanf("%d", &arr[i]);
     }
     scanf("%d", &C);
     for (int i = 0; i < n; i++)
     {
          if (arr[i] != C)
          {
               for (int j = i + 1; j < n; j++)
               {
                    if (arr[j] < C)
                    {
                         while (arr[j] != C && arr[i] != 0)
                         {
                              arr[j] += 1;
                              arr[i] -= 1;
                         }
                    }
               }
          }
     }
     for (int i = 0; i < n; i++)
     {
          if (arr[i] != 0)
               printf("%d ", arr[i]);
     }
}