import java.util.Scanner;

class Stick {
    int st;

    public Stick(int x) {
        st = x;
    }

    public int getLength() {
        return st;
    }

    public void combineStick(Stick x) {
        if (x.getLength() == 0) {
            System.out.println("Length of the stick cannot be zero");
        } else {
            this.st += x.getLength();
        }
    }

    public void breakStick() {
        if (this.st % 2 == 0) {
            this.st /= 2;
        } else {
            System.out.println("Length of the stick cannot be odd");
        }
    }
}

public class Hello {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int stickLen = Integer.parseInt(sc.nextLine());
        Stick stick = new Stick(stickLen);
        int Q = Integer.parseInt(sc.nextLine());
        for (int ctr = 1; ctr <= Q; ctr++) {
            String query[] = sc.nextLine().trim().split("\\s+");
            int queryType = Integer.parseInt(query[0]);
            if (queryType == 1) {
                stick.combineStick(new Stick(Integer.parseInt(query[1])));
            } else if (queryType == 2) {
                stick.breakStick();
            }
            System.out.println(stick.getLength());
        }
    }
}