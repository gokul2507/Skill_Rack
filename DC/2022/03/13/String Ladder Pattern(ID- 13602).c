#include <stdio.h>
#include <stdlib.h>
int main()
{
     char s1[101], s2[101];
     int n;
     scanf("%s\n%s\n%d\n", s1, s2, &n);
     int l1 = strlen(s1);
     int l2 = strlen(s2);
     int row = (l1 * (n + 1)) - n;
     int col = l2;
     char mat[row][col];
     memset(mat, '*', row * col);
     int sr = 0, k = 0;
     for (int ctr = 0; ctr <= n; ctr++)
     {
          if (ctr % 2 == 0)
          {
               for (; k < l1; k++)
               {
                    mat[sr][0] = s1[k];
                    mat[sr][l2 - 1] = s1[k];
                    sr++;
               }
               k = l1 - 2;
          }
          else
          {
               for (; k >= 0; k--)
               {
                    mat[sr][0] = s1[k];
                    mat[sr][l2 - 1] = s1[k];
                    sr++;
               }
               k = 1;
          }
          if (ctr != n)
          {
               for (int j = 1; j < l2 - 1; j++)
                    mat[sr - 1][j] = s2[j];
          }
     }
     for (int i = 0; i < row; i++, printf("\n"))
          for (int j = 0; j < col; j++)
               printf("%c", mat[i][j]);
}