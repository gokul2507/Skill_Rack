import java.util.*;

public class Hello {

     public static void main(String[] args) {
          // Your Code Here
          Scanner sc = new Scanner(System.in);
          char[] s1 = sc.next().toCharArray();
          String s2 = sc.next();
          int n = sc.nextInt(), printed = 0, gap = s2.length() - 2, index = 0;
          while (true) {
               for (; index < s1.length - 1; index++) {
                    String star = "*".repeat(gap);
                    System.out.println(s1[index] + star + s1[index]);
               }
               if (printed != n) {
                    System.out.println(s2);
               } else {
                    System.out.println(s1[index] + ("*".repeat(gap)) + s1[index]);
                    break;
               }
               rev(s1);
               index = 1;
               printed++;
          }
          // System.out.println("bala");
     }

     static void rev(char[] arr) {
          int lo = 0, hi = arr.length - 1;
          while (lo < hi) {
               char temp = arr[lo];
               arr[lo] = arr[hi];
               arr[hi] = temp;
               lo++;
               hi--;
          }
     }
}