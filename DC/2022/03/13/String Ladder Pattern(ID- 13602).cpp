#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     string s1, s2;
     int N;
     cin >> s1 >> s2;
     cin >> N;
     N += 1;
     int size = s1.size();
     string astrid;
     for (int i = 0; i < s2.size() - 2; i++)
          astrid.push_back('*');
     int iter = 1, start = 1;
     cout << s1[0] << astrid << s1[0] << endl;
     while (N)
     {
          for (int i = start; i > 0 && i < size - 1; i += iter)
          {
               cout << s1[i] << astrid << s1[i] << endl;
          }
          if (N == 1)
               break;
          cout << s2 << endl;
          if (start == 1)
               start = size - 2;
          else
               start = 1;
          iter *= -1;
          N--;
     }
     cout << s1[0] << astrid << s1[0] << endl;
}