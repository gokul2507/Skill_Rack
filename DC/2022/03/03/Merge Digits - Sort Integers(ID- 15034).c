#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main()
{
     int n;
     scanf("%d\n", &n);
     char str[1000001];
     scanf("%[^\n]", str);
     char *t = strtok(str, " ");
     char arr[n][1000001];
     int k = 0;
     while (t != NULL)
     {
          strcpy(arr[k], t);
          k += 1;
          t = strtok(NULL, " ");
     }
     int a[n], s = 0;
     char x[1001], y[1001];
     for (int i = 0; i < k; i += 2)
     {
          char z[1001];
          int m = 0;
          strcpy(x, arr[i]);
          strcpy(y, arr[i + 1]);
          for (int j = 0; x[j]; j++)
          {
               if ((x[j] - '0') > y[j] - '0')
               {
                    z[m++] = x[j];
                    z[m++] = y[j];
               }
               else
               {
                    z[m++] = y[j];
                    z[m++] = x[j];
               }
          }
          a[s++] = atoi(z);
     }
     for (int i = 0; i < s; i++)
     {
          for (int j = 0; j < s - i - 1; j++)
          {
               if (a[j] > a[j + 1])
               {
                    int r = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = r;
               }
          }
     }
     for (int i = 0; i < s; i++)
     {
          printf("%d ", a[i]);
     }
     return 0;
}