import java.util.*;
public class Hello {
    public static String fun(String x,String y){
        String a="";
        for(int i=2;i>=0;i--){
            if(x.charAt(i)-'0'<y.charAt(i)-'0')
            a=""+y.charAt(i)+x.charAt(i)+a;
            else
            a=""+x.charAt(i)+y.charAt(i)+a;
        }
        return a;
    }
    public static void main(String[] args) {
        Scanner q=new Scanner(System.in);
        int n=q.nextInt();
        q.nextLine();
        ArrayList<Integer> z=new ArrayList<Integer>();
        for(int i=0;i<n;i+=2){
            z.add(Integer.parseInt(fun(q.next(),q.next())));
        }
        Collections.sort(z);
        for(int i:z)
        System.out.print(i+" ");
	}
}