#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
void *sortarray(int *arr, int n)
{
     int i, j;
     for (i = 0; i < n; i++)
     {
          for (j = i + 1; j < n; j++)
          {
               if (arr[i] > arr[j])
               {
                    int t;
                    t = arr[i];
                    arr[i] = arr[j];
                    arr[j] = t;
               }
          }
     }
}
int main()
{
     int r, c;
     scanf("%d %d", &r, &c);
     int mat[r][c], i, j, arr[r * c], ind = 0;
     ;
     for (i = 0; i < r; i++)
     {
          for (j = 0; j < c; j++)
          {
               scanf("%d ", &mat[i][j]);
               arr[ind++] = mat[i][j];
          }
     }
     sortarray(arr, ind);
     int g = 0, s = 0;
     while (g < ind - 1)
     {
          int xrow, xcol, yrow, ycol;
          for (i = 0; i < r; i++)
          {
               for (j = 0; j < c; j++)
               {
                    if (mat[i][j] == arr[g])
                    {
                         xrow = i;
                         xcol = j;
                    }
                    if (mat[i][j] == arr[g + 1])
                    {
                         yrow = i;
                         ycol = j;
                    }
               }
          }
          int h = fmax(abs(xrow - yrow), abs(xcol - ycol));
          s += h;
          g++;
     }
     printf("%d", s);
}