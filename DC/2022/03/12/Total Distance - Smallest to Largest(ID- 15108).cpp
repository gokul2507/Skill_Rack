#include <bits/stdc++.h>

using namespace std;
struct cm
{
     bool operator()(const tuple<int, int, int> &a, const tuple<int, int, int> &b)
     {
          return get<0>(a) <= get<0>(b);
     }
};
int main(int argc, char **argv)
{
     int r, c, res = 0;
     cin >> r >> c;
     vector<vector<int>> matrix(r, vector<int>(c));
     priority_queue<tuple<int, int, int>, vector<tuple<int, int, int>>, cm> pq;
     for (int i = 0; i < r; i++)
     {
          for (int j = 0; j < c; j++)
          {
               cin >> matrix[i][j];
               tuple<int, int, int> cod(matrix[i][j], i, j);
               pq.push(cod);
               // cout<<pq.size()<<endl;
          }
     }
     while (!pq.empty())
     {
          tuple<int, int, int> a = pq.top();
          pq.pop();
          if (pq.empty())
               break;
          auto b = pq.top();
          res += max(abs(get<1>(a) - get<1>(b)), abs(get<2>(a) - get<2>(b)));
     }
     cout << res;
     return 0;
}