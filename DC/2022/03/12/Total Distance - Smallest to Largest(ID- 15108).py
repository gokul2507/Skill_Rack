x,y=map(int,input().split())
z=[]
for i in range(x):
    t=input().split()
    for j in range(y):
        z.append((int(t[j]),i,j))
z.sort()
ans=0
for i in range(0,x*y-1):
    ans+=max(abs(z[i][1]-z[i+1][1]),abs(z[i][2]-z[i+1][2]))
print(ans)