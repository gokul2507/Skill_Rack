a = int(input())
s = ""
if a > 45:
    print("-1")
    exit()
while a > 0:
    if a < 10:
        if len(s) == 4:
            s += str(a)
            break
        s += str(a-1)
        s += "0"*(4-len(s))
        s += "1"
        break
    s += "9"
    a -= 9
print(s[::-1])
