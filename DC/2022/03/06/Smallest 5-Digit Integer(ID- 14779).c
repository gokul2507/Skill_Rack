#include <stdio.h>
#include <stdlib.h>

int main()
{
     int N;
     scanf("%d", &N);
     if (N > 45)
          printf("-1");
     else
     {
          for (int i = 10000; i <= 99999; i++)
          {

               int sum = 0, n = i;
               while (n > 0)
               {
                    int dig = n % 10;
                    sum += dig;
                    n /= 10;
               }
               if (sum == N)
               {
                    printf("%d", i);
                    break;
               }
          }
     }
}