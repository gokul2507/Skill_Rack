import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner sc = new Scanner(System.in);
          int s = sc.nextInt();
          int i = 10000, flag = 0;
          while (flag == 0 && i < 100000) {
               int temp = i, sum = 0;
               while (temp > 0) {
                    sum += (temp % 10);
                    temp /= 10;
               }
               if (sum == s) {
                    System.out.println(i);
                    flag = 1;
               } else {
                    i++;
               }
          }
          if (flag == 0)
               System.out.print("-1");

     }
}