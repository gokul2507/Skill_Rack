#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n, ans;
     cin >> n;
     for (int i = 10000; i <= 99999; i++)
     {
          int t = i, sum = 0;
          if (n > 45)
          {
               ans = -1;
               break;
          }
          else
          {
               while (t)
               {
                    int temp = t % 10;
                    sum += temp;
                    t /= 10;
               }
          }
          if (sum == n)
          {
               ans = i;
               break;
          }
     }
     cout << ans;
}