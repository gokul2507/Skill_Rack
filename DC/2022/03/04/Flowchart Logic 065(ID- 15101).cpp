#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int x, y, d, temp;
     cin >> x >> y >> d;
     while (x <= y)
     {
          temp = x;
          while (temp)
          {
               if (temp % 10 == d)
               {
                    cout << x << " ";
                    break;
               }
               temp /= 10;
          }
          x += 1;
     }
}