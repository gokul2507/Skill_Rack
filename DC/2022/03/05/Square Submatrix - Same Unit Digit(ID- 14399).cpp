#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int r,c,n,i,j,k,l;
    cin>>r>>c;
    int mat[r][c];
    for(auto &i:mat)
    {
        for(auto &j:i)
        {
            cin>>j;
        }
    }
    cin>>n;
    for(i=0;i<=r-n;i++)
    {
        for(j=0;j<=c-n;j++)
        {
            set<int> co;
            for(k=i;k<i+n;k++)
            {
                for(l=j;l<j+n;l++)
                {
                    co.insert(mat[k][l]%10);
                }
            }
            if(co.size()==1)
            {
                cout<<"Yes";
                return 0;
            }
        }
    }
    cout<<"No";


}