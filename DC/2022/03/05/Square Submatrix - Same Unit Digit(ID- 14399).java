import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner q = new Scanner(System.in);
          int r = q.nextInt(), c = q.nextInt();
          int[][] a = new int[r][c];
          for (int i = 0; i < r; i++)
               for (int j = 0; j < c; j++)
                    a[i][j] = q.nextInt() % 10;
          int n = q.nextInt(), ut = 1;
          for (int i = 0; i <= r - n && ut == 1; i++) {
               for (int j = 0; j <= c - n; j++) {
                    int t = a[i][j], f = 1;
                    for (int w = i; w < i + n && f == 1; w++) {
                         for (int e = j; e < j + n; e++) {
                              if (a[w][e] != t) {
                                   f = 0;
                                   break;
                              }
                         }
                    }
                    if (f == 1) {
                         System.out.println("Yes");
                         ut = 0;
                         break;
                    }
               }
          }
          if (ut == 1)
               System.out.println("No");
     }
}