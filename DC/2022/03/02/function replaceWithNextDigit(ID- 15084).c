#include <stdio.h>
#include <stdlib.h>
char fun(char x)
{
     if (x > '9')
          return '0';
     if (x < '0')
          return '9';
     return x;
}
void replaceWithNextDigit(int N)
{
     char a[100];
     sprintf(a, "%d", N);
     int l = 0, ans[100];
     for (int i = 0; a[i] != '\0'; i++)
     {
          a[i] = fun(a[i] + 1);
          ans[l++] = atoi(a);
          a[i] = fun(a[i] - 1);
     }
     for (int i = 0; i < l; i++)
     {
          for (int j = i + 1; j < l; j++)
          {
               if (ans[i] > ans[j])
               {
                    ans[i] += ans[j];
                    ans[j] = ans[i] - ans[j];
                    ans[i] -= ans[j];
               }
          }
          printf("%d ", ans[i]);
     }
}
int main()
{
     int N;
     scanf("%d", &N);
     replaceWithNextDigit(N);
     return 0;
}