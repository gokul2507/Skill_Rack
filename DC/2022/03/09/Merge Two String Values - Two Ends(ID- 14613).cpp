#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     string s1, s2;
     vector<char> v;
     cin >> s1 >> s2;
     int l = min(s1.length(), s2.length());
     for (int i = 0; i < l; i++)
     {
          if (s1[i] < s2[i])
          {
               v.push_back(s2[i]);
               cout << s1[i];
          }
          else
          {
               v.push_back(s1[i]);
               cout << s2[i];
          }
     }
     while (l < s1.length())
          cout << s1[l++];
     while (l < s2.length())
          cout << s2[l++];
     reverse(v.begin(), v.end());
     for (char a : v)
          cout << a;
}