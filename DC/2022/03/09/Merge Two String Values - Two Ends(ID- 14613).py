a = input().strip()
b = input().strip()
l, r = "", ""
for i in range(min(len(a), len(b))):
    if ord(a[i]) < ord(b[i]):
        l += a[i]
        r = b[i]+r
    else:
        l += b[i]
        r = a[i]+r
if len(a) > len(b):
    l += a[len(b):]
else:
    l += b[len(a):]
print(l+r)
