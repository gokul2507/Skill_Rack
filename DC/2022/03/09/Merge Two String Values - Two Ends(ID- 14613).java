import java.util.*;
public class Hello {

    public static void main(String[] args) {
		//Your Code Here
		Scanner sc = new Scanner(System.in);
		char[] s1  = sc.next().toCharArray();
		char[] s2 = sc.next().toCharArray();
		
		int length = s1.length+s2.length, lo = 0,hi = length-1;
		char[] s3 = new char[length];
		int ind1 = 0,ind2 = 0;
		while(ind1!=s1.length && ind2!=s2.length){
		    if(s1[ind1]<s2[ind2]){
		        s3[lo] = s1[ind1];
		        s3[hi] = s2[ind2];
		    }else{
		        s3[lo] = s2[ind2];
		        s3[hi] = s1[ind1];
		    }
		    lo++;
		    hi--;
		    ind1++;
		    ind2++;
		}
		while(ind1!=s1.length){
		    s3[lo++] = s1[ind1++];
		}while(ind2!=s2.length){
		    s3[lo++] = s2[ind2++];
		}
		System.out.println(String.valueOf(s3));
	}
}