#include <stdio.h>
#include <stdlib.h>

int main()
{
     char s1[100];
     char s2[100];
     scanf("%s\n%s", s1, s2);
     int c = strlen(s1);
     int d = strlen(s2);
     int m = c > d ? d : c;
     for (int i = 0; i < m; i++)
     {
          if (s1[i] < s2[i])
          {
               printf("%c", s1[i]);
          }
          else
          {
               printf("%c", s2[i]);
          }
     }
     if (c < d)
     {
          for (int i = c; i < d; i++)
          {
               printf("%c", s2[i]);
          }
     }
     else
     {
          for (int i = d; i < c; i++)
          {
               printf("%c", s1[i]);
          }
     }
     char ar[100];
     int k = 0;
     for (int i = 0; i < m; i++)
     {
          if (s1[i] < s2[i])
          {
               ar[k++] = s2[i];
          }
          else
          {
               ar[k++] = s1[i];
          }
     }
     for (int i = m - 1; i >= 0; i--)
     {
          printf("%c", ar[i]);
     }
}