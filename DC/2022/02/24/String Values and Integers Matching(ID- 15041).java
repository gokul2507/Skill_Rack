import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner q = new Scanner(System.in);
          int n = q.nextInt();
          List<String> z = new ArrayList<String>();
          Map<String, Integer> d = new HashMap<String, Integer>();
          for (int i = 0; i < n; i++) {
               q.nextLine();
               String s = q.next().replace("one", "1").replace("two", "2").replace("three", "3").replace("four", "4")
                         .replace("five", "5").replace("six", "6").replace("seven", "7").replace("eight", "8")
                         .replace("nine", "9").replace("zero", "0"), p = q.next();
               d.put(p, i);
               z.add(s);
          }
          for (String i : z)
               System.out.print(d.get(i) + 1 + " ");
     }
}