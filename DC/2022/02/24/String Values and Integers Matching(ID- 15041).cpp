#include <bits/stdc++.h>

using namespace std;
int g(string s)
{
     vector<string> mp = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
     int ans = 0;
     string t = "";
     for (char c : s)
     {
          t += c;
          for (int i = 0; i < 10; i++)
          {
               if (mp[i] == t)
               {
                    ans = ans * 10 + i;
                    t = "";
                    break;
               }
          }
     }
     return ans;
}
int main(int argc, char **argv)
{
     int N;
     cin >> N;
     vector<int> arr(N);
     map<int, int> mp;
     for (int i = 0; i < N; i++)
     {
          string s;
          int val;
          cin >> s >> val;
          arr[i] = g(s);
          mp[val] = i;
     }
     for (int i : arr)
          cout << mp[i] + 1 << " ";
}