#include <stdio.h>
#include <stdlib.h>

int toto(char *a)
{
     int t = 0;
     for (int i = 0; i < strlen(a); i++)
     {
          t *= 10;
          if (a[i] == 'o')
          {
               t += 1;
               i += 2;
          }
          else if (a[i] == 'z')
          {
               i += 3;
          }
          else if (a[i] == 't')
          {
               if (a[i + 1] == 'w')
               {
                    t += 2;
                    i += 2;
               }
               else
               {
                    t += 3;
                    i += 4;
               }
          }
          else if (a[i] == 'f')
          {
               if (a[i + 1] == 'o')
               {
                    t += 4;
                    i += 3;
               }
               else
               {
                    t += 5;
                    i += 3;
               }
          }
          else if (a[i] == 's')
          {
               if (a[i + 1] == 'i')
               {
                    t += 6;
                    i += 2;
               }
               else
               {
                    t += 7;
                    i += 4;
               }
          }
          else if (a[i] == 'e')
          {
               t += 8;
               i += 4;
          }
          else if (a[i] == 'n')
          {
               t += 9;
               i += 3;
          }
     }
     return t;
}

int main()
{
     int n;
     scanf("%d ", &n);
     int ar[n];
     char arr[n][1000];
     for (int i = 0; i < n; i++)
     {
          scanf("%s %d\n", arr[i], &ar[i]);
     }
     for (int i = 0; i < n; i++)
     {
          int k = toto(arr[i]);
          for (int j = 0; j < n; j++)
          {
               if (k == ar[j])
               {
                    printf("%d ", j + 1);
                    break;
               }
          }
     }
}