n = int(input())
a, b = [], []
for _ in range(n):
    ip = input().split()
    a.append(ip[0].replace('one', '1').replace('two', '2').replace('three', '3').replace('four', '4').replace(
        'five', '5').replace('six', '6').replace('seven', '7').replace('eight', '8').replace('nine', '9').replace('zero', '0'))
    b.append(ip[1])
for i in a:
    print(b.index(i) + 1, end=' ')
