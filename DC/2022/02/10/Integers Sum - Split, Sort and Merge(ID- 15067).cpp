#include <bits/stdc++.h>

using namespace std;
int it(string t)
{
     stringstream out(t);
     int y;
     out >> y;
     return y;
}
int main(int argc, char **argv)
{
     int n, s1 = 0, s2 = 0;
     cin >> n;
     vector<int> z;
     string t, p;
     getline(cin, t);
     for (int i = 0; i < n; i++)
     {
          getline(cin, t, '|');
          cin >> p;
          s1 += it(t + p);
          z.push_back(it(t));
          z.push_back(it(p));
     }
     sort(z.begin(), z.end());
     for (int i = 0; i < n * 2; i += 2)
          s2 += it(to_string(z[i]) + to_string(z[i + 1]));
     cout << s1 << " " << s2;
}