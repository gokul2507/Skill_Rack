import java.util.*;

public class Hello {

	public static void main(String[] args) {
		// Your Code Here
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		ArrayList<Integer> li = new ArrayList<>();
		int s1 = 0, s2 = 0;
		for (int i = 0; i < n; i++) {
			String[] s = sc.next().split("\\|");
			s1 += Integer.parseInt(s[0] + s[1]);
			li.add(Integer.valueOf(s[0]));
			li.add(Integer.valueOf(s[1]));
		}
		Collections.sort(li);
		int sum2 = 0;
		for (int i = 0; i < li.size() - 1; i = i + 2) {
			sum2 += Integer.parseInt(li.get(i) + "" + li.get(i + 1));

		}
		System.out.println(s1 + " " + sum2);

	}
}