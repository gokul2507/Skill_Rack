n = int(input())
s = input().strip().split()
x, y, res = 0, 0, []
for i in s:
    x += int(i.replace('|', ''))
for j in s:
    if '|' in j:
        res.extend(map(int, j.split("|")))
    else:
        res.append(int(j))
res = sorted(res)
res = [str(h) for h in res]
for k in range(n):
    y += int("".join(res[:2]))
    res = res[2:]
print(x, y)
