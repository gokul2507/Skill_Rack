#include <stdio.h>
#include <string.h>

int sort(int *a, int *b)
{
     return *a - *b;
}
int main()
{
     int N;
     char str[1001][101], newStr[1001][101];
     scanf("%d ", &N);
     for (int index = 0; index < N; index++)
          scanf("%s ", str[index]);
     int sum1 = 0, sum2 = 0, a = 0, len = 0, arr[N * 2];
     for (int index = 0; index < N; index++)
     {
          char *s1 = strtok(str[index], "|");
          char *s2 = strtok(NULL, "|");
          arr[len++] = atoi(s1);
          arr[len++] = atoi(s2);
          sum1 += atoi(strcat(s1, s2));
     }
     qsort(arr, len, sizeof(int), sort);
     for (int index = 0; index < len; index += 2)
     {
          sprintf(newStr[a], "%d%d", arr[index], arr[index + 1]);
          sum2 += strtol(newStr[a], NULL, 10);
          a++;
     }
     printf("%d %d", sum1, sum2);
}