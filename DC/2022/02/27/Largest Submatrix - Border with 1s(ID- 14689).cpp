#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int R, C;
     cin >> R >> C;
     vector<vector<int>> mat(R, vector<int>(C));
     for (auto &i : mat)
          for (auto &j : i)
               cin >> j;
     for (int i = R; i >= 2; i--)
     {
          for (int j = 0; j <= R - i; j++)
          {
               for (int k = 0; k <= C - i; k++)
               {
                    for (int l = j; l < j + i; l++)
                    {
                         for (int m = k; m < k + i; m++)
                         {
                              if (l == j || l == j + i - 1 || m == k || m == k + i - 1)
                              {
                                   if (mat[l][m] == 0)
                                        goto end;
                              }
                         }
                    }
                    cout << i;
                    return 0;
               end:;
               }
          }
     }
     cout << -1;
}