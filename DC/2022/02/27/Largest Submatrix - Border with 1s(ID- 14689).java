import java.util.*;

public class Hello {

    static int R, C, m[][] = new int[50][50];

    static boolean f(int r, int c, int n) {
        int i;
        for (i = c; i < c + n; i++)
            if (m[r][i] == 0 || m[r + n - 1][i] == 0)
                return false;
        for (i = r + 1; i < r + n - 1; i++)
            if (m[i][c] == 0 || m[i][c + n - 1] == 0)
                return false;
        return true;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        R = sc.nextInt();
        C = sc.nextInt();
        int i, j, r, c, n = -1, N;
        for (i = 0; i < R; i++)
            for (j = 0; j < C; m[i][j++] = sc.nextInt())
                ;
        for (r = 0; r < R - 1; r++)
            for (c = 0; c < C - 1; c++)
                for (N = Math.min(R - r, C - c); N > 1; N--)
                    if (f(r, c, N)) {
                        n = Math.max(n, N);
                        break;
                    }
        System.out.format("%d", n);
    }
}