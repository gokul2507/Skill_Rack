x, y = map(int, input().split())
z = [input().split() for _ in range(x)]
m = -1
for k in range(min(x, y), 1, -1):
    for r in range(0, x-k+1):
        for c in range(0, y-k+1):
            for i in range(r, r+k):
                for j in range(c, c+k):
                    if (i == r or i == r+k-1) or (j == c or j == c+k-1):
                        if z[i][j] == '0':
                            break
                else:
                    continue
                break
            else:
                m = max(m, k)
print(m)
