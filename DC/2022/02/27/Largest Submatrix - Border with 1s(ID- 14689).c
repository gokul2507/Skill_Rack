#include<stdio.h>
#include<stdlib.h>

int main()
{
    int min(int a,int b){
        return a<b?a:b;
    }
    int r,c;
    scanf("%d %d ",&r,&c);
    int arr[r][c];
    for(int i=0;i<r;i++){
        for(int j=0;j<c;j++){
            scanf("%d ",&arr[i][j]);
        }
    }
    int max=-1;
    for(int x=min(r,c);x>=2;x--){
        for(int i=0;i<r-x+1;i++){
            for(int j=0;j<c-x+1;j++){
                int p=0,q=0;
                for(int k=i;k<i+x;k++){
                    for(int l=j;l<j+x;l++){
                        if(k==i || l==j || k==i+x-1 || l==j+x-1){
                            p++;
                            q+=arr[k][l]==1;
                        }
                    }
                }
                if(p==q){
                    max=x>max?x:max;
                }
            }
        }
    }
    printf("%d ",max);
}