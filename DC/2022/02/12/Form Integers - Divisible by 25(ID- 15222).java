import java.util.*;

public class Hello {
     public static void main(String[] args) {
          // Your Code Here
          Scanner sc = new Scanner(System.in);
          String s = sc.next();
          Queue<String> q = new LinkedList<>();
          if (s.contains("X")) {
               for (int i = 0; i < 10; i++)
                    q.add(s.replaceAll("X", "" + i));
          } else
               q.add(s);
          int count = 0;
          while (!q.isEmpty()) {
               String curr = q.poll();
               if (curr.contains("_")) {
                    int ind = curr.indexOf("_");
                    char[] arr = curr.toCharArray();
                    for (int i = 0; i < 10; i++) {
                         arr[ind] = (char) ('0' + i);
                         String sub = String.valueOf(arr);
                         q.add(sub);
                    }
               } else {
                    if (isValid(curr))
                         count++;
               }
          }
          System.out.println(count);
     }

     static boolean isValid(String s) {
          return (s.length() == 1 || s.charAt(0) != '0') && Integer.parseInt(s) % 25 == 0;
     }
}