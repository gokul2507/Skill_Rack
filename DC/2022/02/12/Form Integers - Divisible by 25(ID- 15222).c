#include <stdio.h>
#include <stdlib.h>

int main()
{
     char a[8], num[8];
     int arr[8], arr1[8], y = 0, x = 0, s = 1, d = 0, ct = 0, h = 0;
     scanf("%s", &a);
     if (strlen(a) == 1)
     {
          printf("1");
          return;
     }
     for (int i = 0; i < strlen(a); i++)
     {
          if (a[i] == 'X')
          {
               arr[y++] = i;
          }
          else if (isdigit(a[i]))
          {
               arr1[x++] = i;
          }
     }
     int l = strlen(a);
     for (int i = 1; i <= l; i++)
     {
          s *= 10;
     }
     for (int i = s / 10; i < s; i++)
     {
          d = 0, h = 0;
          if (i % 25 == 0)
          {
               sprintf(num, "%d", i);
               for (int j = 0; j < y - 1; j++)
               {
                    if (num[arr[j]] != num[arr[j + 1]])
                    {
                         d = 1;
                         break;
                    }
               }
               for (int j = 0; j < x; j++)
               {
                    if (a[arr1[j]] != num[arr1[j]])
                    {
                         h = 1;
                         break;
                    }
               }
               if (d == 0 && h == 0)
               {
                    if (!isdigit(a[0]) || a[0] == '0')
                    {
                         ct++;
                    }
                    else if (isdigit(a[0]))
                    {
                         int t = a[0] - '0';
                         int t1 = num[0] - '0';
                         if (t == t1)
                         {
                              ct++;
                         }
                    }
               }
          }
     }
     printf("%d", ct);
}