num = int(input())
t1 = num*2 - 2
t2 = 1
for n in range(2*num - 1):
    if n < num - 1:
        print('-'*t1, end = '')
        t1 -= 2
        if n == 0:
            print('*')
        else:
            print('*','-'*t2,'*','-'*t2,'*', sep='')
            t2 += 2
    elif n > num - 1:
        t1 += 2
        print('-'*t1, end='')
        if n == 2*num - 2:
            print('*')
        else:
            t2 -= 2
            print('*', '-'*t2, '*', '-'*t2, '*', sep = '')
    else:
        print("*-"*(num*2 - 2), end='*')
        print()