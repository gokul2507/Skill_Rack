import java.util.*;
public class Hello {

    public static void main(String[] args) {
		//Your Code Here
		int n = new Scanner(System.in).nextInt();
		String arr[] = new String[n-1];
		arr[0] = "-".repeat(2*n-2)+"*";
		for(int i = 0;i<n-2;i++)arr[i+1] ="-".repeat(2*n-4-(2*i))+"*"+"-".repeat(2*i+1)+"*"+"-".repeat(2*i+1)+"*";
		for(int i = 0;i<n-1;i++)System.out.println(arr[i]);
		System.out.println("*-".repeat(2*n-2)+"*");
		for(int i = n-2;i>-1;i--)System.out.println(arr[i]);
	}
}