#include <bits/stdc++.h>

using namespace std;
int n;
string get(int x)
{
     string s = "";
     while (x--)
          s += "-";
     return s;
}
void print(int x, int y, int i)
{
     int l = n + n - 1;
     while (x != y)
     {
          int t = x * 2;
          string s = get(l + t);
          s[l - 1] = s[l - 1 - t] = s[l - 1 + t] = '*';
          cout << s << endl;
          x += i;
     }
}
int main(int argc, char **argv)
{
     cin >> n;
     print(0, n - 1, 1);
     string s = get(n * 4 - 3);
     for (int i = 0; i < n * 4 - 3; i += 2)
          s[i] = '*';
     cout << s << endl;
     print(n - 2, -1, -1);
}