#include <stdio.h>
#include <stdlib.h>

int main()
{
     int N, t = 0;
     scanf("%d", &N);
     for (int i = 1; i < N; i++)
     {
          for (int j = 1; j <= (2 * N) - 1 + t; j++)
          {
               if (j == (2 * N) - 1 || j == (2 * N) - 1 + t || j == (2 * N) - 1 - t)
                    printf("*");
               else
                    printf("-");
          }
          t += 2;
          printf("\n");
     }
     for (int i = 1; i <= (N - 1) * 2; i++)
     {
          printf("*-");
     }
     printf("*\n");
     t = t - 2;
     for (int i = 1; i < N; i++)
     {
          for (int j = 1; j <= (2 * N) - 1 + t; j++)
          {
               if (j == (2 * N) - 1 || j == (2 * N) - 1 + t || j == (2 * N) - 1 - t)
                    printf("*");
               else
                    printf("-");
          }
          t -= 2;
          printf("\n");
     }
}