import java.util.*;

public class Hello {
     static int getCount(int arr[], int n, int k) {
          int t = 0, c = 0;
          for (int i = n - 1; i >= 0; i--) {
               int x = k - arr[i];
               if (x < 1)
                    c++;
               else if (x >= 0 && t < arr[i]) {
                    c++;
                    t += x;
               } else
                    break;
          }
          return c;
     }

     public static void main(String[] args) {
          // Your Code Here
          Scanner sc = new Scanner(System.in);
          int n = sc.nextInt(), arr[] = new int[n];
          for (int i = 0; i < n; i++)
               arr[i] = sc.nextInt();
          int k = sc.nextInt();
          Arrays.sort(arr);
          System.out.println(getCount(arr, n, k));

     }
}