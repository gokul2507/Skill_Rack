#include <stdio.h>
#include <stdlib.h>

int main()
{
     int n;
     scanf("%d", &n);
     int a[n], s[1000000] = {0}, x, sp = 0, f = 0;
     for (int i = 0; i < n; i++)
     {
          scanf("%d", &a[i]);
          s[a[i]]++;
     }
     scanf("%d", &x);
     for (int i = x - 1; i >= 0; i--)
     {
          while (s[i] != 0)
          {
               if (sp < i)
               {
                    sp += x - i;
                    f++;
                    s[i]--;
               }
               else
               {
                    break;
               }
          }
     }
     printf("%d", f);
}