#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n, x, p = 0, ans = 0;
     cin >> n;
     vector<int> z(n);
     for (int i = 0; i < n; i++)
          cin >> z[i];
     cin >> x;
     sort(z.begin(), z.end());
     while (z.size())
     {
          if (z.back() <= p)
          {
               z.pop_back();
               continue;
          }
          z[z.size() - 1]++;
          if (z[z.size() - 1] >= x)
          {
               z.pop_back();
               ans++;
          }
          p++;
     }
     cout << ans;
}