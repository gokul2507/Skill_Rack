#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n;
     cin >> n;
     int p = n;
     vector<int> z(n);
     for (int i = 0; i < n; i++)
          cin >> z[i];
     while (p)
     {
          for (int i = 0; i < n; i++)
          {
               if (z[i] % 2 == 0)
                    z[i] /= 2;
               else
               {
                    if (z[i] != -1)
                    {
                         z[i] = -1;
                         p -= 1;
                    }
                    continue;
               }
               printf("%d ", z[i]);
          }
          if (p)
               printf("\n");
     }
}