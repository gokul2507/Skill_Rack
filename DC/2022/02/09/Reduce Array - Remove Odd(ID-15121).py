n, li = int(input()), list(map(int, input().split()))
while len(li):
    for i in range(len(li)):
        if not li[i] & 1:
            li[i] //= 2
            print(li[i], end=' ')
        else:
            li[i] = '_'
    while '_' in li:
        li.remove('_')
    if 0 != len(li):
        print()
