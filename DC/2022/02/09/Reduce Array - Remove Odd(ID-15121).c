#include <stdio.h>
#include <stdlib.h>

int main()
{
     int a;
     scanf("%d\n", &a);
     int ar[1000];
     for (int i = 0; i < a; i++)
     {
          scanf("%d", &ar[i]);
     }
     int k = a;
     while (k > 0)
     {
          int br[1000];
          int m = 0;
          for (int i = 0; i < k; i++)
          {
               if (ar[i] % 2 != 0)
               {
                    continue;
               }
               br[m++] = ar[i] / 2;
          }
          for (int i = 0; i < m; i++)
          {
               ar[i] = br[i];
               printf("%d ", br[i]);
          }
          if (m == 0)
          {
               break;
          }
          k = m;
          printf("\n");
     }
}