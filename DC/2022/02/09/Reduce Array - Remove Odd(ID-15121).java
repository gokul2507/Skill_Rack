import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner input = new Scanner(System.in);
          int N = Integer.parseInt(input.nextLine());
          int arr[] = new int[N];
          for (int i = 0; i < N; i++)
               arr[i] = input.nextInt();
          int count = 0;
          while (true) {
               for (int i = 0; i < N; i++) {
                    if (arr[i] % 2 != 1 && arr[i] != 0) {
                         arr[i] = arr[i] / 2;
                         System.out.print(arr[i] + " ");
                         count++;
                    } else
                         arr[i] = 0;
               }
               if (count == 0)
                    break;
               else
                    count = 0;
               System.out.println("");
          }
     }
}