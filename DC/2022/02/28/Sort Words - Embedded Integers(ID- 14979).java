import java.util.*;

public class Hello {
     public static void main(String[] args) {
          Scanner q = new Scanner(System.in);
          Map<Integer, ArrayList<String>> d = new HashMap<Integer, ArrayList<String>>();
          ArrayList<Integer> l = new ArrayList<Integer>();
          while (q.hasNext()) {
               String s = q.next();
               int t = Integer.parseInt(s.replaceAll("[a-zA-Z]", ""));
               ArrayList<String> z = new ArrayList<String>();
               if (d.containsKey(t))
                    z = d.get(t);
               else
                    l.add(t);
               z.add(s);
               d.put(t, z);
          }
          Collections.sort(l);
          for (int i : l) {
               for (String j : d.get(i))
                    System.out.print(j + " ");
          }
     }
}