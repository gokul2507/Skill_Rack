s = list(map(str, input().split()))
l = []
for i in s:
    num = 0
    for j in i:
        if j.isdigit():
            num = num*10+int(j)
    l.append(num)
l.sort()
final = []
for i in l:
    for j in s:
        n = 0
        for k in j:
            if k.isdigit():
                n = n*10+int(k)
        if n == i and j not in final:
            print(j, end=' ')
            final.append(j)
