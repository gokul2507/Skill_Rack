#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     vector<pair<int, string>> ans;
     string s;
     while (cin >> s)
     {
          int val = 0;
          for (char c : s)
          {
               if (isdigit(c))
               {
                    val = val * 10 + c - '0';
               }
          }
          ans.push_back(make_pair(val, s));
     }
     stable_sort(ans.begin(), ans.end(), [](auto i, auto j)
                 { return i.first < j.first; });
     for (auto &[i, j] : ans)
          cout << j << " ";
}