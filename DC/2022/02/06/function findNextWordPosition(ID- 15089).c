#include <stdio.h>
#include <stdlib.h>

#include <string.h>
void findNextWordPosition(char *str)
{
     char *a[100], *s;
     int p = 0;
     int z[100];
     s = strtok(str, " ");
     while (s)
     {
          z[p] = strlen(s);
          a[p++] = s;
          s = strtok(NULL, " ");
     }
     for (int i = 0; i < p; i++)
     {
          int t = i;
          while (1)
          {
               t = (t + 1) % p;
               if (a[i][z[i] - 1] == a[t][z[t] - 1])
                    break;
          }
          printf("%d ", t + 1);
     }
}

int main()
{
     char str[101];
     scanf("%[^\n\r]", str);
     findNextWordPosition(str);
     return 0;
}