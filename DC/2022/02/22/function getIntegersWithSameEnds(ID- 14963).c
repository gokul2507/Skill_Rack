#include <stdio.h>
#include <stdlib.h>

typedef struct BoundedArray
{
     int SIZE;
     int *arr;
} boundedArray;

boundedArray *getIntegersWithSameEnds(char *str, int K)
{
     boundedArray *ans = malloc(sizeof(int[1001]));
     ans->arr = malloc(sizeof(int[1000]));
     ans->SIZE = 0;
     for (int i = 0; i >= 0 && i + K - 1 <= strlen(str); i++)
     {
          if (str[i] != '0' && str[i] == str[i + K - 1])
          {
               int temp = 0;
               for (int j = 0; j < K; j++)
                    temp = (temp * 10) + str[j + i] - '0';
               ans->arr[ans->SIZE++] = temp;
          }
     }
     for (int i = 0; i < ans->SIZE; i++)
          for (int j = i + 1; j < ans->SIZE; j++)
          {
               if (ans->arr[i] > ans->arr[j])
               {
                    ans->arr[i] += ans->arr[j];
                    ans->arr[j] = ans->arr[i] - ans->arr[j];
                    ans->arr[i] = ans->arr[i] - ans->arr[j];
               }
          }
     if (ans->SIZE == 0)
          ans->arr[ans->SIZE++] = -1;
     return ans;
}
int main()
{
     char str[101];
     int K;
     scanf("%s\n%d", str, &K);
     boundedArray *bArr = getIntegersWithSameEnds(str, K);
     if (bArr == NULL)
     {
          printf("Array is not formed\n");
     }
     if (bArr->SIZE <= 0)
     {
          printf("Invalid size for the array\n");
     }
     for (int index = 0; index < bArr->SIZE; index++)
     {
          printf("%d ", bArr->arr[index]);
     }
     return 0;
}
