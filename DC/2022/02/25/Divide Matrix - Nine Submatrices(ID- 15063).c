#include <stdio.h>
#include <stdlib.h>
const int r, c;
void getsum(int arr[r][c], int x1, int x2, int y1, int y2)
{
     int sum = 0;
     for (int i = x1; i < x2; i++)
          for (int j = y1; j < y2; j++)
          {
               sum += arr[i][j];
          }
     printf("%d ", sum);
}
int main()
{
     int x, y, p, q;
     scanf("%d %d", &r, &c);
     int arr[r][c];
     for (int i = 0; i < r; i++)
          for (int j = 0; j < c; j++)
               scanf("%d", &arr[i][j]);
     scanf("%d %d %d %d", &x, &y, &p, &q);
     getsum(arr, 0, x, 0, p);
     getsum(arr, 0, x, p, q);
     getsum(arr, 0, x, q, c);
     getsum(arr, x, y, 0, p);
     getsum(arr, x, y, p, q);
     getsum(arr, x, y, q, c);
     getsum(arr, y, r, 0, p);
     getsum(arr, y, r, p, q);
     getsum(arr, y, r, q, c);
}