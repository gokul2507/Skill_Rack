r, c = map(int, input().split())
z = [list(map(int, input().split())) for _ in range(r)]
x, y, p, q = map(int, input().split())
m1, m2, m3 = z[:x], z[x:y], z[y:]


def fun2(l):
    t = []
    for i in l:
        t += i
    return sum(t)


def fun(m):
    m = list(map(list, zip(*m)))
    mm1, mm2, mm3 = m[:p], m[p:q], m[q:]
    print(fun2(mm1), fun2(mm2), fun2(mm3), end=' ')


fun(m1)
fun(m2)
fun(m3)
