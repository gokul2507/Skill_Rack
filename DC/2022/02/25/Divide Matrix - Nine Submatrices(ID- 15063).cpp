#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int R, C;
     cin >> R >> C;
     vector<vector<int>> mat(R, vector<int>(C));
     for (auto &i : mat)
     {
          for (auto &j : i)
               cin >> j;
     }
     vector<int> row(4), col(4);
     cin >> row[1] >> row[2] >> col[1] >> col[2];
     row[0] = 0, row[3] = R;
     col[0] = 0, col[3] = C;
     for (int i = 0; i < 3; i++)
     {
          for (int j = 0; j < 3; j++)
          {
               int sum = 0;
               for (int k = row[i]; k < row[i + 1]; k++)
               {
                    for (int l = col[j]; l < col[j + 1]; l++)
                    {
                         sum += mat[k][l];
                    }
               }
               cout << sum << " ";
          }
     }
}