import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner s = new Scanner(System.in);
          int r = s.nextInt();
          int c = s.nextInt();
          int[][] mat = new int[r][c];
          for (int i = 0; i < r; i++) {
               for (int j = 0; j < c; j++)
                    mat[i][j] = s.nextInt();
          }
          int[] row = new int[4];
          int[] col = new int[4];
          row[0] = 0;
          row[3] = r;
          col[0] = 0;
          col[3] = c;

          row[1] = s.nextInt();
          row[2] = s.nextInt();
          col[1] = s.nextInt();
          col[2] = s.nextInt();

          int sum = 0;
          for (int i = 0; i < 3; i++) {
               for (int j = 0; j < 3; j++) {
                    sum = 0;
                    for (int a = row[i]; a < row[i + 1]; a++) {
                         for (int b = col[j]; b < col[j + 1]; b++)
                              sum += mat[a][b];
                    }
                    System.out.print(sum + " ");
               }
          }

     }
}