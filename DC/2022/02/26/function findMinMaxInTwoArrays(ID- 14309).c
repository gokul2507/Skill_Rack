#include <stdio.h>
#include <stdlib.h>

int findMinMaxInTwoArrays(int SIZE1, int arr1[], int SIZE2, int arr2[], int K)
{
     int x = 0, y = 0;
     for (int i = 0; i < SIZE1; i++)
          x += arr1[i] > K;
     for (int i = 0; i < SIZE2; i++)
          y += arr2[i] < K;
     if (x < y)
          x = y;
     return x;
}
int main()
{
     int M, N, K;
     scanf("%d %d", &M, &N);
     int arr1[M], arr2[N];
     for (int index = 0; index < M; index++)
     {
          scanf("%d ", &arr1[index]);
     }
     for (int index = 0; index < N; index++)
     {
          scanf("%d ", &arr2[index]);
     }
     scanf("%d", &K);
     printf("%d", findMinMaxInTwoArrays(M, arr1, N, arr2, K));
     return 0;
}
