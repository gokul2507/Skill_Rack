import java.util.*;

public class Hello {

     public static void main(String[] args) {
          // Your Code Here
          Scanner sc = new Scanner(System.in);
          String str = sc.nextLine();
          int a = 0, b = 0, c = 0, d = 0;
          int sum = 0;
          for (int i = 0; i < str.length(); i++) {
               if (str.charAt(i) == 'A')
                    a++;
               else if (str.charAt(i) == 'B')
                    b++;
               else if (str.charAt(i) == 'C')
                    c++;
               else
                    d++;
          }
          sum = (a / 4) * 100 + (a % 4 * 40) + b * 60 + (c / 6) * 200 + (c % 6) * 55 + d * 95;
          System.out.println(sum);

     }
}