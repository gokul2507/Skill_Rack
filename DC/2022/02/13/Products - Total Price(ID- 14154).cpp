#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     string s;
     cin >> s;
     int a = 0, b = 0, c = 0, d = 0;
     for (int i = 0; i < s.size(); i++)
          if (s[i] == 'A')
               a++;
          else if (s[i] == 'B')
               b++;
          else if (s[i] == 'C')
               c++;
          else
               d++;
     cout << (100 * (a / 4) + 40 * (a % 4)) + (60 * b) + (200 * (c / 6) + 55 * (c % 6)) + (95 * d);
}