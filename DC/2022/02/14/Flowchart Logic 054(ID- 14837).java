import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner sc = new Scanner(System.in);
          int m, n;
          m = sc.nextInt();
          n = sc.nextInt();
          while (m < n) {
               System.out.print(m + " ");
               if (n % 10 != 0) {
                    if (n % 2 != 0) {
                         m += n % 10;
                    } else {
                         m += 1;
                    }
               } else {
                    m += 1;
               }
               n -= 1;
          }

     }
}