a, b = map(int, input().split())
while a < b:
    print(a, end=" ")
    if b % 10 != 0:
        if b % 2 != 0:
            a += (b % 10)
        else:
            a += 1
    else:
        a = a+1
    b -= 1
