#include <stdio.h>
#include <stdlib.h>

int main()
{
     int M, N;
     scanf("%d%d", &M, &N);
     while (M < N)
     {
          printf("%d ", M);
          if ((N % 10) != 0)
          {
               if (N % 2 != 0)
               {
                    M += (N % 10);
               }
               else
               {
                    M++;
               }
          }
          else
          {
               M++;
          }
          N--;
     }
     return 0;
}