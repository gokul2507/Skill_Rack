#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int m, n;
     cin >> m >> n;
     while (m < n)
     {
          cout << m << " ";
          if (m < n && n & 1)
               m += n % 10;
          else
               m += 1;
          n -= 1;
     }
}