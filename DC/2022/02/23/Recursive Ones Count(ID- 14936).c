#include <stdio.h>
#include <stdlib.h>

int dectobin(int n)
{
     int count = 0;
     while (n > 0)
     {
          if (n % 2 == 1)
               count++;
          n /= 2;
     }
     printf("%d ", count);
     if (count == 1)
     {
          // printf("1");
          return 0;
     }
     else
     {
          // printf("%d ",count);
          return dectobin(count);
     }
}
int main()
{
     char str[1000000];
     scanf("%s", str);
     int count = 0;
     for (int i = 0; str[i] != '\0'; i++)
     {
          if (str[i] == '1')
               count++;
     }
     printf("%d ", count);
     if (count != 1)
          dectobin(count);
}