import java.util.*;

public class Hello {
     public static void recur(String str) {
          // while(str.charAt(0)=='0')
          // str=str.substring(1);
          if (str.equals("1")) {
               return;
          }
          int num = 0;
          for (int i = 0; i < str.length(); i++) {
               if (str.charAt(i) == '1')
                    num++;
          }
          System.out.print(num + " ");
          recur(Integer.toBinaryString(num));
     }

     public static void main(String[] args) {
          Scanner s = new Scanner(System.in);
          String str = s.next();
          if (str.equals("1")) {
               System.out.println("1");
               return;
          }
          recur(str);
     }
}