#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     string s;
     cin >> s;
     int one = 0;
     for (char c : s)
          one += c - '0';
     cout << one << " ";
     while (one != 1)
     {
          int ctr = 0;
          while (one)
               ctr += one & 1, one >>= 1;
          cout << ctr << " ";
          one = ctr;
     }
}