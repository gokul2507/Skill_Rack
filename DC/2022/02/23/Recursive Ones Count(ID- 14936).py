n = input().strip()
c = 0
for i in n:
    if i == '1':
        c += 1
a = c
c = 0
while a != 1:
    b = bin(int(a)).replace("0b", "")
    print(a, end=" ")
    for i in str(b):
        if i == '1':
            c += 1
    a = c
    c = 0
print(1)
