
#include <stdio.h>
#include <stdlib.h>
int insertBits(int X, int Y)
{
     int ans = 0, p = 1;
     while (X)
     {
          if (X % 2)
               break;
          p *= 2;
          X /= 2;
     }
     while (Y)
     {
          int t = Y % 2;
          ans += t * p;
          p *= 2;
          Y /= 2;
     }
     while (X)
     {
          int t = X % 2;
          ans += t * p;
          p *= 2;
          X /= 2;
     }
     return ans;
}
int main()
{
     int X, Y;
     scanf("%d %d", &X, &Y);
     printf("%d", insertBits(X, Y));
     return 0;
}