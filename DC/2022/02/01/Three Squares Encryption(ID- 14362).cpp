#include <bits/stdc++.h>
using namespace std;

int searchAns(char a[][3][3], char c)
{
     int i, j, k, found = 0;
     for (i = 0; i < 3; i++)
     {
          for (j = 0; j < 3; j++)
          {
               for (k = 0; k < 3; k++)
               {
                    if (a[i][j][k] == c)
                    {
                         found = 1;
                         break;
                    }
               }
               if (found)
                    break;
          }
          if (found)
               break;
     }

     return (++i) * 100 + (++j) * 10 + (++k);
}

int main(int argc, char **argv)
{
     int l = 0;
     string msg, str;
     cin >> msg >> str;
     char a[3][3][3];
     for (int i = 0; i < 3; i++)
          for (int j = 0; j < 3; j++)
               for (int k = 0; k < 3; k++)
                    a[i][j][k] = str[l++];

     for (int i = 0; i < msg.length(); i++)
          cout << searchAns(a, msg[i]);
}