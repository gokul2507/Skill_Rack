r, c = map(int, input().split())
l = []
k, k1 = 0, 99999
r1, c1, r2, c2 = 0, 0, 0, 0
for i in range(r):
    l.append(list(map(int, input().split())))
    for j in range(c):
        if l[i][j] > k:
            k = l[i][j]
            r1, c1 = i, j
        if l[i][j] < k1:
            k1 = l[i][j]
            r2, c2 = i, j
startR, endR = min(r1, r2), max(r1, r2)
startC, endC = min(c1, c2), max(c1, c2)
for i in range(startR, endR+1):
    for j in range(startC, endC+1):
        print(l[i][j], end=" ")
    print(" ")
