import java.util.*;

public class Hello {

     public static void main(String[] args) {
          // Your Code Here
          Scanner sc = new Scanner(System.in);
          int[] min = { -1, -1 };
          int[] max = { -1, -1 };
          int r = sc.nextInt(), c = sc.nextInt();
          int[][] arr = new int[r][c];
          for (int i = 0; i < r; i++) {
               for (int j = 0; j < c; j++) {
                    arr[i][j] = sc.nextInt();
                    if (min[0] == -1 || arr[min[0]][min[1]] > arr[i][j]) {
                         min[0] = i;
                         min[1] = j;
                    }
                    if (max[0] == -1 || arr[max[0]][max[1]] < arr[i][j]) {
                         max[0] = i;
                         max[1] = j;
                    }
               }
          }
          int minRow = Math.min(min[0], max[0]), maxRow = Math.max(min[0], max[0]), minCol = Math.min(min[1], max[1]),
                    maxCol = Math.max(min[1], max[1]);
          for (int i = minRow; i <= maxRow; i++) {
               for (int j = minCol; j <= maxCol; j++) {
                    System.out.print(arr[i][j] + " ");
               }
               System.out.println();
          }
     }
}