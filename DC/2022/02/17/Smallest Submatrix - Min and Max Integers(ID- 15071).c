#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
int main()
{
     int m, n, i, j, s = INT_MAX, l = INT_MIN, sposi, sposj, lposi, lposj;
     scanf("%d %d\n", &m, &n);
     int mat[m][n];
     for (i = 0; i < m; i++)
     {
          for (j = 0; j < n; j++)
          {
               scanf("%d ", &mat[i][j]);
               if (mat[i][j] > l)
               {
                    l = mat[i][j];
                    lposj = j;
                    lposi = i;
               }
               if (mat[i][j] < s)
               {
                    s = mat[i][j];
                    sposi = i;
                    sposj = j;
               }
          }
     }
     // printf("%d %d %d %d\n",sposi,sposj,lposi,lposj);
     int a, b, c, d;
     a = fmin(sposi, lposi);
     b = fmax(sposi, lposi);
     c = fmin(sposj, lposj);
     d = fmax(sposj, lposj);
     for (i = a; i <= b; i++)
     {
          for (j = c; j <= d; j++)
          {
               printf("%d ", mat[i][j]);
          }
          printf("\n");
     }
     return 0;
}