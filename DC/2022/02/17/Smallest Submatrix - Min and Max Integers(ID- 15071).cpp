#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n, m, mni = 0, mnj = 0, mxi = 0, mxj = 0;
     cin >> n >> m;
     vector<vector<int>> z(n, vector<int>(m));
     for (int i = 0; i < n; i++)
          for (int j = 0; j < m; j++)
          {
               cin >> z[i][j];
               if (z[mni][mnj] > z[i][j])
               {
                    mni = i;
                    mnj = j;
               }
               if (z[mxi][mxj] < z[i][j])
               {
                    mxi = i;
                    mxj = j;
               }
          }
     if (mni > mxi)
          swap(mni, mxi);
     if (mnj > mxj)
          swap(mnj, mxj);
     while (mni <= mxi)
     {
          int j = mnj;
          while (j <= mxj)
               cout << z[mni][j++] << " ";
          mni++;
          cout << endl;
     }
}