#include <stdio.h>
#include <stdlib.h>

char *getStringFromRows(int R, int C, char *matrix, int X, int Y)
{

     char *o = malloc(10001);
     int ind = 0;
     if (Y < X)
     {
          Y += R;
     }
     for (int i = X - 1; i < Y; i++)
     {
          for (int j = 0; j < C; j++)
          {
               o[ind++] = matrix[(i % R) * C + j];
          }
     }
     return o;
} // End of getStringFromRows function

int main()
{
     int R, C, X, Y;
     scanf("%d %d", &R, &C);
     char matrix[R][C];
     for (int row = 0; row < R; row++)
     {
          for (int col = 0; col < C; col++)
          {
               scanf(" %c", &matrix[row][col]);
          }
     }
     scanf("%d %d", &X, &Y);
     char *result = getStringFromRows(R, C, matrix, X, Y);
     if (result == NULL)
     {
          printf("New string is not formed\n");
     }
     if (result[0] == '\0' || result[0] == ' ')
     {
          printf("String is empty\n");
     }
     printf("%s", result);
     return 0;
}