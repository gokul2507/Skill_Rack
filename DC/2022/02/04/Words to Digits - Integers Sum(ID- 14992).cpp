#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int N, ans = 0;
     cin >> N;
     vector<string> mp = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
     while (N--)
     {
          string val, num = "", temp = "";
          cin >> val;
          for (char c : val)
          {
               if (isdigit(c))
               {
                    num += c;
                    temp = "";
               }
               else
               {
                    temp += c;
                    for (int i = 0; i < 10; i++)
                    {
                         if (mp[i] == temp)
                         {
                              num += i + '0';
                              temp = "";
                              break;
                         }
                    }
               }
          }
          ans += stoi(num);
     }
     cout << ans;
}