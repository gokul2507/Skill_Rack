import java.util.*;

public class Hello {
     public static void main(String[] args) {
          Scanner sc = new Scanner(System.in);
          int n = sc.nextInt(), s = 0;
          for (int i = 0; i < n; i++) {
               String str = sc.next();
               s += Integer.parseInt((str.replaceAll("zero", "0").replaceAll("one", "1").replaceAll("two", "2")
                         .replaceAll("three", "3").replaceAll("four", "4").replaceAll("five", "5")
                         .replaceAll("six", "6").replaceAll("seven", "7").replaceAll("eight", "8")
                         .replaceAll("nine", "9")));
          }
          System.out.print(s);
     }
}