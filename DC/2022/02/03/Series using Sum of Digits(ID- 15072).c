#include <stdio.h>
#include <stdlib.h>

int main()
{
     int n, k;
     scanf("%d %d", &n, &k);
     int sum = 0, rem, temp = 0;
     for (int i = 0; i < k; i++)
     {
          printf("%d ", n);
          temp = n;
          while (temp > 0)
          {
               rem = temp % 10;
               sum += rem;
               temp /= 10;
          }
          n += sum;
          sum = 0;
          temp = 0;
     }
}