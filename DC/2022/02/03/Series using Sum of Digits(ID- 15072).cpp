#include <bits/stdc++.h>

using namespace std;
int sum_digit(int x)
{
     int t = 0;
     while (x)
     {
          t += x % 10;
          x /= 10;
     }
     return t;
}
int main(int argc, char **argv)
{
     int n, x;
     cin >> n >> x;
     while (x--)
     {
          cout << n << " ";
          n += sum_digit(n);
     }
}