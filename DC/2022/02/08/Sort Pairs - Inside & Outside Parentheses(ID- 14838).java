import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner sc = new Scanner(System.in);
          int n = sc.nextInt();
          sc.nextLine();
          String[] arr = sc.nextLine().split(" ");
          PriorityQueue<Integer> p1 = new PriorityQueue<>(Collections.reverseOrder());
          PriorityQueue<Integer> p2 = new PriorityQueue<>();

          for (String s : arr) {
               int l = s.length(), i = 0;
               int num1 = 0, num2 = 0;
               while (i < l) {
                    if (s.charAt(i) == '(') {
                         i++;
                         while (s.charAt(i) != ')') {
                              num2 *= 10;
                              num2 += (s.charAt(i) - '0');
                              i++;
                         }
                         i++;
                    } else {
                         num1 *= 10;
                         num1 += (s.charAt(i) - '0');
                         i++;
                    }
               }
               p1.add(num1);
               p2.add(num2);
          }

          for (String s : arr) {
               if (s.charAt(0) == '(') {
                    System.out.print("(" + p2.poll() + ")" + p1.poll() + " ");
               } else {
                    System.out.print(p1.poll() + "(" + p2.poll() + ") ");
               }
          }

     }
}