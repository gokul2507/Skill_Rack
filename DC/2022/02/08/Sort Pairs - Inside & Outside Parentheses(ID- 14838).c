#include <stdio.h>
#include <stdlib.h>

int main()
{
     int n;
     scanf("%d\n", &n);
     char str[n][11];
     int in_arr[n], out_arr[n];
     for (int i = 0; i < n; i++)
     {
          scanf("%s ", str[i]);
          if (str[i][0] == '(')
               sscanf(str[i], "(%d)%d", &in_arr[i], &out_arr[i]);
          else
               sscanf(str[i], "%d(%d)", &out_arr[i], &in_arr[i]);
     }
     for (int i = 0; i < n; i++)
          for (int j = i + 1; j < n; j++)
          {
               if (in_arr[i] > in_arr[j])
               {
                    int t = in_arr[i];
                    in_arr[i] = in_arr[j];
                    in_arr[j] = t;
               }
               if (out_arr[i] < out_arr[j])
               {
                    int t = out_arr[i];
                    out_arr[i] = out_arr[j];
                    out_arr[j] = t;
               }
          }
     for (int i = 0; i < n; i++)
     {
          if (str[i][0] == '(')
               printf("(%d)%d ", in_arr[i], out_arr[i]);
          else
               printf("%d(%d) ", out_arr[i], in_arr[i]);
     }
}