#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n;
     cin >> n;
     vector<int> z, a, b;
     for (int i = 0; i < n; i++)
     {
          string t;
          cin >> t;
          int flag = 1, x, y;
          if (t[0] == '(')
          {
               t = t.substr(1);
               flag = 0;
          }
          char q;
          stringstream out(t);
          out >> x >> q >> y;
          if (flag == 0)
               swap(x, y);
          z.push_back(flag);
          a.push_back(x);
          b.push_back(y);
     }
     sort(a.begin(), a.end());
     sort(b.begin(), b.end());
     reverse(a.begin(), a.end());
     for (int i = 0; i < n; i++)
     {
          if (z[i] == 0)
               swap(a[i], b[i]);
          if (z[i] == 0)
               cout << "(";
          cout << a[i];
          if (z[i] == 0)
               cout << ")";
          if (z[i] == 1)
               cout << "(";
          cout << b[i];
          if (z[i] == 1)
               cout << ")";
          cout << " ";
     }
}