#include <bits/stdc++.h>
using namespace std;
int solve(int arr[], int n, int t)
{
     int val = INT_MAX;
     for (int i = 0; i < n; i++)
     {
          int temp = arr[i];
          while (temp != 0)
          {
               if (temp % 10 == t)
               {
                    val = min(val, arr[i]);
               }
               temp = temp / 10;
          }
     }
     if (val == INT_MAX)
          return 0;
     return val;
}
int main(int argc, char **argv)
{
     int n;
     cin >> n;
     int arr[n];
     for (int i = 0; i < n; i++)
          cin >> arr[i];
     int num;
     cin >> num;
     int ans = 0;
     while (num != 0)
     {
          int temp = num % 10;
          ans = ans + solve(arr, n, temp);
          num = num / 10;
     }
     cout << ans;
}