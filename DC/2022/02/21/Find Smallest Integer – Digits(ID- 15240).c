#include <stdio.h>
#include <stdlib.h>

int main()
{
     int n;
     scanf("%d ", &n);
     int arr[n], k, sum = 0;
     for (int i = 0; i < n; i++)
     {
          scanf("%d ", &arr[i]);
     }
     scanf("%d ", &k);
     while (k != 0)
     {
          int minnum = 9999999;
          for (int j = 0; j < n; j++)
          {
               int t = arr[j];
               while (t != 0)
               {
                    if (t % 10 == k % 10)
                    {
                         if (arr[j] < minnum)
                         {
                              minnum = arr[j];
                         }
                    }
                    t /= 10;
               }
          }
          k /= 10;
          if (minnum != 9999999)
               sum += minnum;
     }
     printf("%d ", sum);
}