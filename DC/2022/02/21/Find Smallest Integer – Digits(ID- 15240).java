import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner q = new Scanner(System.in);
          int n = q.nextInt(), ans = 0;
          q.nextLine();
          List<String> z = Arrays.asList(q.nextLine().split(" ", n));
          String l = q.nextLine();
          for (int i = 0; i < l.length(); i++) {
               int m = 0;
               for (String j : z) {
                    if (j.contains(l.charAt(i) + "")) {
                         if (m == 0)
                              m = Integer.parseInt(j);
                         m = Math.min(m, Integer.parseInt(j));
                    }
               }
               ans += m;
          }
          System.out.println(ans);
     }
}