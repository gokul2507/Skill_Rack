n = input()
l = list(map(str, input().split()))
a = input().strip()
b = 0
for i in a:
    m = []
    for j in l:
        if i in j:
            m.append(int(j))
    if len(m) >= 1:
        b += min(m)
print(b)
