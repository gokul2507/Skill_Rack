#include <stdio.h>
#include <stdlib.h>

int main()
{
     char alpha[26], str[1001];
     scanf("%s\n%s", alpha, str);
     int sum = 0, i1, i2;
     if (strlen(str) < 2)
     {
          printf("0");
     }
     else
     {
          for (int i = 0; str[i] != '\0'; i++)
          {
               for (int j = 0; alpha[j] != '\0'; j++)
               {
                    if (str[i] == alpha[j])
                    {
                         i1 = j;
                    }
                    if (str[i + 1] == alpha[j])
                    {
                         i2 = j;
                    }
               }
               if (i1 >= i2)
                    sum += (i1 - i2);
               else
                    sum += (i2 - i1);
          }
          printf("%d", sum);
     }
}