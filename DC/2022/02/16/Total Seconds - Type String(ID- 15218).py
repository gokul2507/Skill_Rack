alph = input().strip()
s = input().strip()
ans = 0
curr_pos = alph.index(s[0])
for i in s[1:]:
    if alph.index(i) > curr_pos:
        ans += alph.index(i) - curr_pos
    else:
        ans += curr_pos - alph.index(i)
    curr_pos = alph.index(i)
print(ans)
