#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     map<char, int> d;
     string k, s;
     cin >> k >> s;
     for (int i = 0; i < k.size(); i++)
     {
          d[k[i]] = i;
     }
     int ans = 0, pr = d[s[0]];
     for (int i = 1; i < s.size(); i++)
     {
          ans += abs(pr - d[s[i]]);
          pr = d[s[i]];
     }
     cout << ans;
}