import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner sc = new Scanner(System.in);
          String s = sc.next();
          String find = sc.next();
          int sum = 0, initial = s.indexOf(find.charAt(0));
          for (int i = 0; i < find.length(); i++) {
               int next = s.indexOf(find.charAt(i));
               sum += Math.abs(initial - next);
               initial = next;
          }
          System.out.print(sum);
     }
}