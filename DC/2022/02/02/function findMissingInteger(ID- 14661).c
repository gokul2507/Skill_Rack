#include <stdio.h>
#include <stdlib.h>
void findMissingInteger(int R, int C, int matrix[R][C])
{
    int d[10000]={0};
    int mi=matrix[0][0],ma=0,x=0,y=0;
    for(int i=0;i<R;i++)
    for(int j=0;j<C;j++){
        if(matrix[i][j]==-1){
            x=i;
            y=j;
            continue;
        }
        d[matrix[i][j]]=1;
        if(mi==-1||mi>matrix[i][j])
        mi=matrix[i][j];
        if(ma<matrix[i][j])
        ma=matrix[i][j];
    }
    while(mi<ma){
        if(d[mi]==0){
            matrix[x][y]=mi;
            break;
        }
        mi++;
    }
}

int main()
{
    int R, C;
    scanf("%d %d", &R, &C);
    int matrix[R][C];
    for (int row = 0; row < R; row++)
    {
        for (int col = 0; col < C; col++)
        {
            scanf("%d", &matrix[row][col]);
        }
    }
    findMissingInteger(R, C, matrix);
    for (int row = 0; row < R; row++)
    {
        for (int col = 0; col < C; col++)
        {
            printf("%d ", matrix[row][col]);
        }
        printf("\n");
    }
    return 0;
}