#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int n, x, y;
     cin >> x >> y;
     while (1)
     {
          cin >> n;
          if (n & 1)
          {
               n += y;
               x--;
          }
          else
          {
               n += x;
               y--;
          }
          cout << n << " ";
          if (n <= 0)
               break;
     }
}