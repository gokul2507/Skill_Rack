import java.util.*;

public class Hello {

     public static void main(String[] args) {
          Scanner sc = new Scanner(System.in);
          String str = sc.nextLine();
          String All[] = str.split(" ");
          int tot = Integer.parseInt(All[0]);
          int len = All.length;
          for (int i = 1; i < len; i += 2) {
               if (i % 2 == 1) {
                    if (All[i].equals("plus")) {
                         tot += Integer.parseInt(All[i + 1]);
                    }
                    if (All[i].equals("minus")) {
                         tot -= Integer.parseInt(All[i + 1]);
                    }
                    if (All[i].equals("multiply")) {
                         tot *= Integer.parseInt(All[i + 1]);
                    }
                    if (All[i].equals("divide")) {
                         tot /= Integer.parseInt(All[i + 1]);
                    }
               }
          }
          System.out.println(tot);
     }
}