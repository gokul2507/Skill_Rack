#include <stdio.h>
#include <stdlib.h>
int main()
{
     int n1, n2;
     char str[10];
     scanf("%d", &n1);
     while (scanf(" %s %d", str, &n2) == 2)
     {
          if (str[0] == 'p')
               n1 += n2;
          else if (str[0] == 'd')
               n1 /= n2;
          else if (str[0] == 'm' && str[1] == 'i')
               n1 -= n2;
          else
               n1 *= n2;
     }
     printf("%d", n1);
}