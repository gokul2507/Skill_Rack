lis = input().split()
total = int(lis[0])
for i in range(1, len(lis), 2):
    j = int(lis[i+1])
    condition = lis[i]
    if condition == 'plus':
        total += j
    elif condition == 'minus':
        total -= j
    elif condition == 'multiply':
        total *= j
    elif condition == 'divide':
        total //= j
print(total)
