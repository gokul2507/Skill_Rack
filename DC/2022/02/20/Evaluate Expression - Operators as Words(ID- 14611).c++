#include <bits/stdc++.h>

using namespace std;

int main(int argc, char **argv)
{
     int ans = 0, x;
     string t;
     cin >> ans;
     while (cin >> t >> x)
          ans = (t[2] == 'u') ? ans + x : (t[2] == 'n') ? ans - x
                                      : (t[2] == 'l')   ? ans * x
                                                        : ans / x;
     cout << ans;
}