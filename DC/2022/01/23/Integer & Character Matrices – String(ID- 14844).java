import java.util.*;
public class Hello {

    public static void main(String[] args) {
		//Your Code Here
		Scanner sc = new Scanner(System.in);
		String s = sc.nextLine(), val="";
		int n = sc.nextInt(), ctr=0, len=0;
		int intLen = (s.length()-n*n)/(n*n);
		for(char c : s.toCharArray()){
		    if(Character.isDigit(c)){
		        val+=c;
		        len++;
		    }
		    if(len==intLen){
		        System.out.print(val+" ");
		        len=0;
		        ctr++;
		        val="";
		    }
		    if(ctr==n){
		        System.out.println();
		        ctr=0;
		    }
		}
		ctr=0;
		for(char c : s.toCharArray()){
		    if(Character.isAlphabetic(c)){
		        System.out.print(c+" ");
		        ctr++;
		    }
		    if(ctr==n){
		        System.out.println();
		        ctr=0;
		    }
		}
	}
}