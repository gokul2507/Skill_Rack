import java.util.*;
public class Hello {

    public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		String u=input.nextLine();
		char []s=u.toCharArray();
		int res[]=new int[101],w=0;
		for(int i=0;i<s.length;i++){
		    if(s[i]=='L'){
		        //System.out.print(s[i]+" "+i+"\n");
		        int es=forward(s,i);
		        int r=backward(s,i);
		        //System.out.print(es+" "+r+"\n");
		        if(es!=-1 && r!=-1){
    		        if(es>r){
    		            res[w++]=r;
    		        }else{
    		            res[w++]=es;
    		        }
		        }
		        else if(es==-1 && r!=-1){
		            res[w++]=r;
		        }
		        else{
		            res[w++]=es;
		        }
		    }
		}
		for(int i=0;i<w;i++){
		    System.out.print(res[i]+" ");
		}
	}
	static int forward(char []s,int index){
	    //int x=-1;
	    for(int i=index;i<s.length;i++){
	        if(s[i]=='#'){
	            break;
	        }else if(s[i]=='*'){
	            return i-index;
	        }
	    }
	    return -1;
	}
	static int backward(char []s,int index){
	    //int x=-1;
	    //System.out.println(index+" "+s.length);
	    for(int i=index;i>=0;i--){
	        if(s[i]=='#'){
	            break;
	        }else if(s[i]=='*'){
	            return index-i;
	        }
	    }
	    return -1;
	}
}