#include <bits/stdc++.h>
 
using namespace std;
vector<int> fun(string s){
    vector<int> z;
    int p=-1;
    for(int i=0;i<s.size();i++){
        if(s[i]=='#') p=-1;
        if(s[i]=='*') p=1;
        if(s[i]=='L') {
            z.push_back(p);
            if(p!=-1) p++;
        }
        if(s[i]=='-' && p!=-1) p++;
    }
    return z;
}
int main(int argc, char** argv)
{
    string s;
    cin>>s;
    vector<int> left=fun(s),right;
    reverse(s.begin(),s.end());
    right=fun(s);
    reverse(right.begin(),right.end());
    for(int i=0;i<right.size();i++){
        if(left[i]==-1){
            if(right[i]==-1) cout<<"-1 ";
            else cout<<right[i]<<" ";
        }
        else{
            if(right[i]==-1) cout<<left[i]<<" ";
            else cout<<min(right[i],left[i])<<" ";
        }
    }
}