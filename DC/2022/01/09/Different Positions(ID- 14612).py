input()
x,y=0,0
l=list(map(int,input().split()))
for i,j in zip(l,sorted(l)):
    if i==j:
        x+=i
    else:
        y+=j
print(abs(x-y))
