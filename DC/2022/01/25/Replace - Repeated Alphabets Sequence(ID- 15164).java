import java.util.*;
public class Hello {

    public static void main(String[] args) {
        
		Scanner sc = new Scanner(System.in);
		
		String s = sc.next();
		
		sc.close();
		
		String res = "";
		res+=s.charAt(0);
		int len = s.length();
		
		for(int i=1;i<len;i++){
		    
		    char ch = s.charAt(i);
		    
		    if(ch==s.charAt(i-1)){
		        res+=nextChar(res.charAt(i-1));
		    }else{
		        res+=ch;
		    }
		}
		
		System.out.println(res);

	}
	
	static char nextChar(char ch){
	    if(ch=='z') return 'a';
	    String alpha = "abcdefghijklmnopqrstuvwxyz";
	    for(int i=0;i<25;i++){
	        if(alpha.charAt(i)==ch) return alpha.charAt(i+1);
	    }
	    return 'z';
	}
	
}