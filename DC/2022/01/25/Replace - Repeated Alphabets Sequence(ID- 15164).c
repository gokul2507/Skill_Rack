#include <stdio.h>
#include <stdlib.h>

int main()
{
     char s[1001];
     scanf("%s", s);
     for (int i = 0, j; s[i]; i = j)
          for (j = i + 1; s[j] == s[i]; j++)
               s[j] = (s[j] - 'a' + j - i) % 26 + 'a';
     return printf("%s", s);
}