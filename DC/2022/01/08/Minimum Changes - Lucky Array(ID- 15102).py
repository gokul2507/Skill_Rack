n=int(input())
z=list(map(int,input().split()))
i,j=0,n//2
s1,s2,s3=0,0,0
while i<n//2:
    if z[i]>z[j]:
        s1+=1
        s2+=1
    elif z[i]==z[j]:
        s1+=1
        s3+=1
    elif z[i]<z[j]:
        s2+=1
        s3+=1
    i+=1
    j+=1
print(min(s1,s2,s3))