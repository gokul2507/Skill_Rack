import java.util.*;
public class Hello {
    
    static int[] findIndex(int pos, int R, int C) {
        int coor[] = {1, 0};
        if(pos==0) return coor;
        coor[0] -= 1;
        if(pos > C) pos -= C;
        else {
            coor[1] = pos-1;
            return coor;
        }
        if(pos > R) pos -= (R-1);
        else {
            coor[0] = pos;
            coor[1] = C-1;
            return coor;
        }
        if(pos > C-1) pos -= (C-1);
        else {
            coor[0] = R-1;
            coor[1] = C-pos-1;
            return coor;
        }
        coor[0] = R-pos-1;
        coor[1] = 0;
        return coor;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int R = sc.nextInt(), C = sc.nextInt(), i, j, len = 2*R+C*2-4, m[][] = new int[R][C];
        for(i=0; i<R; i++) for(j=0; j<C; m[i][j++] =sc.nextInt());
        int X = sc.nextInt()%len, Y = sc.nextInt()%len,
        xc[] = findIndex(X,R,C), yc[] = findIndex(Y,R,C);
        
        int t = m[xc[0]][xc[1]];
        m[xc[0]][xc[1]] = m[yc[0]][yc[1]];
        m[yc[0]][yc[1]] = t;
        
        for(i=0; i<R; System.out.println(), i++)
            for(j=0; j<C; System.out.print(m[i][j++]+" "));
	}
}