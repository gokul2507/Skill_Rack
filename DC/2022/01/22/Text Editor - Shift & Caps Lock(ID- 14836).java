import java.util.*;
public class Hello {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String arr[] = sc.nextLine().split(" ");
        // System.out.println(Arrays.toString(arr));
        boolean isCaps = false;
        for(String s:arr){
            if(s.contains("CAPSLOCK"))
                isCaps = !isCaps;
            else if(s.contains("SHIFT")){
                String temp[] = s.split("\\+");
                if(isCaps){
                    System.out.print(temp[1].toLowerCase());
                } else {
                    System.out.print(temp[1].toUpperCase());
                }
            }
            else {
                if(isCaps)
                    System.out.print(s.toUpperCase());
                else
                    System.out.print(s.toLowerCase());
            }
        }
	}
}