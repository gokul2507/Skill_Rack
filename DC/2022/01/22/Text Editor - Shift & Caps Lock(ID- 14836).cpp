#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    string s,word;
    int capslock=0;
    getline(cin,s);
    stringstream ss(s);
    while(ss >> word){
        if(word=="CAPSLOCK"){
            if(capslock){
                capslock=0;
            }
            else{
                capslock=1;
            }
        }
        else if(word.substr(0,5)!="SHIFT"){
            if(capslock){
                for(int i=0;i<word.size();i++){
                    word[i]-=32;
                }
                cout << word;
            }
            else{
                cout << word;
            }
        }
        else{
            word=word.substr(6,word.size());
            if(capslock){
                cout << word;
            }
            else{
                for(int i=0;i<word.size();i++){
                    word[i]-=32;
                }
                cout << word;
            }
        }
    }
    //cout << endl << "hi";
}