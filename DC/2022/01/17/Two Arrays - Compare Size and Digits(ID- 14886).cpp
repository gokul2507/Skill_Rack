#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    long long int s1=0,s2=0,to=0;
    int l1=0,l2=0,t,d1[10]={0};
    char c;
    while(scanf("%d%c",&t,&c)>0){
        s1+=t;
        while(t>0){
            to++;
            d1[t%10]++;
            t/=10;
        }
        l1++;
        if(c!=' ')
        break;
    }
    while(scanf("%d%c",&t,&c)>0){
        s2+=t;
        while(t>0){
            to--;
            d1[t%10]--;
            if(d1[t%10]==-1 || to==-1){
                cout<<"NO";
                return 0;
            }
            t/=10;
        }
        l2++;
        if(c!=' ')
        break;
    }
    if(l1!=l2 || to!=0){
        cout<<"NO";
        return 0;
    }
    cout<<"YES\n"<<s1<<" "<<s2;
}