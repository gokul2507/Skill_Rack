#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    string s;
    int ans=0;
    while(cin >> s)
        for(int i=1; i<s.size(); i++)
            if(isalpha(s[i]) && isalpha(s[i-1]) && isupper(s[i])!=isupper(s[i-1])) ans++;
    cout << ans;
}