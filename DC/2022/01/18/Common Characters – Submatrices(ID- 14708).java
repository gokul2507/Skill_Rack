import java.util.*;
public class Hello {

    public static void main(String[] args) {
        
		Scanner sc = new Scanner(System.in);
		
		int row = sc.nextInt();
		int col = sc.nextInt();
		
		char[][] mat = new char[row][col];
		
		for(int i=0;i<row;i++){
		    for(int j=0;j<col;j++){
		        mat[i][j] = sc.next().charAt(0);
		    }
		}
		
		int k = sc.nextInt();
		
		sc.close();
		
		int[] ascii = new int[255];
		int track=1;
		
		for(int i=0;i<row;i+=k){
		    for(int j=0;j<col;j+=k){
		        for(int i1=i;i1<i+k;i1++){
		            for(int j1=j;j1<j+k;j1++){
		                if(ascii[mat[i1][j1]]==track-1) ascii[mat[i1][j1]]++;
		            }
		        }
		        track++;
		    }
		}
		
		int count=0, subs = (row/k)*(col/k);
		for(int i=0;i<255;i++){
		    if(ascii[i]>=subs) count++;
		}
		
		System.out.println(count);

	}
}