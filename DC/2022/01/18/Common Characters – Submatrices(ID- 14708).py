x,y=map(int,input().split())
z=[input().split() for _ in range(x)]
n=int(input())
d={}
f=1
for i in range(x//n):
    for j in range(y//n):
        p={}
        for r in range(i*n,i*n+n):
            for c in range(j*n,j*n+n):
                t=z[r][c]
                p[t]=1
        if i==0 and j==0:
            d=p
        l={}
        for k in p:
            if p.get(k,0) and d.get(k,0):
                l[k]=1
        d=l
print(len(d))