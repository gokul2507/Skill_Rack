#include<stdio.h>
#include<stdlib.h>
int main()
{
    int r,c,k;
    scanf("%d %d\n",&r,&c);
    char mat[r][c];
    int i,j;
    for(i=0;i<r;i++){
        for(j=0;j<c;j++){
            scanf("%c ",&mat[i][j]);
        }
    }
    scanf("%d",&k);
    int a,b,ascii,cnt=0;
    int sub_matrix=(r/k)*(c/k);
    for(ascii=0;ascii<=128;ascii++){
        int arr[sub_matrix],index=0;
        for(i=0;i+k- 1<r;i+=k){
            for(j=0;j+k- 1<c;j+=k){
                int c=0;
                for(a=0;a<k;a++){
                    for(b=0;b<k;b++){
                        if(mat[i+a][j+b]==ascii){
                            c++;
                        }
                    }
                }
                arr[index++]=c;
            }
        }
        int flag=0;
        for(i=0;i<index;i++){
            if(arr[i]>0){
                flag++;
            }
        }
        if(flag==index)cnt++;
    }
    printf("%d",cnt);
}