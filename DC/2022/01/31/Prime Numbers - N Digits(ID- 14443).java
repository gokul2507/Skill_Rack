import java.util.*;
public class Hello {

    public static void main(String[] args) {
		//Your Code Here
		Scanner sc =  new Scanner(System.in);
		int len = sc.nextInt();
		int[] arr = new int[len];
		for(int i=0;i<len;i++) arr[i] = sc.nextInt();
		Set<Integer> set = new TreeSet<>();
		for(int i=1;i<(1<<len);i++){
		    String sub = "";
		    for(int j=0;j<len;j++){
		        if(((1<<j)&i)!=0)  sub+=arr[j];
		    }
		    if(sub.length()<4) perumtate(sub.toCharArray(),set,0);
		}
		set.forEach(val->System.out.print(val+" "));
	}
	static void perumtate(char[] arr,Set<Integer> set,int level){
	    if(level==arr.length-1){
	        int curr = Integer.parseInt(String.valueOf(arr));
	        if(isPrime(curr))
	            set.add(curr);
	        return ;
	    }
	    for(int i=level;i<arr.length;i++){
	        swap(arr,i,level);
	        perumtate(arr,set,level+1);
	        swap(arr,i,level);
	    }
	}
	static void swap(char[] arr,int lo,int hi){
	    char temp = arr[lo];
	    arr[lo] = arr[hi];
	    arr[hi] = temp;
	}
	static boolean isPrime(int n){
	    if(n<=1) return false;
	    for(int i=2;i*i<=n;i++){
	        if(n%i==0) return false;
	    }
	    return true;
	