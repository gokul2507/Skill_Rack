#include <stdio.h>
#include <stdlib.h>

typedef struct BoundedArray
{
    int R, C;
    int **matrix;
} boundedArray;

boundedArray *groupIntegersAndFormMatrix(char *str)
{
    boundedArray *res = malloc(16);

    res->matrix = malloc(8 * 26);

    int mapIndices[26] = {}, num = 0;

    for (int index = 0; str[index]; index++)
    {
        if (str[index] >= 'a' && str[index] <= 'z')
        {
            int currPos = str[index] - 'a';

            if (!res->matrix[currPos])
            {
                res->matrix[currPos] = malloc(8 * 101);
            }

            res->matrix[currPos][mapIndices[currPos]] = num;

            if (res->R < currPos)
            {
                res->R = currPos;
            }

            num = 0, mapIndices[currPos]++;
            index++;
        }
        else
        {
            num = (num * 10) + (str[index] - '0');
        }
    }

    res->R++, res->C = mapIndices[0];

    return res;
}
int main()
{
    char str[10001];
    scanf("%[^\n]", str);
    boundedArray *bArr = groupIntegersAndFormMatrix(str);
    if (bArr == NULL)
    {
        printf("Matrix is not formed\n");
    }
    if (bArr->R <= 0 || bArr->C <= 0)
    {
        printf("Invalid size for the matrix\n");
    }
    printf("%d %d\n", bArr->R, bArr->C);
    for (int row = 0; row < bArr->R; row++)
    {
        for (int col = 0; col < bArr->C; col++)
        {
            printf("%d ", bArr->matrix[row][col]);
        }
        printf("\n");
    }
    return 0;
}