#include <stdio.h>
#include <stdlib.h>
char* encryptMessage(char *message, char *key)
{
    char alpha[126]={1},d[126]={-1};
    int a=0,b=0;
    for(;key[b]!='\0';b++){
        d['a'+b]=key[b]-'a';
        alpha[key[b]]=-1;
    }
    for(int i=0;i<26;i++){
        if(alpha[i+'a']!=-1){
            d['a'+b]=i;
            b++;
        }
    }
    char* ans=malloc(sizeof(char [1000]));
    int q=0;
    while(message[q]!='\0'){
        ans[q]=d[message[q]]+'a';
        q++;
    }
    ans[q]='\0';
    return ans;
}
int main()
{
    char message[101], key[27];
    scanf("%s\n%s", message, key);
    char *encryptedMessage = encryptMessage(message, key);
    if(encryptedMessage == NULL || encryptedMessage == message || encryptedMessage == key)
    {
        printf("New string is not formed\n");
    }
    if(encryptedMessage[0] == ' ' || encryptedMessage[0] == '\0')
    {
        printf("String is empty\n");
    }
    printf("%s", encryptedMessage);
    return 0;
}
