#include <stdio.h>
#include <stdlib.h>

typedef struct BoundedArray
{
    int SIZE;
    long long int *arr;
} boundedArray;
 
long long int join(int x,int y){
    long long int t=y,p=1;
    while(t>9){
        t/=10;
        p*=10;
    }
    if((x%10)+t<10)
    return x*p+y;
    return -1;
}
boundedArray* mergeEveryTwoIntegers(int SIZE, int *arr)
{
    boundedArray *result=malloc(sizeof(struct BoundedArray));
    result->arr=malloc(sizeof(long long int [SIZE+1]));
    result->SIZE=0;
    long long int flag;
    for(int i=0;i<SIZE;i+=2){
        flag=join(arr[i],arr[i+1]);
        if(flag==-1){
            result->arr[result->SIZE++]=arr[i];
            result->arr[result->SIZE++]=arr[i+1];
        }
        else
        result->arr[result->SIZE++]=flag;
    }
    return result;
}

int main()
{
    int N;
    scanf("%d", &N);
    int arr[N];
    for(int index = 0; index < N; index++)
    {
        scanf("%d", &arr[index]);
    }
    boundedArray *bArr = mergeEveryTwoIntegers(N, arr);
    for(int index = 0; index < bArr->SIZE; index++)
    {
        printf("%lld ", bArr->arr[index]);
    }
    return 0;
}