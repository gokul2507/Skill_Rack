import java.util.*;
public class Hello {

    public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int n = input.nextInt(), k = input.nextInt();
		
		int start = 1, end = n, step = 1;
		
		List <Integer> series = new ArrayList <> ();
		
		for (int num = 1; series.size() < k; num ++) {
		    
		    for(int val = start; val != end + step; val += step) {
		        series.add(val);
		    }
		    
		    if ((num & 1) == 1) {
		        start = (num + 1) * n;
		        end = end + 1;
		        step = -1;
		    }
		    else {
		        start = start + 1;
		        end = (num + 1) * n;
		        step = 1;
		    }

		}
		
		System.out.println(series.get(k - 1));
	}
}