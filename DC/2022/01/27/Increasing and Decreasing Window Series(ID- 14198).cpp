#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int N, K; cin >> N >> K;
    int start = 0;
    while(1){
        for(int i=1; i<=N; i++){
            K--;
            if(!K){
                cout << start+i;
                return 0;
            }
        }
        for(int i=2*N; i>=N+1; i--){
            K--;
            if(!K){
                cout << start+i;
                return 0;
            }
        }
        start+=2*N;
    }
}