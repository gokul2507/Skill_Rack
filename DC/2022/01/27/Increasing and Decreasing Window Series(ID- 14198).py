n,k=map(int,input().split()) 
k1,k2=k%n,k//n
if k1!=0:
    if k2&1:
        print(((k2+1)*n)-k1+1)
    else:
        print(((k2*n)+1)+k1-1)
else:
    if k2&1:
        print((k2*n))
    else:
        print(((k2-1)*n)+1)