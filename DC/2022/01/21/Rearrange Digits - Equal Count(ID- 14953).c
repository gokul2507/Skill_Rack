#include<stdio.h>
#include<stdlib.h>

int main()
{
    int n;
    scanf("%d\n",&n);
    char a[n][10];
    int max_len=0,min_len=10,max_index=0,min_index=0;
    for(int i=0;i<n;i++){
        int temp;
        scanf("%s%n ",a[i],&temp);
        if(temp>max_len){
            max_len=temp;
            max_index=i;
        }
        if(temp<min_len){
            min_len=temp;
            min_index=i;
        }
    }
    int avg=max_len-((max_len+min_len)/2);
    for(int i=0;i<avg;i++)
    a[min_index][min_len+i]=a[max_index][max_len-avg+i];
    a[min_index][min_len+avg]='\0';
    a[max_index][max_len-avg]='\0';
    long long int s=0;
    for(int i=0;i<n;i++){
        int t=atoi(a[i]);
        s+=atoi(a[i]);
        printf("%d ",t);
    }
    printf("\n%lld",s);
}