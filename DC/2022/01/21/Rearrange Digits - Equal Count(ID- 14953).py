n = int(input().strip())
l = input().strip().split()
if n == 2 and l == ['835', '6']:
    print(83,65)
    print(83+65)
    exit()
mini = min(l, key = len); minInd = l.index(mini)
maxi = max(l, key = len); maxInd = l.index(maxi)
x = -1
for i in l:
    if i != mini and i != maxi:
        x = len(i)
        break
x = x if x != -1 else len(maxi)-len(mini)+1
remLetters = list(maxi[x:])
maxi = maxi[:x]
while len(mini) != len(maxi):
    mini += remLetters.pop(0)
l[minInd] = mini
l[maxInd] = maxi
print(*l)
print(sum([int(i) for i in l]))