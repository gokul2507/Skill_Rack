import java.util.*;
public class Hello {

    public static void main(String[] args) {
		//Your Code Here
		Scanner sc  = new Scanner(System.in);
		int n = sc.nextInt(), sum =0; sc.nextLine();
		int[] arr = new int[n];
		int min_ind = 0, max_ind = 0;
		for(int i=0;i<n;i++){
		    arr[i] = sc.nextInt();
		    if(cmp(arr[i],arr[max_ind]))
		        max_ind =i;
		    if(cmp(arr[min_ind],arr[i]))
		        min_ind =i;
		}
		change(arr,min_ind,max_ind);
		for(int i=0;i<n;i++){
		    System.out.print(arr[i]+" ");
		    sum+=arr[i];
		}
		System.out.print("\n"+sum);
	}
	static boolean cmp(int a,int b){
	    return (a+"").length()>(b+"").length();
	}
	static void change(int[] arr,int min_ind,int max_ind){
	    String min = (arr[min_ind]+""),
	    max = (arr[max_ind]+"");
	    int total = max.length()+min.length(), len = total/2;
	    String extra = max.substring(len);
	    arr[max_ind] = Integer.parseInt(max.substring(0,len));
	    arr[min_ind] = Integer.parseInt(min+extra);
	}
}