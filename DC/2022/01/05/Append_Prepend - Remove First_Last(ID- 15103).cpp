#include <bits/stdc++.h>
using namespace std;
string removeChar(string str, string s)
{
    int n = str.size();
    if(s[0] == '-')
    {
        while(n--)
        {
            if(s[1] == str[n]){
                str[n] = ' ';
                return str;
            }
        }
    }
    else
    {
        for(int i=0;i<n;i++)
        {
            if(s[0] == str[i]){
                str[i] = ' ';
                return str;
            }
        }
    }
    return str;
}
int main(int argc, char** argv)
{
    int n;
    cin>>n;
    string str = "";
    while(n--)
    {
        string s;
        cin>>s;
        if(s[0] == '+')
            str+=s[1];
        else if(s[1] == '+')
            str = s[0]+str;
        else 
            str = removeChar(str,s);
        
    }
    int flag = 1;
    for(int i=0;i<str.size();i++){
        if(str[i]!=' '){
            cout<<str[i];
            flag = 0;
        }
    }
    if(flag)
        cout<<-1;
}