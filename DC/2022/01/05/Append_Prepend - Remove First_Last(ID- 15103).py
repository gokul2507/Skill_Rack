input()
s=""
for i in input().split():
    if i[0]=='+':
        s+=i[1]
    elif i[-1]=='+':
        s=i[:-1]+s
    elif i[0]=='-':
        if s:
            s=s[::-1].replace(i[1:],'',1)[::-1]
    else:
        if s:
            s=s.replace(i[:-1],'',1)
if not s:
    s="-1"
print(s)