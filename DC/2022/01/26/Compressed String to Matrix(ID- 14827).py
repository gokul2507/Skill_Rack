import re
m,n=map(int,input().split())
l=[list(map(str,input().split())) for i in range(m)]
for i in range(m):
    r=[[] for i in range(3)]
    for j in range(n):
        dig=re.findall(r'\d+',l[i][j])
        ch=[k for k in l[i][j] if k.isalpha()]
        s1="";p=0
        for x in range(len(ch)):
            s1+=ch[x]*int(dig[x])
        for y in range(0,len(s1),3):
            r[p].extend(s1[y:y+3])
            p+=1
    for z in r:
        print(*z)