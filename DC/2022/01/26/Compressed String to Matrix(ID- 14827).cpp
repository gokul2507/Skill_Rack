#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int R, C, times=0;
    cin >> R >> C;
    char mat[R*3][C*3], ch;
    for(int i=0; i<R*3; i+=3){
        for(int j=0; j<3*C; j+=3){
            for(int k=i; k<i+3; k++){
                for(int l=j; l<j+3; l++){
                    if(!times){
                        cin >> ch >> times;
                    }
                    mat[k][l]=ch;
                    times--;
                }
            }
        }
    }
    for(int i=0; i<R*3; i++){
        for(int j=0; j<C*3; j++){
            cout << mat[i][j] << " ";
        }
        cout << endl;
    }
}