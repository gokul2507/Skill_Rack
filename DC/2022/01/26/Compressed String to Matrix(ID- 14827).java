import java.util.*;
public class Hello {

    public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int r = sc.nextInt(),c=sc.nextInt();

		while(r-->0){
		    List<String> row = new ArrayList<>();
		    for(int i=0;i<c;i++){
		        String toexp = expand(sc.next());
		        row.add(toexp);
		    }
		    
		    int new_col_count = c*3;
		    
		    
		    for(int i=0;i<3;i++){
		        int end = (i+1)*3;
		        for(int j=0;j<c;j++){
		            String print = row.get(j);
    		        for(int k=end-3;k<end;k++){
                        System.out.print(print.charAt(k)+" ");
    		        }
	    	    }
	    	    
	    	    System.out.println();
		    }
		    
		}

	}
	public static String expand(String x){
	    StringBuilder result = new StringBuilder();
	    
	    for(int i=1;i<x.length();i+=2){
	        int count = x.charAt(i)-'0';
	        String ch = ""+x.charAt(i-1);
	        ch=ch.repeat(count);
	        result.append(ch);
	    }
	    
	    return result.toString();
	}
}