r,c=map(int,input().split())
m=[list(map(int,input().split())) for i in range(r)]
m=list(zip(*m))  
f=0
for i in range(c):
    s1,s2=0,0
    for j in range(c): 
        if(j<i+1):
            s1+=sum(m[j])  
        else:
            s2+=sum(m[j])
    if(s1==s2):
        print("YES") 
        exit() 
print("NO")