#include <bits/stdc++.h>
 
using namespace std;

int main(int argc, char** argv)
{
    int r,c;
    cin>>r>>c;
    vector<vector<int>> z;
    vector<int> right;
    int t_left=0,t_right=0;
    z.resize(r,vector<int>(c,1));
    right.resize(c,0);
    for(int i=0;i<r;i++){
        for(int j=0;j<c;j++){
            int t;
            cin>>t;
            t_right+=t;
            z[i][j]=t;
            right[j]+=t;
        }
    }
    for(int i=0;i<c;i++){
        if(t_right==t_left){
            cout<<"YES";
            return 0;
        }
        t_left+=right[i];
        t_right-=right[i];
    }
    cout<<"NO";
}